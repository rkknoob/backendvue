<?php

use Illuminate\Database\Seeder;
use App\Model\Noticeimages;
use Carbon\Carbon;

class NoticeimagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('notice_images')->delete();

        $json = File::get("database/noticeimages.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
            Noticeimages::create(array(
                'id' => $obj->id,
                'notice_id' => $obj->notice_id,
                'detail_type' => $obj->detail_type,
                'img_en' => $obj->img_en,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ));
        }
    }
}
