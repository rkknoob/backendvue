-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 13, 2020 at 03:24 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` text COLLATE utf8mb4_unicode_ci,
  `lname` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `password` text COLLATE utf8mb4_unicode_ci,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `role` text COLLATE utf8mb4_unicode_ci,
  `token` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_token` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fname`, `lname`, `email`, `password`, `phone`, `role`, `token`, `is_active`, `reset_password_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'JSM', 'Beauty', 'JungSaemMool@mail.com', '$2y$10$xZGxfT/9ezOJSlv6pjUljujN9CJ8nkNp0SbpV55foA77e7pxNQl3K', '0900900990', 'SuperAdmin', 'HAWAouUrHyA9txpGXEqdz9bIfah24sY68DrborRI', 'Y', '', NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(2, 'jsm', 'siampiwat', 'jsm@mail.com', '$2y$10$r67yy17I/DaetuUtKbPAYOqgbxvmXy5Mqm4pPLcwtXsAwktA1WEge', '029999999', 'Admin', 'HAWAouUrHyA9txpGXEqdz9bIfah24sY68DrborRI', 'N', '', NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, 'test', 'system', 'p@mail.com', '$2y$10$yBVYuof/OajXoKrrrWtA3.gBpNelY9eIvQ8729rDLEJDbkfSd5vqm', '0900900990', 'Admin', 'HAWAouUrHyA9txpGXEqdz9bIfah24sY68DrborRI', 'N', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'siam', 'piwat', 'siam-piwat@mail.com', '$2y$10$telbrbggbUG3Y8Fi77gunOdkE7auJs1CkwRq6.bBs4uGJkuKBiw6q', NULL, NULL, NULL, 'N', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Kwanruthai', 'Ratanamongkolgul', 'kwanruthai.r@siampiwat.com', '$2y$10$Cg/FbQuQATH3Sj3cxsG4ZultIOEv.xkBVB2XkG1kgZ9NP3RK60lJ2', NULL, NULL, NULL, 'Y', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'Kriengkrai', 'Chutrakul', 'Kriengkrai.C@siampiwat.com', '$2y$10$5Ojhb8ImHHk4Th5fQiaetu6CtCcdZE3DYSrB8K6LNuhSrYFkD7QdC', NULL, NULL, NULL, 'Y', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'FTest<script>alert(1)</script>', 'LTest<script>alert(2)</script>', 'kraisie.a@siampiwat.com', '$2y$10$cSvYxpM8a1l9rj7LUd2pTeuS4MaGAjACaJUHWe/u2oPyX1BCQNbHa', NULL, NULL, NULL, 'Y', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'Test1', 'Test2', 'kenthmsn@hotmail.com', '$2y$10$ZAxUtKq87i7/wlgRhyJzsO9zH3bBt2Luurmv5/ZSLsZl0nZAZfLZi', NULL, NULL, NULL, 'Y', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'Sujittra', 'Wauwai', 'sujittra.adev@gmail.com', '$2y$10$0RslsdCv1demOnv9cs5bU.TOM6mYZ3eBNGLkr3iSzmN/iMpu5MsaC', NULL, NULL, NULL, 'Y', '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collapse` text COLLATE utf8mb4_unicode_ci,
  `sort_id` int(11) DEFAULT NULL,
  `show` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `name_en`, `name_th`, `icon`, `uri`, `collapse`, `sort_id`, `show`, `created_at`, `updated_at`) VALUES
(1, 'Manage Admin', 'จัดการแอดมิน', 'fas fa-users-cog', '/cms/admin', 'cms/admin*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Manage Product', 'จัดการสินค้า', 'fas fa-store', '/cms/dashboard', 'cms/products*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Manage Banner Category', 'จัดการแบนเนอร์ประเภทสินค้า', 'fa fa-wrench', '/cms/category', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Manage VDO Index', 'จัดการวีดีโอ', 'fas fa-video', '/cms/video', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Manage Pop-Up Index', 'จัดการป๊อปอัพ', 'fas fa-image', '/cms/popup', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'Manage Brand JSM', 'จัดการแบรนด์ JSM', 'fas fa-photo-video', '/cms/dashboard', 'cms/brandjsm*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'Manage Artist JSM', 'จัดการ Artist JSM', 'fas fa-photo-video', '/cms/dashboard', 'cms/artist*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'Manage Artist Tip', 'จัดการ Artist Tip', 'fas fa-photo-video', '/cms/dashboard', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'Manage Find A Store', 'จัดการร้านค้า', 'fas fa-photo-video', '/cms/dashboard', 'cms/store*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 'Manage Events', 'จัดการอีเว้นท์', 'fas fa-calendar-alt', '/cms/event', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 'Manage New Arrival', 'จัดการสินค้ามาใหม่', 'fas fa-paint-brush', '/cms/linestory', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 'Manage FAQ', 'จัดการ FAQ', 'far fa-comment', '/cms/dashboard', 'cms/faq*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 'Manage Notice', 'จัดการประกาศ', 'fas fa-bullhorn', '/cms/notice', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 'Manage Q&A', 'จัดการถามและตอบ', 'fas fa-question', '/cms/dashboard', 'cms/qa*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 'Manage Review', 'จัดการรีวิว', 'far fa-star', '/cms/dashboard', 'cms/review*', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 'Manage Banner Index', 'จัดการแบนเนอร์', 'fas fa-tools', '/cms/banner', NULL, 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 'Manage Menu', 'จัดการเมนู', 'fas fa-indent', '/cms/settingmenu', 'cms/settingmenu*', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 'Manage Lang', 'จัดการสองภาษา', 'fas fa-globe-europe', '/cms/setting-lang', '', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_head`
--

CREATE TABLE `admin_menu_head` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu_head`
--

INSERT INTO `admin_menu_head` (`id`, `name_en`, `name_th`, `is_active`, `menu_id`, `sort_id`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'แอดมิน', 'Y', NULL, 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'PRODUCT', 'ผลิตภัณฑ์', 'Y', NULL, 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'INDEX', 'อินเด็กซ์', 'Y', NULL, 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'JUNG SAEM MOOL', 'จองแซมมุล', 'Y', NULL, 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'LOUNGE', 'เลาจน์', 'Y', NULL, 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'CONTACT', 'ติดต่อ', 'Y', NULL, 6, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_process`
--

CREATE TABLE `admin_menu_process` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `head_id` int(11) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu_process`
--

INSERT INTO `admin_menu_process` (`id`, `menu_id`, `head_id`, `submenu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 2, 2, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 3, 3, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 4, 3, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 5, 3, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 6, 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 7, 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 8, 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 9, 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 10, 5, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 11, 5, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 12, 6, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 13, 6, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 14, 6, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 15, 6, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 16, 3, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 17, 1, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 18, 1, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_submenu`
--

CREATE TABLE `admin_submenu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_id` int(11) DEFAULT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `show` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_submenu`
--

INSERT INTO `admin_submenu` (`id`, `sub_id`, `name_en`, `name_th`, `icon`, `uri`, `sort_id`, `show`, `created_at`, `updated_at`) VALUES
(1, 6, 'List Concept', 'คอนเซ็ปต์', 'fas fa-list', 'cms/brandjsm/concept', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 6, 'List Film', 'ฟิล์ม', 'fas fa-list', 'cms/brandjsm/brandcontent', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 7, 'List Introduction', 'แนะนำ', 'fas fa-list', 'cms/artist', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 7, 'List Magazine', 'แม็กกาซีน', 'fas fa-list', 'cms/artist/magazine', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 9, 'List Find A Store', 'สโตร์', 'fas fa-list', 'cms/store', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 9, 'List Store Category', 'ประเภทสโตร์', 'fas fa-list', 'cms/store/plops', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 14, 'Setting Q&A', 'ตั้งค่าถาม-ตอบ', 'fas fa-list', 'cms/qa/settingqa', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 14, 'Reply Q&A', 'ตอบกลับ', 'fas fa-list', 'cms/qa/replyqa', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 15, 'List Review', 'รีวิว', 'fas fa-list', 'cms/review/settingreview', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 2, 'List Product', 'ผลิตภัณฑ์', 'fas fa-list', 'cms/products', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 2, 'List Sku', 'เอสเคยูลิส', 'fas fa-list', 'cms/sku', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 1, 'Add Admin', 'เพิ่มแอดมิน', 'fas fa-users-cog', 'cms/admin/add', 1, 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 1, 'List Admin', 'แอดมินลิส', 'fas fa-users-cog', 'cms/admin', 2, 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 17, 'Setting Group Menu', 'ตั้งค่ากลุ่มเมนู', 'fas fa-list-ol', 'cms/settingmenu', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 17, 'Setting Menu', 'ตั้งค่าเมนู', 'fas fa-list-ol', 'cms/settingmenu-mainmenu', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 17, 'Setting Submenu', 'ตั้งค่าเมนูย่อย', 'fas fa-list-ol', 'cms/settingmenu-mainmenu-submenu', 3, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 17, 'Setting Menu Home', 'ตั้งค่าเมนูหลัก', 'fas fa-list-ol', 'cms/settingmenu-home', 4, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 12, 'Setting Faq', 'ตั้งค่า FAQ', 'fas fa-list', 'cms/faq/settingfaq', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(19, 12, 'Setting Faq Category', 'ตั้งค่าประเภท FAQ', 'fas fa-list', 'cms/faq/settingfaq-cate', 2, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 8, 'List Artist Tip', 'อาร์สติกทิป', 'fas fa-list', 'cms/artistTip', 1, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(21, 6, 'List Magazine', 'แม็กกาซีน', 'fas fa-list', 'cms/brandjsm-magazine-setting', 3, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `answers_qa`
--

CREATE TABLE `answers_qa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qa_id` int(11) DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers_qa`
--

INSERT INTO `answers_qa` (`id`, `qa_id`, `details`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gu Pune ตอบ กลับ', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 1, 'Gu Pune เทส 2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 1, 'ทดสอบตอบคำถาม', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 1, 'test', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 1, 'Test Answer', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 3, 'ทดสอบข้อมูล', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 1, 'Test', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 1, 'Test Cross-Site Scripting (XSS)', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 1, 'Display cookie', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 3, 'เทสอีกรอบ', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `img_url` longtext COLLATE utf8mb4_unicode_ci,
  `img_size` longtext COLLATE utf8mb4_unicode_ci,
  `type` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `artist_magazine`
--

CREATE TABLE `artist_magazine` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `img_url` longtext COLLATE utf8mb4_unicode_ci,
  `img_url_thai` longtext COLLATE utf8mb4_unicode_ci,
  `img_size` longtext COLLATE utf8mb4_unicode_ci,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `type` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artist_magazine`
--

INSERT INTO `artist_magazine` (`id`, `name_en`, `name_th`, `img_url`, `img_url_thai`, `img_size`, `detail_th`, `detail_en`, `type`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '201612_주부생활', '201612_주부생활', '20200121170050.jpg', NULL, NULL, NULL, NULL, 'M', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Introduction', 'แนะนำ', '20191204112402.jpg', '20200407130325.jpg', NULL, NULL, NULL, 'T', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, '201612_퍼스트룩', '201612_퍼스트룩', '20200121170143.jpg', NULL, NULL, NULL, NULL, 'M', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, '201701_인스타일', '201701_인스타일', '20200121170221.jpg', NULL, NULL, NULL, NULL, 'M', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, '201707_보그', '201707_보그', '20200121170248.jpg', NULL, NULL, NULL, NULL, 'M', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, '201707_보그(2)', '201707_보그(2) TH', '20200121171441.jpg', NULL, NULL, NULL, NULL, 'M', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `artist_tip`
--

CREATE TABLE `artist_tip` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci,
  `title_th` text COLLATE utf8mb4_unicode_ci,
  `img_banner` text COLLATE utf8mb4_unicode_ci,
  `detail_en` text COLLATE utf8mb4_unicode_ci,
  `detail_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `artist_tip`
--

INSERT INTO `artist_tip` (`id`, `title_en`, `title_th`, `img_banner`, `detail_en`, `detail_th`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Classic Autumn MAKE UP', 'ทดสอบ', '20191119120103.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/suQxNTfjsmc\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/suQxNTfjsmc\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'How to use Lip Lacquer', 'ทดสอบ', '20191119120149.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/HzKvUVmhb8E\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/HzKvUVmhb8E\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Leopard Makeup', 'ทดสอบ', '20191119120231.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/D6PvzqsFNeY\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/D6PvzqsFNeY\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Holiday Makeup', 'ทดสอบ', '20191119120314.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/HpVY07IidH4\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/HpVY07IidH4\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Winter See-through Makeup', 'ทดสอบ', '20191119120358.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Atl5YxKyT8w\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Atl5YxKyT8w\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'Quick and Easy Makeup for Men', 'ทดสอบ', '20191119120435.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/GqsS_uPvIUw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/GqsS_uPvIUw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'ROZY MAKEUP', 'ทดสอบ', '20191119120506.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/sSxxqO0bwv8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/sSxxqO0bwv8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'HOW TO USE ARTISTIC HAIR CONTOUR', 'ทดสอบ', '20191119120548.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/oYe3cgiVzDQ\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/oYe3cgiVzDQ\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'How to build perfect skincare routine', 'ทดสอบ', '20191119120624.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/_aRvK0NpMQ8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/_aRvK0NpMQ8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 'Your\'Red\'MAKEUP', 'ทดสอบ', '20191119120659.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/h_2F4NhDv2Q\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/h_2F4NhDv2Q\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 'One-tone, Two-tone Gradient Lip Makeup', 'ทดสอบ', '20191119120749.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/ijH3YcfSiJ0\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/ijH3YcfSiJ0\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 'How to create long lasting makeup for oily skin', 'ทดสอบ', '20191119120820.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/6YzTWfh0gKw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/6YzTWfh0gKw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 'Sugar Summer MAKEUP', 'ทดสอบ', '20191119120913.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/dLxujHUOkgw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/dLxujHUOkgw\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 'How to sculpt short brows', 'ทดสอบ', '20191119121002.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/jazc96pL2gk\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/jazc96pL2gk\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 'NEWTRO Makeup', 'ทดสอบ', '20191119121046.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/JKkxCDIJDpU\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/JKkxCDIJDpU\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 'Nearly Nude Makeup', 'ทดสอบ', '20191119121122.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/A8VGT1kZQ2E\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/A8VGT1kZQ2E\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 'Light on! Makeup', 'ทดสอบ', '20191119121147.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/pCUPYyKWBhE\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/pCUPYyKWBhE\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 'Contour Makeup', 'ทดสอบ', '20191119121216.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/baB19JIr5Y8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/baB19JIr5Y8\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(19, 'Newtro Chilly Red Makeup', 'ทดสอบ', '20191119121240.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/ZmNBB6qPDhg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/ZmNBB6qPDhg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 'Trench Coat Makeup', 'ทดสอบ', '20191119121306.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/_mgzxl5aNFE\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/_mgzxl5aNFE\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `type`, `id_category`, `id_product`, `seq`, `name_en`, `name_th`, `img_en`, `img_th`, `is_active`, `created_at`, `updated_at`) VALUES
(1, NULL, 4, 45, 1, 'ESSENTIAL MOOL RADIANCE CREAM', NULL, '20191202120740.jpg', '20191202120744.jpg', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, NULL, 4, 50, 2, 'MINIFYING VC AMPOULE', NULL, '20191202121308.jpg', '20191202121312.jpg', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, NULL, 2, 25, 3, 'LIP-PRESSION #Newtro Chilly', NULL, '20191211160735.jpg', '20191211160739.jpg', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `brandjsm`
--

CREATE TABLE `brandjsm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `detail_film` longtext COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `link_video` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brandjsm`
--

INSERT INTO `brandjsm` (`id`, `name_en`, `name_th`, `type`, `is_active`, `img_en`, `img_th`, `detail_film`, `detail_en`, `detail_th`, `link_video`, `created_at`, `updated_at`) VALUES
(1, '[Film] GO MAKE UP', '[Film] GO MAKE UP TH', 'F', 'Y', '20200122115851.jpg', '20200122115854.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Akvy47c19dI\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, 'https://youtu.be/zggkI75Fwio', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(2, '[Film] TVC MAKE UP', '[Film] TVC MAKE UP', 'F', 'Y', '20200122115916.jpg', '20200122115919.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/29tc1CZmZa4\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, 'https://youtu.be/zggkI75Fwio', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, '[Film] Star-cealer foundation How to', '[Film] Star-cealer foundation How to', 'F', 'Y', '20200122115958.jpg', '20200122120002.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/NKncf6YCBwg\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, 'https://youtu.be/zggkI75Fwio', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(17, '[Film] High Tinted Lip Lacquer Hyper Matt & Full Glaze', '[Film] High Tinted Lip Lacquer Hyper Matt & Full Glaze', 'F', 'Y', '20200122120140.jpg', '20200122120143.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/YkoIeiJj38E\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, 'https://youtu.be/zggkI75Fwio', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(18, 'Concept', 'คอนเซ็ปต์', 'C', 'Y', '20200401200401.png', '20200407130310.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/zggkI75Fwio\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, 'https://youtu.be/zggkI75Fwio', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(19, '201706_마리끌레르', '201706_마리끌레르', 'M', 'Y', '20200122152816.jpg', '20191127111640.jpg', NULL, '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/zggkI75Fwio\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/zggkI75Fwio\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(20, '[Film] Artist Layer Concealing base', '[Film] Artist Layer Concealing base TH', 'F', 'Y', '20200122120805.jpg', '20200122120808.jpg', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/zggkI75Fwio\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', NULL, NULL, NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(21, '201706_보그', '201706_보그', 'M', 'Y', '20200122152905.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(22, '201706_쎄씨', '201706_쎄씨', 'M', 'Y', '20200122152921.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(23, '201706_얼루어(1)', '201706_얼루어(1)', 'M', 'Y', '20200122152957.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(24, '201706_얼루어(2)', '201706_얼루어(2)', 'M', 'Y', '20200122153412.jpg', NULL, NULL, NULL, NULL, NULL, '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `brand_magazine`
--

CREATE TABLE `brand_magazine` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img_banner` text COLLATE utf8mb4_unicode_ci,
  `topic_name` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_video` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `img`, `is_active`, `folder_name`, `created_at`, `updated_at`) VALUES
(1, 'BASE', '20191127163238.jpg', 'Y', 'base', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(2, 'LIP', '20191127163253.jpg', 'Y', 'lip', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, 'EYE', '20191127163317.jpg', 'Y', 'eye', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(4, 'SKINCARE', '20191127163340.jpg', 'Y', 'skincare', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(5, 'TOOL', '20191127163402.jpg', 'Y', 'tool', '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `cate_qa`
--

CREATE TABLE `cate_qa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cate_qa`
--

INSERT INTO `cate_qa` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Product inquiry', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Shipping inquiry', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Payment inquiry', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Cancel request (Before shipping)', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Exchange&Refund', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `dim_line_user`
--

CREATE TABLE `dim_line_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_type` text COLLATE utf8mb4_unicode_ci,
  `banner1` text COLLATE utf8mb4_unicode_ci,
  `banner2` text COLLATE utf8mb4_unicode_ci,
  `topic_en` text COLLATE utf8mb4_unicode_ci,
  `topic_th` text COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_type`, `banner1`, `banner2`, `topic_en`, `topic_th`, `detail_en`, `detail_th`, `start_date`, `end_date`, `is_active`, `created_at`, `updated_at`) VALUES
(1, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(2, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(4, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(5, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(6, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(7, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(8, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(9, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(10, NULL, '20191128032659.jpg', '20191128032704.jpg', 'Skin nuder Foundation  ', 'Skin nuder Foundation TH', '<p>Skin nuder Foundation&nbsp; _en</p><p><img src=\"http://13.250.225.6/public/event/20200217182400gEC4D.jpg\" style=\"width: 387px;\"><br></p>', '<p>Skin nuder Foundation&nbsp; _th</p><p><img src=\"http://13.250.225.6/public/event/20200217182407HjVvR.jpg\" style=\"width: 387px;\"><br></p>', '2019-12-01 00:00:00', '2019-12-31 00:00:00', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `fact_menu`
--

CREATE TABLE `fact_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `subject_th` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci,
  `question` text COLLATE utf8mb4_unicode_ci,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `subject`, `question`, `answer`, `type`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'เกิดข้อผิดพลาดเมื่อทำการเช็คเอาท์', 'เกิดข้อผิดพลาดเมื่อทำการเช็คเอาท์', 'n/a', '2', 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'สามารถใช้คะแนนสะสมได้อย่างไรบ้าง ?', 'สามารถใช้คะแนนสะสมได้อย่างไรบ้าง ?', 'n/a', '2', 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'คะแนนสะสมและคูปองส่วนลดสามารถใช้ได้นานเท่าไร ?', 'คะแนนสะสมและคูปองส่วนลดสามารถใช้ได้นานเท่าไร ?', 'n/a', '4', 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'เกณฑ์ในการยกเลิก/เปลี่ยน/คืนสินค้า', 'เกณฑ์ในการยกเลิก/เปลี่ยน/คืนสินค้า', 'เกณฑ์ในการยกเลิก/เปลี่ยน/คืนสินค้า\n- หากลูกค้ามีความประสงค์ที่จะเปลี่ยนหรือคืนสินค้า ต้องดำเนินการขอเปลี่ยนหรือคืนสินค้าภายใน 7 วัน นับจากที่ได้รับสินค้า และลูกค้าต้องเป็นผู้รับผิดชอบค่าจัดส่งสินค้าคืน\n- หากสินค้าที่ได้รับ ไม่ตรงตามที่แสดงในเว็บไซต์หรือในโฆษณา หรือสินค้ามีข้อบกพร่อง ลูกค้าสามารถแจ้งความประสงค์ในการเปลี่ยนสินค้า ไม่เกิน 30 วัน นับจากได้รับสินค้า โดยทางเราจะเป็นผู้รับผิดชอบค่าใช้จ่ายในการเปลี่ยนสินค้า\n- การปลี่ยนหรือคืนสินค้า ไม่สามารถทำได้ในกรณีต่อไปนี้ เมื่อเกินกำหนดระยะเวลาการคืนสินค้า, สินค้าเสียหายหรือสูญหายอันเนื่องมาจากผู้ซื้อเป็นสาเหตุ, สินค้าได้รับความเสียหายเนื่องจากบรรจุภัณฑ์, มูลค่าของผลิตภัณฑ์ที่ลดลงในทางการขายเมื่อเกินจากระยะเวลาที่เหมาะสม', '1', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'หากมีการสั่งซื้อและลงทะเบียนชำระเงินผ่านบัญชีธนาคาร ต้องทำการชำระเงินภายในระยะเวลาเท่าไร ?', 'หากมีการสั่งซื้อและลงทะเบียนชำระเงินผ่านบัญชีธนาคาร ต้องทำการชำระเงินภายในระยะเวลาเท่าไร ?', 'n/a', '3', 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'หากมีปัญหาเกี่ยวกับผิวหลังจากใช้ผลิตภัณฑ์ สามารถขอคืนสินค้าได้หรือไม่ ?', 'หากมีปัญหาเกี่ยวกับผิวหลังจากใช้ผลิตภัณฑ์ สามารถขอคืนสินค้าได้หรือไม่ ?', 'หากลูกค้าพบว่าปัญหาเกี่ยวกับผิวที่เกิดขึ้น เป็นผลมาจากการใช้ผลิตภัณฑ์ฯ ลูกค้าสามารถส่งความคิดเห็นที่ได้รับจากแพทย์ผิวหนังมายังศูนย์บริการลูกค้า โดยทางเราจะทำการคืนเงินและคืนผลิตภัณฑ์หลังจากมีการพูดคุยกับทางผู้ผลิต ในเงื่อนไขดังนี้ \n- ปริมาณผลิตภัณฑ์ที่คงเหลือต้องไม่น้อยกว่า 2/3 ของปริมาณทั้งหมด\n- การเปลี่ยนและคืนสินค้า (คืนเงิน) เนื่องจากผลข้างเคียงหลังจากใช้ผลิตภัณฑ์ไม่สามารถทำได้หลังจาก 30 วัน นับจากวันที่ได้รับสินค้า', '3', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'หากมีการทำเรื่องขอเปลี่ยนหรือคืนสินค้าแล้ว จะทราบผลเมื่อไร ?', 'หากมีการทำเรื่องขอเปลี่ยนหรือคืนสินค้าแล้ว จะทราบผลเมื่อไร ?', 'ลูกค้าสามารถตรวจสอบการแลกเปลี่ยนหรือคืนสินค้าได้ที่เมนู [หน้าของฉัน] → [สอบถามรายละเอียดเพิ่มเติมเกี่ยวกับการคืนสินค้า/แลกเปลี่ยนสินค้า]', '3', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'ยกเลิกการสั่งซื้อแล้ว แต่ยังมีการจัดส่งสินค้า', 'ยกเลิกการสั่งซื้อแล้ว แต่ยังมีการจัดส่งสินค้า', 'ติดต่อศูนย์บริการลูกค้าของเรา ที่หมายเลข 080-xxx-xxxx เพื่อรับคำแนะนำและขั้นตอนการแก้ไขปัญหา', '3', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 'การชำระเงินด้วยบัตรเครดิตปลอดภัยหรือไม่ ?', 'การชำระเงินด้วยบัตรเครดิตปลอดภัยหรือไม่ ?', 'n/a', '1', 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 'สามารถถอนคำขอยกเลิกการสั่งซื้อได้หรือไม่ ?', 'สามารถถอนคำขอยกเลิกการสั่งซื้อได้หรือไม่ ?', 'คำสั่งซื้อที่ได้ทำการยกเลิกแล้ว จะไม่สามารถเรียกคืนคำสั่งซื้อดังกล่าวได้ หากลูกค้าต้องการสั่งซื้อสินค้ารายการเดิม ต้องทำการกดสั่งซื้อใหม่อีกครั้ง', '3', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 'การยกเลิกคำสั่งซื้อ', 'การยกเลิกคำสั่งซื้อ', 'หากต้องการยกเลิกคำสั่งซื้อ สามารถยกเลิกโดยเข้าไปที่เมนู [หน้าของฉัน] → [คำสั่งซื้อ/การจัดส่ง] โดยลูกค้าสามารถยกเลิกคำสั่งซื้อได้ก็ต่อเมื่อสถานะการสั่งซื้อคือ [ได้รับคำสั่งซื้อแล้ว] และ [การชำระเงินเสร็จสิ้น]', '1', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 'การติดตามการจัดส่ง', 'การติดตามการจัดส่ง', 'หากคุณเป็นสมาชิก สามารถตรวจสอบติดตามคำสั่งซื้อและการจัดส่งของคุณได้จากเมนู [หน้าของฉัน] → [คำสั่งซื้อ/การจัดส่ง], หากคุณไม่ได้เป็นสมาชิก สามารถป้อนหมายเลขคำสั่งซื้อที่ต้องการตรวจสอบได้ที่เมนูรายการสั่งซื้อ เพื่อตรวจสอบติดตามการจัดส่ง', '1', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE `faq_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`id`, `name_en`, `name_th`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Delivery', 'การจัดส่งสินค้า', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Coupon', 'คูปอง', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Refund', 'คืนเงิน', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Order', 'ใบสั่งซื้อสินค้า', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `finestore`
--

CREATE TABLE `finestore` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) DEFAULT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `img1` text COLLATE utf8mb4_unicode_ci,
  `img2` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `tel` text COLLATE utf8mb4_unicode_ci,
  `map` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `finestore`
--

INSERT INTO `finestore` (`id`, `type`, `name_en`, `name_th`, `detail_en`, `detail_th`, `img1`, `img2`, `address`, `tel`, `map`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, 'PLOPS', 'PLOPS', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Akvy47c19dI\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '<p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/Akvy47c19dI\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p>', '20200128155445sNzEC.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 2, 'Hyundai Department Store,Shinchon (1F)', 'Hyundai Department Store,Shinchon (1F)', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180436jjdhN.jpg\" style=\"width: 903px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180442Y0YRd.jpg\" style=\"width: 920px;\"><br></p>', '202001281605273LIth.jpg', NULL, '1F, 83, Sinchon-ro, Seodaemun-gu, Seoul, Republic of Korea', '02-3145-2190', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 2, 'Hyundai Department Store, Jungdong (1F)', 'Hyundai Department Store, Jungdong (1F)', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180605myHXe.jpg\" style=\"width: 903px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180609J0xBN.jpg\" style=\"width: 920px;\"><br></p>', '20200128160857xWUIl.jpg', NULL, '180, Gilju-ro, Bucheon-si, Gyeonggi-do, Republic of Korea', '032-623-2167', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 2, 'Lotte Department Store, Main Branch (B1)', 'Lotte Department Store, Main Branch (B1)', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180641VZZ8O.jpg\" style=\"width: 903px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/2020021718064568amu.jpg\" style=\"width: 920px;\"><br></p>', '202001281610085pCIc.jpg', NULL, 'B1, 81, Namdaemun-ro, Jung-gu, Seoul, Republic of Korea', '02-772-3231', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 2, 'Lotte Department store, Daegu (B1)', 'Lotte Department store, Daegu (B1)', '<p><img src=\"http://13.250.225.6/public/findstore/202002171808571qhn2.jpg\" style=\"width: 903px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180902kUtKw.jpg\" style=\"width: 920px;\"><br></p>', '20200128163145onjgF.jpg', NULL, 'B1, 161, Taepyeong-ro, Buk-gu, Daegu, Republic of Korea', '053-660-3983 / 053-660-3984', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 2, 'Lotte Department Store, Jamsil (1F)', 'Lotte Department Store, Jamsil (1F)', '<p><img src=\"http://13.250.225.6/public/findstore/20200217180934gDKUV.jpg\" style=\"width: 920px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/202002171809386rsDW.jpg\" style=\"width: 937px;\"><br></p>', '20200128163301QSFfO.jpg', NULL, '240, Olympic-ro, Songpa-gu, Seoul, Republic of Korea', '02-2143-7219 / 02-2143-7223', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 3, 'Chicor - Hongik University', 'Chicor - Hongik University', '<p><img src=\"http://13.250.225.6/public/findstore/202002171810042nQoe.jpg\" style=\"width: 320px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/202002171810118Lv6f.jpg\" style=\"width: 320px;\"><br></p>', '20200128163849SeMMY.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 3, 'Chicor - Myeongdong', 'Chicor - Myeongdong', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181029Zmpl0.jpg\" style=\"width: 320px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181033c0soj.jpg\" style=\"width: 320px;\"><br></p>', '20200128163947qNGko.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 3, 'Chicor - AK Plaza, Wonju', 'Chicor - AK Plaza, Wonju', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181048yZvAH.jpg\" style=\"width: 320px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/202002171810545a1mH.jpg\" style=\"width: 320px;\"><br></p>', '20200128164023Q7fax.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 4, 'Hyundai Dept. Store DUTY FREE(9F)', 'Hyundai Dept. Store DUTY FREE(9F)', '<p><img src=\"http://13.250.225.6/public/findstore/202002171811097eFNJ.jpg\" style=\"width: 320px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181113asYUy.jpg\" style=\"width: 320px;\"><br></p>', '20200128164157bVfIQ.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 4, 'Lotte Duty Free(11F)', 'Lotte Duty Free(11F)', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181146C5lcP.jpg\" style=\"width: 320px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181150RVO0E.jpg\" style=\"width: 320px;\"><br></p>', '20200128164232BukBh.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 5, 'JSM Inspiration EAST', 'JSM Inspiration EAST', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181222VwMhR.jpg\" style=\"width: 600px;\"><br></p>', '<p><img src=\"http://13.250.225.6/public/findstore/20200217181227T5Bep.jpg\" style=\"width: 600px;\"><br></p>', '20200128164331m6yIV.jpg', NULL, 'เคาน์เตอร์ JUNGSAEMMOOL ชั้น G โซน Atrium 2 สยามเซ็นเตอร์', '02-6713-5345', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.55250052108!2d100.5320329146257!3d13.745521403427773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f96ec7a3455%3A0xda6cc6341430e893!2sJungSaemMool%20Pop-up%20Counter%2C%20Siam%20Center!5e0!3m2!1sth!2sth!4v1575213614327!5m2!1sth!2sth\" width=\"1150\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `frontlang`
--

CREATE TABLE `frontlang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_en` text COLLATE utf8mb4_unicode_ci,
  `subject_th` text COLLATE utf8mb4_unicode_ci,
  `summary_en` text COLLATE utf8mb4_unicode_ci,
  `summary_th` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frontlang`
--

INSERT INTO `frontlang` (`id`, `subject_en`, `subject_th`, `summary_en`, `summary_th`, `created_at`, `updated_at`) VALUES
(1, 'JUNGSAEMMOOL', 'จองแซมมุล', 'JUNGSAEMMOOL', 'JUNGSAEMMOOL Thailand', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'BRAND JSM', 'แบรนด์ JSM', 'Brand JUNGSAEMMOOL', 'แบรนด์ JUNGSAEMMOOL', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'ARTIST JSM', 'อาร์ติส JSM', 'Artist JUNGSAEMMOOL', 'อาร์ติส JUNGSAEMMOOL', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'ARTIST TIP', 'ข้อแนะนำอาร์สติก', 'JUNGSAEMMOOL BEAUTY TIP', 'ข้อแนะนำความงาม', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'FIND A STORE', 'สาขาสโตร์', 'FIND A STORE', 'สาขาสโตร์', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'ALL PRODUCTS', 'ผลิตภัณฑ์', 'ALL PRODUCTS', 'ผลิตภัณฑ์', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'BEST SELLERS', 'ผลิตภัณฑ์ขายดี', 'BEST SELLERS', 'ผลิตภัณฑ์ขายดี', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'BASE', 'รองพื้น', 'BASE', 'รองพื้น', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'LIP', 'ลิป', 'LIP', 'ลิป', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 'EYE', 'ตา', 'EYE', 'ตา', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 'SKINCARE', 'สกินแคร์', 'SKINCARE', 'สกินแคร์', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 'TOOL', 'เครื่องมือ', 'TOOL', 'เครื่องมือ', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 'LOUNGE', 'เลาจน์', 'LOUNGE', 'เลาจน์', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 'EVENT', 'อีเว้นท์', 'JUNGSAEMMOOL BEAUTY EVENT', 'อีเว้นท์', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 'NEW ARRIVAL', 'สินค้ามาใหม่', 'NEW ARRIVAL', 'สินค้ามาใหม่', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 'CONTACT', 'ติดต่อ', 'CONTACT', 'ติดต่อ', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 'FAQ', 'คำถามที่พบบ่อย', 'FAQ', 'คำถามที่พบบ่อย', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 'NOTICE', 'แจ้งเตือน', 'NOTICE', 'แจ้งเตือน', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(19, 'Q&A', 'ถาม-ตอบ', 'Q&A', 'ถาม-ตอบ', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 'REVIEW', 'รีวิว', 'REVIEW', 'รีวิวสินค้า', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `linestory`
--

CREATE TABLE `linestory` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `banner_en` text COLLATE utf8mb4_unicode_ci,
  `banner_th` text COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `linestory`
--

INSERT INTO `linestory` (`id`, `name_en`, `name_th`, `banner_en`, `banner_th`, `detail_en`, `detail_th`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Clean Start', NULL, '20191125161034Zcynz.jpg', NULL, NULL, NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Find Your Aura', NULL, '201911251611085RViv.jpg', NULL, NULL, NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'A Double Boosting Suncare', NULL, '20191125161142emHSQ.jpg', NULL, NULL, NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Artist Eye Liner', NULL, '201911251612107YJIc.jpg', NULL, NULL, NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Skin Setting Base', NULL, '20200226160903gBHf4.jpg', NULL, NULL, NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `linestory_description`
--

CREATE TABLE `linestory_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seq` int(11) DEFAULT NULL,
  `linestory_id` int(11) DEFAULT NULL,
  `detail_type` text COLLATE utf8mb4_unicode_ci,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `linestory_description`
--

INSERT INTO `linestory_description` (`id`, `seq`, `linestory_id`, `detail_type`, `img_en`, `img_th`, `is_active`, `created_at`, `updated_at`) VALUES
(77, 1, 1, NULL, '20200426122858rVtAz.jpg', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(78, 1, 2, NULL, '20200426122910WlEzH.jpg', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(79, 1, 3, NULL, '20200426122922wLD6H.jpg', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(80, 1, 4, NULL, '20200426122933r6w1L.jpg', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(81, 1, 5, NULL, '20200426122945XsQep.jpg', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_th` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `uri` text COLLATE utf8mb4_unicode_ci,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name_en`, `name_th`, `parent_id`, `uri`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'JUNGSAEMMOOL', 'JUNGSAEMMOOL', 1, NULL, 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'BRAND JSM', 'BRAND JSM', 1, '/brand', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'ARTIST JSM', 'ARTIST JSM', 1, '/artist', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'ARTIST TIP', 'ARTIST TIP', 1, '/artisttip', 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'FIND A STORE', 'FIND A STORE', 1, '/store', 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'PRODUCT', 'PRODUCT', 2, '/Product/Category/list/cid/0', 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'BEST SELLERS', 'BEST SELLERS', 2, '/Product/Category/list/cid/150', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'BASE', 'BASE', 2, '/Product/Category/list/cid/1', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'LIP', 'LIP', 2, '/Product/Category/list/cid/2', 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 'EYE', 'EYE', 2, '/Product/Category/list/cid/3', 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 'SKINCARE', 'SKINCARE', 2, '/Product/Category/list/cid/4', 6, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 'TOOL', 'TOOL', 2, '/Product/Category/list/cid/5', 7, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 'LOUNGE', 'LOUNGE', 3, NULL, 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 'EVENT', 'EVENT', 3, '/event', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 'NEW ARRIVAL', 'NEW ARRIVAL', 3, '/linestory', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 'CONTACT', 'CONTACT', 4, NULL, 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 'FAQ', 'FAQ', 4, '/faq', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 'NOTICE', 'NOTICE', 4, '/notice', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(19, 'Q&A', 'Q&A', 4, '/qa', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 'REVIEW', 'REVIEW', 4, '/review', 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `menu_lang`
--

CREATE TABLE `menu_lang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_lang`
--

INSERT INTO `menu_lang` (`id`, `menu_id`, `lang_id`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 2, 2, 'brand', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 3, 3, 'artist', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 4, 4, 'artisttip', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 5, 5, 'store', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 6, 6, 'Product/Category/list/cid/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 7, 7, 'Product/Category/list/cid/150', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 8, 8, 'Product/Category/list/cid/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 9, 9, 'Product/Category/list/cid/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(10, 10, 10, 'Product/Category/list/cid/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(11, 11, 11, 'Product/Category/list/cid/4', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(12, 12, 12, 'Product/Category/list/cid/5', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(13, 13, 13, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(14, 14, 14, 'event', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(15, 15, 15, 'linestory', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(16, 16, 16, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(17, 17, 17, 'faq', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(18, 18, 18, 'notice', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(19, 19, 19, 'qa', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 20, 20, 'review', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(21, 2, 2, 'brand/film', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(22, 2, 2, 'brand/magazine', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(23, 2, 2, 'brand/film/media/cid', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(24, 6, 6, 'Product/Category/list/cid/soft/0/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(25, 6, 6, 'Product/Category/list/cid/soft/0/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(26, 6, 6, 'Product/Category/list/cid/soft/0/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(27, 6, 6, 'Product/Category/list/cid/soft/0/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(28, 7, 7, 'Product/Category/list/cid/soft/150/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(29, 7, 7, 'Product/Category/list/cid/soft/150/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(30, 7, 7, 'Product/Category/list/cid/soft/150/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(31, 7, 7, 'Product/Category/list/cid/soft/150/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(32, 8, 8, 'Product/Category/list/cid/soft/1/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(33, 8, 8, 'Product/Category/list/cid/soft/1/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(34, 8, 8, 'Product/Category/list/cid/soft/1/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(35, 8, 8, 'Product/Category/list/cid/soft/1/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(36, 9, 9, 'Product/Category/list/cid/soft/2/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(37, 9, 9, 'Product/Category/list/cid/soft/2/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(38, 9, 9, 'Product/Category/list/cid/soft/2/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(39, 9, 9, 'Product/Category/list/cid/soft/2/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(40, 10, 10, 'Product/Category/list/cid/soft/3/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(41, 10, 10, 'Product/Category/list/cid/soft/3/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(42, 10, 10, 'Product/Category/list/cid/soft/3/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(43, 10, 10, 'Product/Category/list/cid/soft/3/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(44, 11, 11, 'Product/Category/list/cid/soft/4/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(45, 11, 11, 'Product/Category/list/cid/soft/4/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(46, 11, 11, 'Product/Category/list/cid/soft/4/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(47, 11, 11, 'Product/Category/list/cid/soft/4/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(48, 12, 12, 'Product/Category/list/cid/soft/5/1', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(49, 12, 12, 'Product/Category/list/cid/soft/5/2', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(50, 12, 12, 'Product/Category/list/cid/soft/5/3', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(51, 12, 12, 'Product/Category/list/cid/soft/5/0', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(297, '2014_10_12_000000_create_users_table', 1),
(298, '2019_04_27_113639_create_dim_line_user_table', 1),
(299, '2019_08_19_000000_create_failed_jobs_table', 1),
(300, '2019_08_27_084550_create_category_table', 1),
(301, '2019_09_09_140702_create_artisttip_table', 1),
(302, '2019_09_09_141011_create_brand_table', 1),
(303, '2019_10_14_063738_create_admins_table', 1),
(304, '2019_10_23_024537_create_event_table', 1),
(305, '2019_10_23_024744_create_linestory_table', 1),
(306, '2019_10_23_075920_create_notice_table', 1),
(307, '2019_10_30_052105_create_product_table', 1),
(308, '2019_10_30_052158_create_product_detail_table', 1),
(309, '2019_10_30_052227_create_product_description_table', 1),
(310, '2019_10_31_164754_create_menu_table', 1),
(311, '2019_10_31_170205_create_submenu_table', 1),
(312, '2019_11_04_173750_create_brand_jsm_table', 1),
(313, '2019_11_19_105715_create_artist_table', 1),
(314, '2019_11_19_140613_create_artistmag_table', 1),
(315, '2019_11_21_180215_create_table_linestory_description', 1),
(316, '2019_11_29_172902_create_banner_table', 1),
(317, '2019_11_29_172936_create_video_table', 1),
(318, '2019_12_01_183118_create_finestore_table', 1),
(319, '2019_12_02_173630_create_popup_table', 1),
(320, '2019_12_04_171006_create_qa_table', 1),
(321, '2019_12_04_173826_create_qa_answers_table', 1),
(322, '2019_12_06_103022_create_cate_qa_table', 1),
(323, '2019_12_06_142031_create_qarelat_table', 1),
(324, '2019_12_13_161818_create_review', 1),
(325, '2019_12_18_120542_create_admin_menu_table', 1),
(326, '2019_12_18_133133_create_admin_menu_head_table', 1),
(327, '2019_12_18_133925_create_admin_menu_process_table', 1),
(328, '2019_12_20_143447_create_admin_submenu_table', 1),
(329, '2019_12_22_141438_create_frontlang_table', 1),
(330, '2019_12_22_141740_create_menu_lang_table', 1),
(331, '2019_12_23_165723_create_fact_menu_table', 1),
(332, '2020_01_07_111124_create_faq_table', 1),
(333, '2020_01_07_144646_create_faq_category_table', 1),
(334, '2020_01_27_143524_create_store_category_table', 1),
(335, '2020_04_27_212952_create_notice_images_table', 1),
(336, '2020_04_28_152943_create_product_description_th_table', 1),
(337, '2020_05_13_120720_create_verify_users_table', 1),
(338, '2020_05_14_123626_create_sendemail_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `hit` int(11) DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `name`, `content`, `type`, `detail`, `hit`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'JSM', 'JSM Beauty Global Mall Shipping Notice', 'normal', '20191206171857tKeWR.jpg', 3, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'JSM Beauty', 'Shipping notice for Lunar New Year', 'normal', '20200226144003FZ9cZ.jpg', 14, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `notice_images`
--

CREATE TABLE `notice_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notice_id` int(11) DEFAULT NULL,
  `detail_type` longtext COLLATE utf8mb4_unicode_ci,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notice_images`
--

INSERT INTO `notice_images` (`id`, `notice_id`, `detail_type`, `img_en`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '20191206171857tKeWR.jpg', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 2, NULL, '20200226144003FZ9cZ.jpg', '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 1, NULL, '20200427231012mEfoz.jpg', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `popup`
--

CREATE TABLE `popup` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `link` text COLLATE utf8mb4_unicode_ci,
  `user_id` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `popup`
--

INSERT INTO `popup` (`id`, `name_en`, `name_th`, `img_en`, `img_th`, `link`, `user_id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Pop-Up', NULL, '20191211161341.png', NULL, NULL, NULL, 'N', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sku` text COLLATE utf8mb4_unicode_ci,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `img_color` text COLLATE utf8mb4_unicode_ci,
  `img_product` text COLLATE utf8mb4_unicode_ci,
  `product_details_id` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `sku`, `name_en`, `name_th`, `img_color`, `img_product`, `product_details_id`, `stock`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '8809467634917', 'Rosy', NULL, '20200415152850.jpg', '20200415152854.jpg', 1, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(2, '8809467634511', 'Blend', NULL, '20200415153054.jpg', '20200415153058.jpg', 2, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(3, '8809467631145', 'Skin', NULL, '20200415153113.jpg', '20200415153116.jpg', 2, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(4, '8809467631701', 'Contour Palette', NULL, NULL, NULL, 3, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(5, '8809467633828', 'Bronze', NULL, '20200415153225.jpg', '20200415153229.jpg', 4, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(6, '8809467633811', 'Pink', NULL, '20200415153245.jpg', '20200415153248.jpg', 4, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(7, '8809467633804', 'Warm', NULL, '20200415153304.jpg', '20200415153308.jpg', 4, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(8, '8809467634771', 'Fair Light', NULL, '20191216121842.jpg', '20191216121846.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(9, '8809467634788', 'Fair Pink', NULL, '20191216121920.jpg', '20191216121924.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(10, '8809467635341', 'Healthy-Medium', NULL, '20191216122006.JPG', '20191216122010.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(11, '8809467634795', 'Light', NULL, '20191216122047.jpg', '20191216122051.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(12, '8809467634818', 'Medium', NULL, '20191216122121.jpg', '20191216122126.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(13, '8809467634801', 'Pink Light', NULL, '20191216122153.jpg', '20191216122157.jpg', 5, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(14, '8809467634825', 'Fair Light', NULL, '20191216123014.jpg', '20191216123017.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(15, '8809467634832', 'Fair Pink', NULL, '20191216123056.jpg', '20191216123059.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(16, '8809467635358', 'Healthy-Medium', NULL, '20191216123130.jpg', '20191216123133.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(17, '8809467634849', 'Light', NULL, '20191216123207.jpg', '20191216123210.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(18, '8809467634863', 'Medium', NULL, '20191216123244.jpg', '20191216123248.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(19, '8809467634856', 'Pink Light', NULL, '20191216123315.jpg', '20191216123319.jpg', 6, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(20, '8809467631268', 'Fluffy Peach', NULL, '20200415153657.jpg', '20200415153701.jpg', 7, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(21, '8809467634580', 'Ginger Wood', NULL, '20200415153724.jpg', '20200415153729.jpg', 7, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(22, '8809467631244', 'Indi Pink', NULL, '20200415153810.jpg', '20200415153814.jpg', 7, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(23, '8809467631275', 'Pale Lavender', NULL, '20200415153838.jpg', '20200415153842.jpg', 7, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(24, '8809467634597', 'Tan Brown', NULL, '20200415153901.jpg', '20200415153905.jpg', 7, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(25, '8809467630346', 'Pink Glow', NULL, '20200415152336.jpg', '20200415152340.jpg', 8, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(26, '8809467630353', 'Warm Glow', NULL, '20200415152358.jpg', '20200415152402.jpg', 8, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(27, '8809467633088', 'Correcting Green', NULL, '20200415154153.jpg', '20200415154157.jpg', 9, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(28, '8809467633095', 'Correcting Orange', NULL, '20200415154222.jpg', '20200415154227.jpg', 9, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(29, '8809467633057', 'Rosy', NULL, '20200415154250.jpg', '20200415154254.jpg', 9, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(30, '8809467632272', 'Fair Light', NULL, '20191216140839.jpg', '20191216140843.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(31, '8809467633897', 'Fair Pink', NULL, '20191216141008.jpg', '20191216141012.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(32, '8809467630162', 'Light', NULL, '20191216141050.jpg', '20191216141053.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(33, '8809467630179', 'Medium', NULL, '20191216141121.jpg', '20191216141124.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(34, '8809467630186', 'Medium Deep', NULL, '20191216141150.JPG', '20191216141154.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(35, '8809467634559', 'Pink Light', NULL, '20191216141231.jpg', '20191216141235.jpg', 10, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(36, '8809467632784', 'Fair Light', NULL, '20191216143842.jpg', '20191216143845.jpg', 11, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(37, '8809467633903', 'Fair Pink', NULL, '20191216143912.jpg', '20191216143915.jpg', 11, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(38, '8809467630865', 'Light', NULL, '20191216143948.jpg', '20191216143952.jpg', 11, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(39, '8809467630872', 'Medium', NULL, '20191216144019.jpg', '20191216144023.jpg', 11, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(40, '8809467634566', 'Pink Light', NULL, '20191216144054.jpg', '20191216144059.jpg', 11, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(41, '8809467632845', 'Fair Light', NULL, '20191216144913.jpg', '20191216144916.jpg', 12, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(42, '8809467632852', 'Light', NULL, '20191216144945.jpg', '20191216144948.jpg', 12, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(43, '8809467632869', 'Medium', NULL, '20191216145013.jpg', '20191216145016.jpg', 12, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(44, '8809467633491', 'Clear Light', NULL, '20200415152724.jpg', '20200415152727.jpg', 13, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(45, '8809467633507', 'Clear Medium', NULL, '20200415152747.jpg', '20200415152752.jpg', 13, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(46, '8809467632098', 'Fair Light', NULL, '20191216150729.jpg', '20191216150733.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(47, '8809467633866', 'Fair Pink', NULL, '20191216150804.jpg', '20191216150808.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(48, '8809467632036', 'Light', NULL, '20191216150840.jpg', '20191216150844.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(49, '8809467632043', 'Medium', NULL, '20191216150910.jpg', '20191216150913.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(50, '8809467630094', 'Medium Deep', NULL, '20191216150937.jpg', '20191216150940.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(51, '8809467634528', 'Pink Light', NULL, '20191216151008.jpg', '20191216151012.jpg', 14, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(52, '8809467632746', 'Fair Light', NULL, '20191216152036.jpg', '20191216152039.jpg', 15, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(53, '8809467633873', 'Fair Pink', NULL, '20191216152108.jpg', '20191216152112.jpg', 15, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(54, '8809467630100', 'Light', NULL, '20191216152138.jpg', '20191216152142.jpg', 15, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(55, '8809467630117', 'Medium', NULL, '20191216152208.jpg', '20191216152213.jpg', 15, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(56, '8809467634535', 'Pink Light', NULL, '20191216152242.jpg', '20191216152248.jpg', 15, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(57, '8809467634672', 'Glowing Base', NULL, NULL, NULL, 16, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(58, '8809467634665', 'Smoothing Base', NULL, NULL, NULL, 17, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(59, '8809467634689', 'Tone Balancing Base', NULL, NULL, NULL, 18, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(60, '8809467635501', 'Tone Manner Base', NULL, NULL, NULL, 19, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(61, '8809467635419', 'Tone-up Sun Base', NULL, NULL, NULL, 20, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(62, '8809467630520', 'Coral Glow', NULL, '20200415135107.png', '20200415135115.jpg', 21, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(63, '8809467630513', 'Pink Glow', NULL, '20200415135217.png', '20200415135224.jpg', 21, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(64, '8809467635020', 'Posy Glow', NULL, '20200415135257.png', '20200415135301.jpg', 21, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(65, '8809467635013', 'Red Glow', NULL, '20200415135326.png', '20200415135330.jpg', 21, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(66, '8809467634993', 'Brick Moment', NULL, '20200415145227.jpg', '20200415145231.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(67, '8809467632623', 'Carmen Red', NULL, '20200415145325.jpg', '20200415145328.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(68, '8809467632654', 'Damask Rose', NULL, '20200415145356.jpg', '20200415145359.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(69, '8809467632630', 'Extreme Red', NULL, '20200415145428.jpg', '20200415145431.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(70, '8809467635310', 'French Pink', NULL, '20200415145456.jpg', '20200415145501.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(71, '8809467635334', 'Muted Pink', NULL, '20200415145532.jpg', '20200415145535.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(72, '8809467632647', 'Nude Apricot', NULL, '20200415145601.jpg', '20200415145605.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(73, '8809467635327', 'Rose Cosset', NULL, '20200415145632.jpg', '20200415145637.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(74, '8809467635006', 'Youth Rose', NULL, '20200415145659.jpg', '20200415145703.jpg', 22, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(75, '8809467633187', 'Crystal', NULL, '20200415135513.jpg', '20200415135518.jpg', 23, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(76, '8809467633125', 'Red Heel', NULL, '20200415135549.jpg', '20200415135552.jpg', 23, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(77, '8809467633156', 'Rose Petal', NULL, '20200415135623.jpg', '20200415135626.jpg', 23, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(78, '8809467634191', 'Sheer Red', NULL, '20200415135654.jpg', '20200415135700.jpg', 23, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(79, '8809467634207', 'Sheer Rosy', NULL, '20200415135726.jpg', '20200415135730.jpg', 23, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(80, '8809467633231', 'Coral Topic', NULL, '20200415140000.jpg', '20200415140004.jpg', 24, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(81, '8809467633224', 'Feminine Rose', NULL, '20200415140028.jpg', '20200415140034.jpg', 24, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(82, '8809467633194', 'Inside Red', NULL, '20200415140135.png', '20200415140138.jpg', 24, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(83, '8809467633200', 'Red Made', NULL, '20200415140204.png', '20200415140209.jpg', 24, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(84, '8809467635945', 'Force Pink', NULL, '20200415145745.png', '20200415145749.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(85, '8809467635921', 'Healthy Rose', NULL, '20200415145906.png', '20200415145910.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(86, '8809467635860', 'Just Red', NULL, '20200415145938.png', '20200415145941.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(87, '8809467635914', 'Lively Rose', NULL, '20200415150013.png', '20200415150017.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(88, '8809467635969', 'Mood Brick', NULL, '20200415150045.png', '20200415150049.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(89, '8809467635938', 'Morning Pink', NULL, '20200415150126.png', '20200415150129.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(90, '8809467635907', 'Natural Rose', NULL, '20200415150225.png', '20200415150228.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(91, '8809467635976', 'Newtro Chilly', NULL, '20200415150311.png', '20200415150314.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(92, '8809467635884', 'Pure Red', NULL, '20200415150353.png', '20200415150356.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(93, '8809467635952', 'Simply Coral', NULL, '20200415150518.png', '20200415150521.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(94, '8809467635891', 'Steal Red', NULL, '20200415150551.png', '20200415150555.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(95, '8809467635877', 'Temting Red', NULL, '20200415150622.png', '20200415150626.jpg', 25, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(96, '8809467634337', 'Bloom & Petal', NULL, '20200415163933.jpg', '20200415163937.jpg', 26, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(97, '8809467631459', 'Modern & Chic', NULL, '20200415164112.jpg', '20200415164116.jpg', 26, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(98, '8809467631138', 'Neutral & Tender', NULL, '20200415164133.jpg', '20200415164137.jpg', 26, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(99, '8809467633040', 'Tempting & Classy', NULL, '20200415164158.jpg', '20200415164202.jpg', 26, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(100, '8809467634382', 'Kohl Black', NULL, '20200415155018.jpg', '20200415155023.jpg', 27, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(101, '8809467634399', 'Kohl Brown', NULL, '20200415155115.jpg', '20200415155119.jpg', 27, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(102, '8809467634634', 'Auburn Brown', NULL, '20200415163358.jpg', '20200415163402.jpg', 28, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(103, '8809467634610', 'Burned Black', NULL, '20200415163421.jpg', '20200415163425.jpg', 28, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(104, '8809467634627', 'Cassel Brown', NULL, '20200415163453.jpg', '20200415163457.jpg', 28, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(105, '8809467634641', 'Peanut Brown', NULL, '20200415163518.jpg', '20200415163521.jpg', 28, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(106, '8809467635303', 'Coral Lure', NULL, '20200415164342.jpg', '20200415164346.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(107, '8809467634955', 'Flush Up', NULL, '20200415164427.jpg', '20200415164430.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(108, '8809467634931', 'Glorious', NULL, '20200415164454.jpg', '20200415164457.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(109, '8809467635273', 'Lucir', NULL, '20200415164519.jpg', '20200415164522.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(110, '8809467635297', 'Orange Fizz', NULL, '20200415164603.jpg', '20200415164607.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(111, '8809467635280', 'Pink Coddle', NULL, '20200415164634.jpg', '20200415164637.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(112, '8809467634924', 'Plum Bell', NULL, '20200415164701.jpg', '20200415164705.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(113, '8809467634948', 'Some Brown', NULL, '20200415164726.jpg', '20200415164730.jpg', 29, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(114, '8809467630452', 'Brown-Bony', NULL, '20191217112219.jpg', '20191217112222.jpg', 30, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(115, '8809467630490', 'Light-Bony', NULL, '20191217112310.jpg', '20191217112313.jpg', 30, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(116, '8809467630469', 'Maple-Bony', NULL, '20191217112338.jpg', '20191217112342.jpg', 30, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(117, '8809467630476', 'Natural-Bony', NULL, '20191217112414.jpg', '20191217112418.jpg', 30, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(118, '8809467630445', 'Smoky-Bony', NULL, '20191217112444.jpg', '20191217112447.jpg', 30, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(119, '8809467631336', 'All Black', NULL, '20200415160046.jpg', '20200415160051.jpg', 31, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(120, '8809467631343', 'Soul Brown', NULL, '20200415160114.jpg', '20200415160118.jpg', 31, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(121, '8809467631824', 'Addicted To Olive', NULL, '20200415160225.jpg', '20200415160235.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(122, '8809467634368', 'Extra Violet', NULL, '20200415160258.jpg', '20200415160301.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(123, '8809467631794', 'Naked Sand', NULL, '20200415160321.jpg', '20200415160326.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(124, '8809467634344', 'Pink In Joy', NULL, '20200415160345.jpg', '20200415160349.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(125, '8809467631800', 'Saddle Brown', NULL, '20200415160419.jpg', '20200415160423.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(126, '8809467631831', 'Shell Pink Brown', NULL, '20200415160446.jpg', '20200415160450.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(127, '8809467634351', 'Veil Peach', NULL, '20200415160516.jpg', '20200415160521.jpg', 32, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(128, '8809467631848', 'Apparel On', NULL, '20200415165024.jpg', '20200415165028.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(129, '8809467631916', 'Blue Blade', NULL, '20200415165047.jpg', '20200415165051.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(130, '8809467631923', 'Dark Raspberry', NULL, '20200415165112.jpg', '20200415165116.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(131, '8809467631879', 'Faded Rose', NULL, '20200415165141.jpg', '20200415165144.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(132, '8809467631886', 'Fuchsia Fantasy', NULL, '20200415165211.jpg', '20200415165214.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(133, '8809467631855', 'Iris Pop', NULL, '20200415165235.jpg', '20200415165239.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(134, '8809467631893', 'Parrot Green', NULL, '20200415165311.jpg', '20200415165319.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(135, '8809467631909', 'Pumpkin Pop', NULL, '20200415165339.jpg', '20200415165342.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(136, '8809467631862', 'So French Jasmine', NULL, '20200415170054.jpg', '20200415170058.jpg', 33, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(137, '8809467630506', 'Black', NULL, '20200415155615.jpg', '20200415155619.jpg', 34, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(138, '8809467634603', 'Scarlet Brown', NULL, '20200415155638.jpg', '20200415155642.jpg', 34, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(139, '8809467633026', 'All Black', NULL, '20200415155814.jpg', '20200415155818.jpg', 35, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(140, '8809467630803', 'Maple Brown', NULL, '20200415155834.jpg', '20200415155837.jpg', 35, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(141, '8809467634016', 'Cleansing Water', NULL, NULL, NULL, 36, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(142, '8809467634009', 'Creamy Foam', NULL, NULL, NULL, 37, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(143, '8809467634023', 'Lip & Eye Remover', NULL, NULL, NULL, 38, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(144, '8809467630018', 'Enriched Mask', NULL, NULL, NULL, 39, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(145, '8809467630001', 'Enriched Mask Set', NULL, NULL, NULL, 40, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(146, '8809467634160', 'Fitting Mist 120ml', NULL, NULL, NULL, 41, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(147, '8809467634177', 'Fitting Mist 55ml', NULL, NULL, NULL, 42, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(148, '8809467633446', 'Mool Cream 50ml', NULL, NULL, NULL, 43, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(149, '8809467630032', 'Mool Essence', NULL, NULL, NULL, 44, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(150, '8809467634283', 'Radiance Cream', NULL, NULL, NULL, 45, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(151, '8809467632982', 'Mool Toner', NULL, NULL, NULL, 46, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(152, '8809467634467', 'Waterising Start Mask', NULL, NULL, NULL, 47, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(153, '8809467631718', 'Star Eye Cream', NULL, NULL, NULL, 48, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(154, '8809467635525', 'Boosting Toner', NULL, NULL, NULL, 49, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(155, '8809467635563', 'VC Ampoule', NULL, NULL, NULL, 50, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(156, '8809467635594', 'Water-Wrap Cream', NULL, NULL, NULL, 51, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(157, '8809467633934', 'Sun Extreme', NULL, NULL, NULL, 52, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(158, '8809467633927', 'Sun Waterfull', NULL, NULL, NULL, 53, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(159, '8809467632937', 'Artist Angle Sponge', NULL, NULL, NULL, 54, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(160, '8809467630933', 'Artist Brush Blush', NULL, NULL, NULL, 55, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(161, '8809467631060', 'Artist Brush Brow Edge', NULL, NULL, NULL, 56, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(162, '8809467631053', 'Artist Brush Brow Round', NULL, NULL, NULL, 57, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(163, '8809467630964', 'Artist Brush Concealer L', NULL, NULL, NULL, 58, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(164, '8809467630971', 'Artist Brush Concealer M', NULL, NULL, NULL, 59, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(165, '8809467630988', 'Artist Brush Concealer S', NULL, NULL, NULL, 60, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(166, '8809467630902', 'Artist Brush Contour', NULL, NULL, NULL, 61, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(167, '8809467631084', 'Artist Brush Eye Liner', NULL, NULL, NULL, 62, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(168, '8809467631008', 'Artist Brush Eye Shadow L', NULL, NULL, NULL, 63, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(169, '8809467631015', 'Artist Brush Eye Shadow M', NULL, NULL, NULL, 64, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(170, '8809467631022', 'Artist Brush Eye Shadow Point', NULL, NULL, NULL, 65, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(171, '8809467631046', 'Artist Brush Eye Shadow S', NULL, NULL, NULL, 66, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(172, '8809467631039', 'Artist Brush Eye Shadow Smudge', NULL, NULL, NULL, 67, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(173, '8809467630957', 'Artist Brush Foundation', NULL, NULL, NULL, 68, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(174, '8809467630919', 'Artist Brush Hair Line', NULL, NULL, NULL, 69, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(175, '8809467630940', 'Artist Brush Highlight', NULL, NULL, NULL, 70, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(176, '8809467631077', 'Artist Brush Lip', NULL, NULL, NULL, 71, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(177, '8809467630995', 'Artist Brush Nose Contour', NULL, NULL, NULL, 72, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(178, '8809467631206', 'Artist Brush Pouch Set', NULL, NULL, NULL, 73, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(179, '8809467630926', 'Artist Brush Powder', NULL, NULL, NULL, 74, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(180, '8809467633279', 'Artist Brush Powder & Blusher', NULL, NULL, NULL, 75, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(181, '8809467632517', 'Artist Make-Up Blending Tool', NULL, NULL, NULL, 76, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(182, '8809467634658', 'Artist Pencil Sharpener', NULL, NULL, NULL, 77, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(183, '8809467631176', 'Cleansing Cotton Pad', NULL, NULL, NULL, 78, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(184, '8809467635044', 'Cushion Puff', NULL, NULL, NULL, 79, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(185, '8809467632074', 'Star-Cealer Foundation Puff', NULL, NULL, NULL, 80, 10, 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_description`
--

CREATE TABLE `product_description` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seq` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `detail_type` longtext COLLATE utf8mb4_unicode_ci,
  `img_en` text COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_description`
--

INSERT INTO `product_description` (`id`, `seq`, `product_id`, `detail_type`, `img_en`, `img_th`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 1, 2, NULL, '20191216111700NcxRp.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, 2, 2, NULL, '20191216111713sitAh.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(4, 3, 2, NULL, '201912161117227AEbp.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(5, 1, 3, NULL, '20191216112406j5vtA.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(6, 2, 3, NULL, '20191216112420oZPA5.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(7, 3, 3, NULL, '201912161124316ZTL8.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(9, 1, 5, NULL, '20191216121418ECH7M.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(10, 2, 5, NULL, '20191216121426L94GK.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(11, 3, 5, NULL, '20191216121436PA9eM.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(12, 4, 5, NULL, '20191216121443wxlrt.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(13, 5, 5, NULL, '20191216121451L46Yq.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(14, 6, 5, NULL, '20191216121502EqaqC.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(15, 7, 5, NULL, '20191216121512ivyJt.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(16, 8, 5, NULL, '20191216121523S6pAd.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(17, 1, 6, NULL, '20191216122643n49Qf.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(18, 2, 6, NULL, '20191216122650FMoU5.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(19, 3, 6, NULL, '20191216122702byTFC.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(20, 4, 6, NULL, '20191216122709vdaWq.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(21, 5, 6, NULL, '20191216122723SpyQr.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(22, 6, 6, NULL, '20191216122731hBapq.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(23, 7, 6, NULL, '20191216122740lJrmU.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(24, 8, 6, NULL, '20191216122754hzKFH.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(26, 1, 8, NULL, '201912161246592V6L8.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(28, 1, 10, NULL, '20191216140056vgskN.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(29, 2, 10, NULL, '20191216140215Vx0su.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(30, 3, 10, NULL, '2019121614022402AtH.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(31, 4, 10, NULL, '201912161402342Rno3.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(32, 5, 10, NULL, '20191216140248iow2y.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(33, 1, 11, NULL, '20191216143527ggvoW.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(34, 2, 11, NULL, '20191216143538JVWYX.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(35, 3, 11, NULL, '20191216143544N88gr.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(36, 4, 11, NULL, '20191216143551uxijK.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(37, 5, 11, NULL, '2019121614360940UIX.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(38, 1, 12, NULL, '20191216144603UbAAR.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(39, 2, 12, NULL, '201912161446167eIda.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(40, 3, 12, NULL, '20191216144622Rw6dl.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(41, 4, 12, NULL, '201912161446300deD9.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(42, 5, 12, NULL, '20191216144636lkIP8.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(43, 1, 13, NULL, '20191216145315n2Vz7.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(44, 1, 14, NULL, '20191216150428bcOHO.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(45, 2, 14, NULL, '20191216150438XzXC4.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(46, 3, 14, NULL, '20191216150446GpkBJ.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(47, 4, 14, NULL, '201912161505011ZbTs.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(48, 1, 15, NULL, '20191216151835tWJ94.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(49, 2, 15, NULL, '20191216151846R5rWG.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(50, 3, 15, NULL, '20191216151857m1uaL.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(51, 4, 15, NULL, '20191216151903T2Dr3.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(52, 1, 16, NULL, '20191216152703I0ZFI.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(53, 1, 17, NULL, '20191216153353HLMXk.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(54, 1, 18, NULL, '20191216154404yOWRq.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(55, 1, 19, NULL, '201912161547545OyCv.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(56, 1, 20, NULL, '20191216155157AfDx3.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(57, 2, 20, NULL, '201912161552037dotF.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(58, 1, 21, NULL, '201912161646582OOX5.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(59, 2, 21, NULL, '20191216164705XUl0T.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(60, 3, 21, NULL, '20191216164711ftHAG.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(66, 1, 26, NULL, '20191216212603oUHGD.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(67, 1, 27, NULL, '20191216230240ZfaEO.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(68, 1, 28, NULL, '20191216230821eo6vm.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(70, 1, 30, NULL, '20191217111524Irgb7.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(71, 2, 30, NULL, '20191217111532aFgoj.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(72, 3, 30, NULL, '20191217111539s2E3H.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(73, 1, 31, NULL, '20191217112733PfVNO.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(78, 1, 34, NULL, '20191217120024kWPHP.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(79, 1, 35, NULL, '20191217120457n9VIZ.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(80, 1, 36, NULL, '20191217140042PSZft.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(81, 1, 37, NULL, '20191217140459JuMuu.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(82, 1, 38, NULL, '20191217140859OzFv3.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(83, 1, 39, NULL, '20191217141355ZaXRY.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(84, 1, 40, NULL, '20191217141720k9XYa.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(85, 1, 41, NULL, '20191217142144ALNaC.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(86, 2, 41, NULL, '20191217142150o9gLX.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(87, 3, 41, NULL, '20191217142159HmPJq.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(88, 1, 42, NULL, '20191217142520jLxIu.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(89, 2, 42, NULL, '20191217142529ncw5g.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(90, 3, 42, NULL, '20191217142538GGtLm.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(91, 1, 43, NULL, '20191217143418k9RIi.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(92, 2, 43, NULL, '201912171434331JwyZ.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(93, 3, 43, NULL, '201912171434402Ai0N.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(94, 4, 43, NULL, '20191217144814rYNZU.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(95, 1, 44, NULL, '201912171450424dmzr.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(96, 2, 44, NULL, '20191217145101X4moP.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(97, 3, 44, NULL, '20191217145107KJDhp.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(98, 1, 45, NULL, '20191217145514SPtPz.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(99, 1, 46, NULL, '20191217145817Haplh.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(100, 1, 47, NULL, '20191217150204auqg1.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(101, 1, 48, NULL, '20191217150547XwNVi.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(102, 1, 49, NULL, '20191217150931yYkAp.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(103, 1, 50, NULL, '20191217151845570iG.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(104, 2, 50, NULL, '20191217151850Bgnk6.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(105, 3, 50, NULL, '20191217151858hDCBM.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(106, 1, 51, NULL, '20191217152226xJYLx.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(107, 2, 51, NULL, '20191217152231OdVfu.gif', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(108, 3, 51, NULL, '20191217152239blBMQ.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(109, 1, 52, NULL, '20191217152709s4SSX.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(110, 2, 52, NULL, '201912171527128bVJt.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(111, 1, 53, NULL, '20191217153101d51W7.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(112, 2, 53, NULL, '20191217153108zbhcs.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(113, 1, 54, NULL, '20191217172809SEZUW.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(114, 1, 55, NULL, '201912171732383XzvF.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(115, 1, 56, NULL, '20191217173928bZVZ4.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(116, 1, 57, NULL, '20191217174231H17mw.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(117, 1, 58, NULL, '20191217174546ncZGF.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(118, 1, 59, NULL, '201912171748283stjq.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(119, 1, 60, NULL, '20191217175103R6RcI.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(120, 1, 61, NULL, '20191218102843LgUQB.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(121, 1, 62, NULL, '20191218103435JvGWL.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(122, 1, 63, NULL, '20191218110340mb07f.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(123, 1, 64, NULL, '20191218110718DpBDK.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(124, 1, 65, NULL, '201912181118150v2An.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(125, 1, 66, NULL, '20191218113010HRoKP.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(126, 1, 67, NULL, '20191218113612Cmuso.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(127, 1, 68, NULL, '20191218115110d1Se9.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(128, 1, 69, NULL, '20191218115424cFw0u.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(129, 1, 70, NULL, '20191218115721opZm9.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(130, 1, 71, NULL, '20191218115955dxHPl.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(131, 1, 72, NULL, '20191218120241qu04F.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(132, 1, 73, NULL, '20191218120642OcOJB.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(133, 1, 74, NULL, '20191218121014acqrH.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(134, 1, 75, NULL, '20191218121709FlwPy.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(135, 1, 76, NULL, '20191218122113UpWv4.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(136, 1, 77, NULL, '20191218122416NUUgJ.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(137, 1, 78, NULL, '201912181226596h8TW.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(138, 1, 79, NULL, '20191218122940qRFKW.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(139, 1, 80, NULL, '20191218123305Z3bVB.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(142, 1, 1, NULL, '20200120231417GiMWp.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(143, 1, 4, NULL, '2019121611442001xMC.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(144, 1, 7, NULL, '20191216123932RDSV3.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(145, 1, 9, NULL, '20191216125425474cb.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(146, 1, 22, NULL, '20191216170655tkDde.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(147, 1, 23, NULL, '20191216173200h5yah.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(148, 1, 24, NULL, '20191216173956TRv4e.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(149, 1, 25, NULL, '20191216174630oSkuq.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(150, 2, 25, NULL, '201912161746402eAKG.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(151, 1, 29, NULL, '20191217102815x7n8t.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(152, 1, 32, NULL, '20191217113232OMOiA.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(153, 1, 33, NULL, '20191217114813u2Axr.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(154, 2, 33, NULL, '201912171148228mcbA.png', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(155, 3, 33, NULL, '20191217114831zr7c5.jpg', NULL, 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `product_description_th`
--

CREATE TABLE `product_description_th` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seq` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `detail_type` longtext COLLATE utf8mb4_unicode_ci,
  `img_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_description_th`
--

INSERT INTO `product_description_th` (`id`, `seq`, `product_id`, `detail_type`, `img_th`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 1, 2, NULL, '20191216111700NcxRp.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(3, 2, 2, NULL, '20191216111713sitAh.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(4, 3, 2, NULL, '201912161117227AEbp.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(5, 1, 3, NULL, '20191216112406j5vtA.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(6, 2, 3, NULL, '20191216112420oZPA5.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(7, 3, 3, NULL, '201912161124316ZTL8.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(9, 1, 5, NULL, '20191216121418ECH7M.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(10, 2, 5, NULL, '20191216121426L94GK.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(11, 3, 5, NULL, '20191216121436PA9eM.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(12, 4, 5, NULL, '20191216121443wxlrt.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(13, 5, 5, NULL, '20191216121451L46Yq.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(14, 6, 5, NULL, '20191216121502EqaqC.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(15, 7, 5, NULL, '20191216121512ivyJt.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(16, 8, 5, NULL, '20191216121523S6pAd.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(17, 1, 6, NULL, '20191216122643n49Qf.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(18, 2, 6, NULL, '20191216122650FMoU5.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(19, 3, 6, NULL, '20191216122702byTFC.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(20, 4, 6, NULL, '20191216122709vdaWq.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(21, 5, 6, NULL, '20191216122723SpyQr.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(22, 6, 6, NULL, '20191216122731hBapq.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(23, 7, 6, NULL, '20191216122740lJrmU.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(24, 8, 6, NULL, '20191216122754hzKFH.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(26, 1, 8, NULL, '201912161246592V6L8.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(28, 1, 10, NULL, '20191216140056vgskN.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(29, 2, 10, NULL, '20191216140215Vx0su.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(30, 3, 10, NULL, '2019121614022402AtH.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(31, 4, 10, NULL, '201912161402342Rno3.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(32, 5, 10, NULL, '20191216140248iow2y.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(33, 1, 11, NULL, '20191216143527ggvoW.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(34, 2, 11, NULL, '20191216143538JVWYX.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(35, 3, 11, NULL, '20191216143544N88gr.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(36, 4, 11, NULL, '20191216143551uxijK.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(37, 5, 11, NULL, '2019121614360940UIX.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(38, 1, 12, NULL, '20191216144603UbAAR.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(39, 2, 12, NULL, '201912161446167eIda.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(40, 3, 12, NULL, '20191216144622Rw6dl.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(41, 4, 12, NULL, '201912161446300deD9.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(42, 5, 12, NULL, '20191216144636lkIP8.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(43, 1, 13, NULL, '20191216145315n2Vz7.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(44, 1, 14, NULL, '20191216150428bcOHO.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(45, 2, 14, NULL, '20191216150438XzXC4.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(46, 3, 14, NULL, '20191216150446GpkBJ.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(47, 4, 14, NULL, '201912161505011ZbTs.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(48, 1, 15, NULL, '20191216151835tWJ94.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(49, 2, 15, NULL, '20191216151846R5rWG.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(50, 3, 15, NULL, '20191216151857m1uaL.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(51, 4, 15, NULL, '20191216151903T2Dr3.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(52, 1, 16, NULL, '20191216152703I0ZFI.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(53, 1, 17, NULL, '20191216153353HLMXk.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(54, 1, 18, NULL, '20191216154404yOWRq.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(55, 1, 19, NULL, '201912161547545OyCv.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(56, 1, 20, NULL, '20191216155157AfDx3.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(57, 2, 20, NULL, '201912161552037dotF.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(58, 1, 21, NULL, '201912161646582OOX5.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(59, 2, 21, NULL, '20191216164705XUl0T.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(60, 3, 21, NULL, '20191216164711ftHAG.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(66, 1, 26, NULL, '20191216212603oUHGD.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(67, 1, 27, NULL, '20191216230240ZfaEO.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(68, 1, 28, NULL, '20191216230821eo6vm.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(70, 1, 30, NULL, '20191217111524Irgb7.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(71, 2, 30, NULL, '20191217111532aFgoj.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(72, 3, 30, NULL, '20191217111539s2E3H.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(73, 1, 31, NULL, '20191217112733PfVNO.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(78, 1, 34, NULL, '20191217120024kWPHP.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(79, 1, 35, NULL, '20191217120457n9VIZ.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(80, 1, 36, NULL, '20191217140042PSZft.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(81, 1, 37, NULL, '20191217140459JuMuu.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(82, 1, 38, NULL, '20191217140859OzFv3.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(83, 1, 39, NULL, '20191217141355ZaXRY.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(84, 1, 40, NULL, '20191217141720k9XYa.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(85, 1, 41, NULL, '20191217142144ALNaC.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(86, 2, 41, NULL, '20191217142150o9gLX.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(87, 3, 41, NULL, '20191217142159HmPJq.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(88, 1, 42, NULL, '20191217142520jLxIu.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(89, 2, 42, NULL, '20191217142529ncw5g.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(90, 3, 42, NULL, '20191217142538GGtLm.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(91, 1, 43, NULL, '20191217143418k9RIi.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(92, 2, 43, NULL, '201912171434331JwyZ.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(93, 3, 43, NULL, '201912171434402Ai0N.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(94, 4, 43, NULL, '20191217144814rYNZU.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(95, 1, 44, NULL, '201912171450424dmzr.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(96, 2, 44, NULL, '20191217145101X4moP.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(97, 3, 44, NULL, '20191217145107KJDhp.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(98, 1, 45, NULL, '20191217145514SPtPz.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(99, 1, 46, NULL, '20191217145817Haplh.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(100, 1, 47, NULL, '20191217150204auqg1.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(101, 1, 48, NULL, '20191217150547XwNVi.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(102, 1, 49, NULL, '20191217150931yYkAp.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(103, 1, 50, NULL, '20191217151845570iG.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(104, 2, 50, NULL, '20191217151850Bgnk6.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(105, 3, 50, NULL, '20191217151858hDCBM.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(106, 1, 51, NULL, '20191217152226xJYLx.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(107, 2, 51, NULL, '20191217152231OdVfu.gif', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(108, 3, 51, NULL, '20191217152239blBMQ.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(109, 1, 52, NULL, '20191217152709s4SSX.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(110, 2, 52, NULL, '201912171527128bVJt.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(111, 1, 53, NULL, '20191217153101d51W7.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(112, 2, 53, NULL, '20191217153108zbhcs.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(113, 1, 54, NULL, '20191217172809SEZUW.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(114, 1, 55, NULL, '201912171732383XzvF.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(115, 1, 56, NULL, '20191217173928bZVZ4.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(116, 1, 57, NULL, '20191217174231H17mw.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(117, 1, 58, NULL, '20191217174546ncZGF.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(118, 1, 59, NULL, '201912171748283stjq.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(119, 1, 60, NULL, '20191217175103R6RcI.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(120, 1, 61, NULL, '20191218102843LgUQB.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(121, 1, 62, NULL, '20191218103435JvGWL.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(122, 1, 63, NULL, '20191218110340mb07f.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(123, 1, 64, NULL, '20191218110718DpBDK.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(124, 1, 65, NULL, '201912181118150v2An.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(125, 1, 66, NULL, '20191218113010HRoKP.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(126, 1, 67, NULL, '20191218113612Cmuso.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(127, 1, 68, NULL, '20191218115110d1Se9.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(128, 1, 69, NULL, '20191218115424cFw0u.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(129, 1, 70, NULL, '20191218115721opZm9.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(130, 1, 71, NULL, '20191218115955dxHPl.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(131, 1, 72, NULL, '20191218120241qu04F.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(132, 1, 73, NULL, '20191218120642OcOJB.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(133, 1, 74, NULL, '20191218121014acqrH.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(134, 1, 75, NULL, '20191218121709FlwPy.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(135, 1, 76, NULL, '20191218122113UpWv4.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(136, 1, 77, NULL, '20191218122416NUUgJ.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(137, 1, 78, NULL, '201912181226596h8TW.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(138, 1, 79, NULL, '20191218122940qRFKW.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(139, 1, 80, NULL, '20191218123305Z3bVB.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(142, 1, 1, NULL, '20200120231417GiMWp.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(143, 1, 4, NULL, '2019121611442001xMC.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(144, 1, 7, NULL, '20191216123932RDSV3.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(145, 1, 9, NULL, '20191216125425474cb.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(146, 1, 22, NULL, '20191216170655tkDde.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(147, 1, 23, NULL, '20191216173200h5yah.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(148, 1, 24, NULL, '20191216173956TRv4e.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(149, 1, 25, NULL, '20191216174630oSkuq.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(150, 2, 25, NULL, '201912161746402eAKG.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(151, 1, 29, NULL, '20191217102815x7n8t.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(152, 1, 32, NULL, '20191217113232OMOiA.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(153, 1, 33, NULL, '20191217114813u2Axr.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(154, 2, 33, NULL, '201912171148228mcbA.png', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(155, 3, 33, NULL, '20191217114831zr7c5.jpg', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `cover_img` text COLLATE utf8mb4_unicode_ci,
  `cover_zoom` text COLLATE utf8mb4_unicode_ci,
  `display_type` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `size` text COLLATE utf8mb4_unicode_ci,
  `spl_price` decimal(8,2) DEFAULT NULL,
  `detail_th` longtext COLLATE utf8mb4_unicode_ci,
  `detail_en` longtext COLLATE utf8mb4_unicode_ci,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_bestseller` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `name_en`, `name_th`, `cover_img`, `cover_zoom`, `display_type`, `category_id`, `price`, `size`, `spl_price`, `detail_th`, `detail_en`, `start_time`, `end_time`, `is_active`, `is_bestseller`, `created_at`, `updated_at`) VALUES
(1, 'JUNGSAEMMOOL Artist Blush Touch', NULL, '20191216111340ut3S7.jpg', '20200425152451.jpg', 'Y', 1, '1200.00', '3g', NULL, '<p>บลัชเชอร์อบพร้อมชั้นบาง ๆ และสีโปร่งใสที่สร้างแก้มที่ชัดเจนและสำคัญ สูตรพิเศษสูตรอบด้วยมือ แป้งและสารยึดเกาะถูกอบในเตาอบเพื่อให้ได้ประกายสีที่อบนั้น เนื้อเนียนเรียบไปกับผิวและประกายมีความโปร่งใส พิคเม้นต์ช่วยให้แก้มเรียบเนียนดุจสีแดงละลายลงบนผิวช่วยให้สีปัดแก้มที่หลากหลายตามที่คุณต้องการ&nbsp; อ่อนนุ่มและไม่เป็น คราบืช่วยให้เมคอัพติดทนนานเป็นเวลานานด้วยความสวยงามไร้ที่ติ </p><p><br></p><p style=\"text-align: center; \"><img src=\"http://52.74.78.152/public/product/20200428182320Angts.jpg\" style=\"width: 600px;\"><br></p>', '<p>=== ไม่มีข้อมูล ===</p>', '2020-01-11', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(2, 'JUNGSAEMMOOL Artist Concealer Palette', NULL, '20191218175939wRzQ1.jpg', '20200425152509.jpg', 'Y', 1, '1400.00', '6.6g', NULL, '<p>คอนซีลแลอร์ที่เหล่าอาร์ตทิสเลือกใช้ เพื่อการปกปิดและปรับสีผิวที่ไม่ต้องการได้อย่างสมบูรณ์แบบที่สุด มีหลายสีเพื่อเลือกใช้ในการผสม ปกปิดสีผิวได้อย่างหลากหลายตามความต้องการราวกับมืออาชีพด้วยส่วนผสมของ One-Touch Covering Powder คุณสามารถปกปิดความไม่สมบูรณ์ของคุณได้อย่างสมบูรณ์แบบด้วยปริมาณเล็กน้อย<br></p>', '<p>คอนซีลแลอร์ที่เหล่าอาร์ตทิสเลือกใช้ เพื่อการปกปิดและปรับสีผิวที่ไม่ต้องการได้อย่างสมบูรณ์แบบที่สุด มีหลายสีเพื่อเลือกใช้ในการผสม ปกปิดสีผิวได้อย่างหลากหลายตามความต้องการราวกับมืออาชีพด้วยส่วนผสมของ One-Touch Covering Powder คุณสามารถปกปิดความไม่สมบูรณ์ของคุณได้อย่างสมบูรณ์แบบด้วยปริมาณเล็กน้อย<br></p>', '2020-01-11', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(3, 'JUNGSAEMMOOL Artist Contour Palette', NULL, '20191216112159Vpi8s.jpg', '20200425152559.jpg', 'N', 1, '1200.00', '1.9g / 0.06 oz. * 2ea หรือ 3.5g / 0.12 oz. * 3ea', NULL, '<p>Artist Contour Palette</p><p>พาเล็ทคอนทัวร์ที่ช่างแต่งหน้ามืออาชีพแนะนำสำหรับประโยชน์ที่หลากหลาย จากคอนทัวร์กรอบหน้า เขียนคิ้ว เขียนไรผม จนถึงเทคนิคการผสมสีให้ได้สีที่เป็นธรรมชาติยิ่งขึ้น สีที่โปร่งแสงและสว่างขั้นสูงสุด ด้วยเนื้อมุกชิมเมอร์ที่บางเบา โปร่งแสงช่วยให้ผิวดูส่องสว่างโดยไม่หนาเตอะ เนื้อสัมผัสที่นุ่มละมุน ด้วยส่วนผสมจากแร่ธาตุช่วยให้แต่งแต้มผิวได้อย่างเรียบรื่น และผิวไม่มีจุดด่างพร้อย ประสิทธิภาพในการควบคุมความมัน ช่วยซึมซับเหงื่อ ควบคุมความมัน ให้ผิวดูเรียบเนียน</p><p>เกี่ยวกับสินค้า:</p><p>เฉดสีทั้ง 5 ช่วยให้ใช้งานได้อย่างหลากหลาย ตั้งแต่การคอนทัวร์กรอบหน้า, สร้างทรงคิ้ว, เขียนไรผม ความเข้มข้นของสีที่แตกต่างกัน ช่วยให้พาเล็ทนี้เหมาะสำหรับทุกโอกาส</p><p>วิธีใช้:&nbsp;</p><p>ไฮไลท์ (ชิมเมอร์)&nbsp;</p><p>ปัดบริเวณสันจมูก หน้าผาก กระดูกโหนกแก้ม โหนกคิ้ว และส่วนอื่นๆ ที่นูนเด่นขึ้นมา โดยใช้แปรงปัดที่เหมาะสม&nbsp;</p><p>สีเบจ,&nbsp;</p><p>สีน้ำตาลอ่อน (ชิมเมอร์) ผสมสีทั้งสองเข้าด้วยกัน เพื่อให้ได้สีที่เหมาะกับโทนสีผิวของคุณ ทาบริเวณกราม โหนกแก้มด้วยแปรง เพื่อให้โครงหน้าดูมีมิติ&nbsp;</p><p>สีน้ำตาลเข้ม, สีเข้ม เติมเต็มไรผม หรือ เขียนคิ้ว โดยใช้แปรงที่มีขนแปรงสั้นและแข็ง</p><p>เทคนิคจากผู้เชี่ยวชาญ:&nbsp;</p><p>สำหรับการสร้างกรอบหน้า ใช้แปรงที่มีขนแปรงยาว ปัดโดยเคลื่อนที่เป็นวงกลมอย่างเบามือ&nbsp;</p><p>สำหรับการเขียนคิ้ว และเติมไรผม ใช้แปรงที่มีขนแปรงสั้น และแน่น โดยค่อยๆ แตะเพิ่มเบาๆ ที่ละจุด&nbsp;</p><p>ขอแนะนำให้ใช้ Jungsaemmool Artist Brushes เพื่อเพิ่มความสมบูรณ์แบบและความเป็นมืออาชีพในการแต่งหน้า</p>', '<p>Artist Contour Palette</p><p>พาเล็ทคอนทัวร์ที่ช่างแต่งหน้ามืออาชีพแนะนำสำหรับประโยชน์ที่หลากหลาย จากคอนทัวร์กรอบหน้า เขียนคิ้ว เขียนไรผม จนถึงเทคนิคการผสมสีให้ได้สีที่เป็นธรรมชาติยิ่งขึ้น สีที่โปร่งแสงและสว่างขั้นสูงสุด ด้วยเนื้อมุกชิมเมอร์ที่บางเบา โปร่งแสงช่วยให้ผิวดูส่องสว่างโดยไม่หนาเตอะ เนื้อสัมผัสที่นุ่มละมุน ด้วยส่วนผสมจากแร่ธาตุช่วยให้แต่งแต้มผิวได้อย่างเรียบรื่น และผิวไม่มีจุดด่างพร้อย ประสิทธิภาพในการควบคุมความมัน ช่วยซึมซับเหงื่อ ควบคุมความมัน ให้ผิวดูเรียบเนียน</p><p>เกี่ยวกับสินค้า:</p><p>เฉดสีทั้ง 5 ช่วยให้ใช้งานได้อย่างหลากหลาย ตั้งแต่การคอนทัวร์กรอบหน้า, สร้างทรงคิ้ว, เขียนไรผม ความเข้มข้นของสีที่แตกต่างกัน ช่วยให้พาเล็ทนี้เหมาะสำหรับทุกโอกาส</p><p>วิธีใช้:&nbsp;</p><p>ไฮไลท์ (ชิมเมอร์)&nbsp;</p><p>ปัดบริเวณสันจมูก หน้าผาก กระดูกโหนกแก้ม โหนกคิ้ว และส่วนอื่นๆ ที่นูนเด่นขึ้นมา โดยใช้แปรงปัดที่เหมาะสม&nbsp;</p><p>สีเบจ,&nbsp;</p><p>สีน้ำตาลอ่อน (ชิมเมอร์) ผสมสีทั้งสองเข้าด้วยกัน เพื่อให้ได้สีที่เหมาะกับโทนสีผิวของคุณ ทาบริเวณกราม โหนกแก้มด้วยแปรง เพื่อให้โครงหน้าดูมีมิติ&nbsp;</p><p>สีน้ำตาลเข้ม, สีเข้ม เติมเต็มไรผม หรือ เขียนคิ้ว โดยใช้แปรงที่มีขนแปรงสั้นและแข็ง</p><p>เทคนิคจากผู้เชี่ยวชาญ:&nbsp;</p><p>สำหรับการสร้างกรอบหน้า ใช้แปรงที่มีขนแปรงยาว ปัดโดยเคลื่อนที่เป็นวงกลมอย่างเบามือ&nbsp;</p><p>สำหรับการเขียนคิ้ว และเติมไรผม ใช้แปรงที่มีขนแปรงสั้น และแน่น โดยค่อยๆ แตะเพิ่มเบาๆ ที่ละจุด&nbsp;</p><p>ขอแนะนำให้ใช้ Jungsaemmool Artist Brushes เพื่อเพิ่มความสมบูรณ์แบบและความเป็นมืออาชีพในการแต่งหน้า</p>', '2020-01-11', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(4, 'JUNGSAEMMOOL Artist Glow Touch', NULL, '20200120231710I9Wp3.jpg', '20200425152619.jpg', 'Y', 1, '1200.00', '2g', NULL, '<p>ประกายเพิ่มความโดดเด่นให้ใบหน้า สร้างผิวหน้าที่สดใส ส่องสว่างและโดดเด่นมากขึ้น ความสมบูรณ์ของการแต่งหน้า เพื่อสร้างรูปลักษณ์ที่เป็นธรรมชาติของผิว เป็นเทคนิคการอบสีแทนชนิดผง เพื่อให้ประกายนั้น แนบลงบนผิวมากที่สุด ไม่ฟุ้งปลิวหลุดออกจากผิว ผลิตจากประเทศอิตาลี<br></p>', '<p>ประกายเพิ่มความโดดเด่นให้ใบหน้า สร้างผิวหน้าที่สดใส ส่องสว่างและโดดเด่นมากขึ้น ความสมบูรณ์ของการแต่งหน้า เพื่อสร้างรูปลักษณ์ที่เป็นธรรมชาติของผิว เป็นเทคนิคการอบสีแทนชนิดผง เพื่อให้ประกายนั้น แนบลงบนผิวมากที่สุด ไม่ฟุ้งปลิวหลุดออกจากผิว ผลิตจากประเทศอิตาลี<br></p>', '2020-01-11', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(5, 'JUNGSAEMMOOL Cushion-cealer', NULL, '20191216121337Yt4eG.jpg', '20200425152641.jpg', 'Y', 1, '1700.00', 'Cushion 14g*2 Concealer 2g', NULL, '<p>ปกปิด และเปล่งประกายด้วยคูชชั่นระดับมืออาชีพ Jung Saem Mool</p><p>- คูชชั่นที่เน้นการปกปิด พร้อมด้วย SPF 50+ / PA+++ ปกปิดเรียบเนียน แต่บางเบาเสมือนเป็นเนื้อเดียวกับผิว และไม่จับตัวหนาเป็นแป้งเค้ก สูตรป้องกันน้ำและป้องกันเหงื่อ ที่ประสานเข้ากับชั้นผิว ช่วยให้ผิวเรียบเนียน ไร้ตำหนิ แต่บางเบาดุจขนนกตลอดวัน&nbsp;</p><p>- คอนซิลเลอรร์ที่ให้ผิวเปล่งประกาย&nbsp; คอนซิลเลอร์เนื้อบางเบาที่ประกอบด้วย Pro Reflex ช่วยปกปิดข้อบกพร่องบนผิว และไฮไลท์ส่วนที่โดดเด่น</p>', '<p>ปกปิด และเปล่งประกายด้วยคูชชั่นระดับมืออาชีพ Jung Saem Mool</p><p>- คูชชั่นที่เน้นการปกปิด พร้อมด้วย SPF 50+ / PA+++ ปกปิดเรียบเนียน แต่บางเบาเสมือนเป็นเนื้อเดียวกับผิว และไม่จับตัวหนาเป็นแป้งเค้ก สูตรป้องกันน้ำและป้องกันเหงื่อ ที่ประสานเข้ากับชั้นผิว ช่วยให้ผิวเรียบเนียน ไร้ตำหนิ แต่บางเบาดุจขนนกตลอดวัน&nbsp;</p><p>- คอนซิลเลอรร์ที่ให้ผิวเปล่งประกาย&nbsp; คอนซิลเลอร์เนื้อบางเบาที่ประกอบด้วย Pro Reflex ช่วยปกปิดข้อบกพร่องบนผิว และไฮไลท์ส่วนที่โดดเด่น</p>', '2020-01-11', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(6, 'JUNGSAEMMOOL Cushion-cealer Refill', NULL, '20191216122622ySaTt.jpg', '20200425152737.jpg', 'Y', 1, '700.00', '14g', NULL, '<p>ปกปิด และเปล่งประกายด้วยคูชชั่นระดับมืออาชีพ Jung Saem Mool</p><p>- คูชชั่นที่เน้นการปกปิด พร้อมด้วย SPF 50+ / PA+++ ปกปิดเรียบเนียน แต่บางเบาเสมือนเป็นเนื้อเดียวกับผิว และไม่จับตัวหนาเป็นแป้งเค้ก สูตรป้องกันน้ำและป้องกันเหงื่อ ที่ประสานเข้ากับชั้นผิว ช่วยให้ผิวเรียบเนียน ไร้ตำหนิ แต่บางเบาดุจขนนกตลอดวัน&nbsp;</p><p>- คอนซิลเลอรร์ที่ให้ผิวเปล่งประกาย&nbsp; คอนซิลเลอร์เนื้อบางเบาที่ประกอบด้วย Pro Reflex ช่วยปกปิดข้อบกพร่องบนผิว และไฮไลท์ส่วนที่โดดเด่น</p>', '<p>ปกปิด และเปล่งประกายด้วยคูชชั่นระดับมืออาชีพ Jung Saem Mool</p><p>- คูชชั่นที่เน้นการปกปิด พร้อมด้วย SPF 50+ / PA+++ ปกปิดเรียบเนียน แต่บางเบาเสมือนเป็นเนื้อเดียวกับผิว และไม่จับตัวหนาเป็นแป้งเค้ก สูตรป้องกันน้ำและป้องกันเหงื่อ ที่ประสานเข้ากับชั้นผิว ช่วยให้ผิวเรียบเนียน ไร้ตำหนิ แต่บางเบาดุจขนนกตลอดวัน&nbsp;</p><p>- คอนซิลเลอรร์ที่ให้ผิวเปล่งประกาย&nbsp; คอนซิลเลอร์เนื้อบางเบาที่ประกอบด้วย Pro Reflex ช่วยปกปิดข้อบกพร่องบนผิว และไฮไลท์ส่วนที่โดดเด่น</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(7, 'JUNGSAEMMOOL Essential Cheek Blush', NULL, '20200120231839D6X08.jpg', '20200425180106.jpg', 'Y', 1, '650.00', '5.2g', NULL, '<p>บลัชเชอร์อบพร้อมชั้นบาง ๆ และสีโปร่งใสที่สร้างแก้มที่ชัดเจนและสำคัญ สูตรพิเศษสูตรอบด้วยมือ แป้งและสารยึดเกาะถูกอบในเตาอบเพื่อให้ได้ประกายสีที่อบนั้น เนื้อเนียนเรียบไปกับผิวและประกายมีความโปร่งใส พิคเม้นต์ช่วยให้แก้มเรียบเนียนดุจสีแดงละลายลงบนผิวช่วยให้สีปัดแก้มที่หลากหลายตามที่คุณต้องการ&nbsp; อ่อนนุ่มและไม่เป็น คราบืช่วยให้เมคอัพติดทนนานเป็นเวลานานด้วยความสวยงามไร้ที่ติ<br></p>', '<p>บลัชเชอร์อบพร้อมชั้นบาง ๆ และสีโปร่งใสที่สร้างแก้มที่ชัดเจนและสำคัญ สูตรพิเศษสูตรอบด้วยมือ แป้งและสารยึดเกาะถูกอบในเตาอบเพื่อให้ได้ประกายสีที่อบนั้น เนื้อเนียนเรียบไปกับผิวและประกายมีความโปร่งใส พิคเม้นต์ช่วยให้แก้มเรียบเนียนดุจสีแดงละลายลงบนผิวช่วยให้สีปัดแก้มที่หลากหลายตามที่คุณต้องการ&nbsp; อ่อนนุ่มและไม่เป็น คราบืช่วยให้เมคอัพติดทนนานเป็นเวลานานด้วยความสวยงามไร้ที่ติ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(8, 'JUNGSAEMMOOL Essential Powder Illuminator', NULL, '20191216124630zU8Zb.jpg', '20200425152825.jpg', 'Y', 1, '1000.00', '10g', NULL, '<p>รายละเอียดสินค้า JUNGSAEMMOOL Essential Powder Illuminator&nbsp;<br></p><p>แป้งฝุ่นประกายไข่มุกโปร่งแสงติดทนนานที่ผสมเข้ากับผิวอย่างง่ายดายและแก้ไขการแต่งหน้าที่สมบูรณ์แบบ&nbsp; แป้งเนื้อมุกที่เนียนนุ่มเป็นพิเศษ ช่วยแก้ไขการแต่งหน้าและเพิ่มเอฟเฟกต์ที่ติดทนนานโปร่งใสและเนียน ช่วยปกปิดข้อบกพร่องและรูขุมขน และทำให้ผิวเรียบเนียนและกระจ่างใสอย่างสมบูรณ์แบบ</p><div><br></div>', '<p><span style=\"font-size: 1rem;\">รายละเอียดสินค้า JUNGSAEMMOOL Essential Powder Illuminator&nbsp;</span><br></p><p>แป้งฝุ่นประกายไข่มุกโปร่งแสงติดทนนานที่ผสมเข้ากับผิวอย่างง่ายดายและแก้ไขการแต่งหน้าที่สมบูรณ์แบบ&nbsp; แป้งเนื้อมุกที่เนียนนุ่มเป็นพิเศษ ช่วยแก้ไขการแต่งหน้าและเพิ่มเอฟเฟกต์ที่ติดทนนานโปร่งใสและเนียน ช่วยปกปิดข้อบกพร่องและรูขุมขน และทำให้ผิวเรียบเนียนและกระจ่างใสอย่างสมบูรณ์แบบ</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(9, 'JUNGSAEMMOOL Essential Skin Correcting Brightner', NULL, '20200120232006Dj68b.jpg', '20200425180059.jpg', 'Y', 1, '950.00', '2g', NULL, '<p>ปรับความสว่างให้ผิวหน้า บริเวณ star -zone ซึ่งใช้แก้ไขโทนสีผิว แปรงสามารถปรับปริมาณการใช้ได้อย่างลงตัว&nbsp; แก้ไขจุดบกพร่อง&nbsp; ตามจุดต่างๆที่ต้องการ ด้วยเนื้อที่เนียนกลืนไปกับผิวอย่างเป็นธรรมชาติ<br></p>', '<p>ปรับความสว่างให้ผิวหน้า บริเวณ star -zone ซึ่งใช้แก้ไขโทนสีผิว แปรงสามารถปรับปริมาณการใช้ได้อย่างลงตัว&nbsp; แก้ไขจุดบกพร่อง&nbsp; ตามจุดต่างๆที่ต้องการ ด้วยเนื้อที่เนียนกลืนไปกับผิวอย่างเป็นธรรมชาติ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(10, 'JUNGSAEMMOOL Essential Skin Nuder Cushion', NULL, '20191216140044hnUOU.jpg', '20200425152900.jpg', 'Y', 1, '1400.00', '14g*2', NULL, '<p>JUNGSAEMMOOL Essential Skin Nuder Cushion (SPF50+/ PA+++)</p><p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึง โทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิวด้วยการสร้างฟิล์มบาง ๆ บนผิวทาให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนานผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุ่มชื่นตลอดวัน</p>', '<p>JUNGSAEMMOOL Essential Skin Nuder Cushion (SPF50+/ PA+++)</p><p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึง โทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิวด้วยการสร้างฟิล์มบาง ๆ บนผิวทาให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนานผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุ่มชื่นตลอดวัน</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(11, 'JUNGSAEMMOOL Essential Skin Nuder Cushion Refill', NULL, '201912161435131HxmW.jpg', '20200425173205.jpg', 'Y', 1, '600.00', '14g', NULL, '<p>JUNGSAEMMOOL Essential Skin Nuder Cushion (SPF50+/ PA+++)</p><p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึง โทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิวด้วยการสร้างฟิล์มบาง ๆ บนผิวทาให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนานผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุ่มชื่นตลอดวัน</p>', '<p><span style=\"font-size: 1rem;\">JUNGSAEMMOOL Essential Skin Nuder Cushion (SPF50+/ PA+++)</span><br></p><p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึง โทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิวด้วยการสร้างฟิล์มบาง ๆ บนผิวทาให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนานผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุ่มชื่นตลอดวัน</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(12, 'JUNGSAEMMOOL Essential Skin Nuder Long Wear Cushion', NULL, '20191216144539gfUkA.jpg', '20200425173221.jpg', 'Y', 1, '1400.00', '14g*2', NULL, '<p>รองพื้นคูชชั่นที่ให้เนื้อสัมผัสนุ่มละมุน ซึมเข้าเป็นเนื้อเดียวกับผิวอย่างอ่อนโยน และติดทนนาน&nbsp;</p><p>- ลุคแมท ด้วยสูตรพิเศษที่ช่วยสร้างสมดุลระหว่างความชุ่มชื้น และน้ำมัน จึงได้ลุคที่เรียบเนียนดุจใยไหม สอดประสานกับความเชี่ยวชาญของเมคอัพอาร์ทติส ทำให้ได้สูตรพิเศษที่ปกปิดอย่างอ่อนโยนได้ลุคซาติน</p><p>- ความสมบูรณ์แบบของผิวที่ชุ่มชื้นและเปล่งประกาย Seed extracts และ White flower extracts ช่วยให้ผิวดูเปล่งประกาย&nbsp;</p><p>- สูตรปราศจากรอยด่างดำ เนื้อแป้งจากซิลิโคนช่วยปกปิดอย่างเรียบเนียน โดยไม่ลดเลือน หรือจางลง&nbsp;</p><p>- พัฟแต่งหน้าที่ช่วยเบลนด์สี และสร้างเนื้อสัมผัสใหม่ได้ดั่งใจ&nbsp;</p>', '<p>รองพื้นคูชชั่นที่ให้เนื้อสัมผัสนุ่มละมุน ซึมเข้าเป็นเนื้อเดียวกับผิวอย่างอ่อนโยน และติดทนนาน&nbsp;</p><p>- ลุคแมท ด้วยสูตรพิเศษที่ช่วยสร้างสมดุลระหว่างความชุ่มชื้น และน้ำมัน จึงได้ลุคที่เรียบเนียนดุจใยไหม สอดประสานกับความเชี่ยวชาญของเมคอัพอาร์ทติส ทำให้ได้สูตรพิเศษที่ปกปิดอย่างอ่อนโยนได้ลุคซาติน</p><p>- ความสมบูรณ์แบบของผิวที่ชุ่มชื้นและเปล่งประกาย Seed extracts และ White flower extracts ช่วยให้ผิวดูเปล่งประกาย&nbsp;</p><p>- สูตรปราศจากรอยด่างดำ เนื้อแป้งจากซิลิโคนช่วยปกปิดอย่างเรียบเนียน โดยไม่ลดเลือน หรือจางลง&nbsp;</p><p>- พัฟแต่งหน้าที่ช่วยเบลนด์สี และสร้างเนื้อสัมผัสใหม่ได้ดั่งใจ&nbsp;</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(13, 'JUNGSAEMMOOL Essential Smooth Finish Pact', NULL, '20191216145303H9U7a.jpg', '20200425173241.jpg', 'Y', 1, '1100.00', '12g', NULL, '<p>แป้งโปร่งแสงเนื้อแคชเมียร์ แป้งฝุ่นเนื้อนุ่มบางเบาพร้อมเนื้อสัมผัสที่ชุ่มชื้นจะถูกนำไปใช้กับผิวอย่างอ่อนโยน ติดทนและควบคุมความมัน อนุภาคผงแป้งละเอียดจะสร้างชั้นเคลือบทับรองพื้น แตะเนื้อแปรงด้วยแปรงที่ให้มาและปรับปริมาณผงจากจานสีผิว กวาดบนเบา ๆ หลังจากทารองพื้น ด้วยเทคนิค WET &amp; DRY การแต่งหน้าใช้งานได้ยาวนานตลอดทั้งวัน</p><div><br></div>', '<p>แป้งโปร่งแสงเนื้อแคชเมียร์ แป้งฝุ่นเนื้อนุ่มบางเบาพร้อมเนื้อสัมผัสที่ชุ่มชื้นจะถูกนำไปใช้กับผิวอย่างอ่อนโยน ติดทนและควบคุมความมัน อนุภาคผงแป้งละเอียดจะสร้างชั้นเคลือบทับรองพื้น แตะเนื้อแปรงด้วยแปรงที่ให้มาและปรับปริมาณผงจากจานสีผิว กวาดบนเบา ๆ หลังจากทารองพื้น ด้วยเทคนิค WET &amp; DRY การแต่งหน้าใช้งานได้ยาวนานตลอดทั้งวัน</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(14, 'JUNGSAEMMOOL Essential Star-cealer Foundation', NULL, '20191216150412Fp3Wo.jpg', '20200425173254.jpg', 'Y', 1, '1400.00', 'Foundation 15g+Concealer 4.5g', NULL, '<p>คุชชั่น เพื่อมอบผิวสวยธรมมชาติที่สุด ในแบบของ JUNGSAEMMOOL คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกันผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Hue balancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้งโพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบาง ๆ บนผิวทำให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนาน&nbsp;<br></p>', '<p>คุชชั่น เพื่อมอบผิวสวยธรมมชาติที่สุด ในแบบของ JUNGSAEMMOOL คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ&nbsp;<span style=\"font-size: 1rem;\">Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกันผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Hue balancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้งโพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบาง ๆ บนผิวทำให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนาน&nbsp;</span><br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(15, 'JUNGSAEMMOOL Essential Star-cealer Foundation – Foundation Refill', NULL, '20191216151737llE4T.jpg', '20200425173312.jpg', 'Y', 1, '900.00', '15g', NULL, '<p>คุชชั่น เพื่อมอบผิวสวยธรมมชาติที่สุด ในแบบของ JUNGSAEMMOOL คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกันผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Hue balancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้งโพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบาง ๆ บนผิวทำให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนาน&nbsp;<br></p>', '<p>คุชชั่น เพื่อมอบผิวสวยธรมมชาติที่สุด ในแบบของ JUNGSAEMMOOL คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ&nbsp;<span style=\"font-size: 1rem;\">Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกันผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Hue balancing powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้วรอย ผสานกับ สกินฟิตติ้งโพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบาง ๆ บนผิวทำให้เม็ดสีและความชุ่มชื้นบนผิว คงอยู่ยาวนาน&nbsp;</span><br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(16, 'JUNGSAEMMOOL Skin Setting Glowing Base', NULL, '20191216152655yhtMl.jpg', '20200425173326.jpg', 'N', 1, '1100.00', '40ml', NULL, '<p>Skin Setting Glowing Base เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี ฉ่ำวาวมีชีวิตชีวา เหมาะสำหรับผิวแพ้ง่าย</p><p>- เตรียมพร้อมสำหรับเมคอัพ: ผิวกระจ่างใส และ ฉ่ำวาว ครีมเนื้อมุกชิมเมอร์ช่วยให้ผิวกระจ่างใส เปลั่งปลั่ง มีส่วนผสมจาก pink flower complex เพิ่มความชุ่มชื้นและบำรุงผิว</p><p>- เตรียมพร้อมความเรียบเนียน: ไฮยาลูโรนิคช่วยเติมผิวให้เรียบเนียน ปราศจากรูขุมขน และเส้นริ้วรอยต่าง ๆ&nbsp;</p><p>- เตรียมพร้อมความชุ่มชื้น: ด้วยส่วนผสมจาก Mineral Complex ช่วยเติมความชุ่มชื้นให้ผิวอย่างสม่ำเสมอ</p>', '<p>Skin Setting Glowing Base เบสที่ช่วยให้ผิวกระจ่างใสสุขภาพดี ฉ่ำวาวมีชีวิตชีวา เหมาะสำหรับผิวแพ้ง่าย</p><p>- เตรียมพร้อมสำหรับเมคอัพ: ผิวกระจ่างใส และ ฉ่ำวาว ครีมเนื้อมุกชิมเมอร์ช่วยให้ผิวกระจ่างใส เปลั่งปลั่ง มีส่วนผสมจาก pink flower complex เพิ่มความชุ่มชื้นและบำรุงผิว</p><p>- เตรียมพร้อมความเรียบเนียน: ไฮยาลูโรนิคช่วยเติมผิวให้เรียบเนียน ปราศจากรูขุมขน และเส้นริ้วรอยต่าง ๆ&nbsp;</p><p>- เตรียมพร้อมความชุ่มชื้น: ด้วยส่วนผสมจาก Mineral Complex ช่วยเติมความชุ่มชื้นให้ผิวอย่างสม่ำเสมอ</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(17, 'JUNGSAEMMOOL Skin Setting Smoothing Base', NULL, '20191216153323wR5To.jpg', '20200425173342.jpg', 'N', 1, '1100.00', '40ml', NULL, '<p>Skin Setting Smoothing Base เมคอัพเบสที่ช่วยให้เครื่องสำอางติดทนนาน พร้อมควบคุมความมัน</p><p>- เตรียมพร้อมสำหรับเมคอัพ: ควบคุมความมัน และช่วยให้เครื่องสำอางติดทนนานด้วย Cotton Seed Extract ซึ่งมีประสิทธิภาพในการควบคุมความมัน ช่วยให้ผิวของคุณดูสมบูรณ์แบบ พร้อมสำหรับการแต่งหน้า โดยไม่ต้องแก้ไขผิว</p><p>- เตรียมพร้อมความเรียบเนียน: ไฮยาลูโรนิคช่วยเติมผิวให้เรียบเนียน ปราศจากรูขุมขน และเส้นริ้วรอยต่าง ๆ&nbsp;</p><p>- เตรียมพร้อมความชุ่มชื้น: ด้วยส่วนผสมจาก Mineral Complex ช่วยเติมความชุ่มชื้นให้ผิวอย่างสม่ำเสมอ</p>', '<p>Skin Setting Smoothing Base เมคอัพเบสที่ช่วยให้เครื่องสำอางติดทนนาน พร้อมควบคุมความมัน</p><p>- เตรียมพร้อมสำหรับเมคอัพ: ควบคุมความมัน และช่วยให้เครื่องสำอางติดทนนานด้วย Cotton Seed Extract ซึ่งมีประสิทธิภาพในการควบคุมความมัน ช่วยให้ผิวของคุณดูสมบูรณ์แบบ พร้อมสำหรับการแต่งหน้า โดยไม่ต้องแก้ไขผิว</p><p>- เตรียมพร้อมความเรียบเนียน: ไฮยาลูโรนิคช่วยเติมผิวให้เรียบเนียน ปราศจากรูขุมขน และเส้นริ้วรอยต่าง ๆ&nbsp;</p><p>- เตรียมพร้อมความชุ่มชื้น: ด้วยส่วนผสมจาก Mineral Complex ช่วยเติมความชุ่มชื้นให้ผิวอย่างสม่ำเสมอ</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(18, 'JUNGSAEMMOOL Skin Setting Tone Balancing Base', NULL, '20191216154344ejdWx.jpg', '20200425173404.jpg', 'N', 1, '1100.00', '40ml', NULL, '<p>ช่วยปรับผิวเรียบเนียน ล็อคความชุ่มชื่นผิวดีเยี่ยม ช่วยเซ็ตผิวก่อนลงรองพื้นได้อย่างสมบูรณ์แบบ เพื่อผิวดูใส สุขภาพดี เมกอัพติดทนตลอดวัน ดุจผิวธรรมชาติที่ดูสมบูรณ์แบบ สูตรเพื่อผิวกระจ่างใส เนียนเรียบ ปกปิดดีเยี่ยม มีส่วนผสมของกันแดดประสิทธิภาพสูง เหมาะสำหรับทุกสภาพผิว ต้องการการปกปิดสูง ป้องกันรังสี UV เนื้อสัมผัสเบาบาง&nbsp;<br></p>', '<p>ช่วยปรับผิวเรียบเนียน ล็อคความชุ่มชื่นผิวดีเยี่ยม ช่วยเซ็ตผิวก่อนลงรองพื้นได้อย่างสมบูรณ์แบบ เพื่อผิวดูใส สุขภาพดี เมกอัพติดทนตลอดวัน ดุจผิวธรรมชาติที่ดูสมบูรณ์แบบ สูตรเพื่อผิวกระจ่างใส เนียนเรียบ ปกปิดดีเยี่ยม มีส่วนผสมของกันแดดประสิทธิภาพสูง เหมาะสำหรับทุกสภาพผิว ต้องการการปกปิดสูง ป้องกันรังสี UV เนื้อสัมผัสเบาบาง&nbsp;<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(19, 'JUNGSAEMMOOL Skin Setting Tone Manner Base', NULL, '201912161547404POgp.jpg', '20200425173416.jpg', 'N', 1, '1100.00', '40ml', NULL, '<p>เบสสำหรับผู้ชายออล อิน วัน ที่ครบทั้งให้ความชุ่มชื้น ปรับสีผิวให้เป็นธรรมชาติ และปกป้องแสงแดด</p><p>- ปรับสีผิวให้เป็นธรรมชาติ ด้วยสีของเบสที่สามารถปรับให้เข้ากับสีผิวของคุณอย่างเป็นธรรมชาติ ปกปิดรอยแดง และช่วยให้ผิวเรียบเนียน</p><p>- สูตรพิเศษ ช่วยเติมน้ำให้ผิวด้วยส่วนประกอบจากน้ำทะเลลึกจากฮาวายในปริมาณสูงถึง 10,000 ppm. ช่วยเติมน้ำและความชุ่มชื้นให้ผิว ทำให้ผิวที่แห้งผากเปล่งประกายและอ่อนเยาว์&nbsp;</p><p>- สูตรควบคุมความมันด้วย Tea Tree Leaf Extract ช่วยควบคุมความมันส่วนเกิน และช่วยให้ผิวนุ่มนวล สดชื่นเป็นระยะเวลานาน&nbsp;</p>', '<p>เบสสำหรับผู้ชายออล อิน วัน ที่ครบทั้งให้ความชุ่มชื้น ปรับสีผิวให้เป็นธรรมชาติ และปกป้องแสงแดด</p><p>- ปรับสีผิวให้เป็นธรรมชาติ ด้วยสีของเบสที่สามารถปรับให้เข้ากับสีผิวของคุณอย่างเป็นธรรมชาติ ปกปิดรอยแดง และช่วยให้ผิวเรียบเนียน</p><p>- สูตรพิเศษ ช่วยเติมน้ำให้ผิวด้วยส่วนประกอบจากน้ำทะเลลึกจากฮาวายในปริมาณสูงถึง 10,000 ppm. ช่วยเติมน้ำและความชุ่มชื้นให้ผิว ทำให้ผิวที่แห้งผากเปล่งประกายและอ่อนเยาว์&nbsp;</p><p>- สูตรควบคุมความมันด้วย Tea Tree Leaf Extract ช่วยควบคุมความมันส่วนเกิน และช่วยให้ผิวนุ่มนวล สดชื่นเป็นระยะเวลานาน&nbsp;</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(20, 'JUNGSAEMMOOL Skin Setting Tone-up Sun Base', NULL, '20191216155134sAxy0.jpg', '20200425173430.jpg', 'N', 1, '1100.00', '35ml', NULL, '<p>เตรียมผิวคุณให้พร้อมก่อนการแต่งหน้า ด้วยเมคอัพเบสโทนชมพู ที่ช่วยปรับโทนสีผิวให้ดูชมพูเป็นธรรมชาติ และช่วยให้การแต่งหน้าติดทนนาน ช่วยให้ผิวที่หมองคล้ำดูกระจ่างใสยิ่งขึ้น พร้อมกันแดดเนื้อบางเบา SPF 50+ PA+++ ไม่เหนียวติดผิว ให้ผิวคุณพร้อมเผชิญแสงแดดในทุกช่วงวัน<br></p>', '<p>เตรียมผิวคุณให้พร้อมก่อนการแต่งหน้า ด้วยเมคอัพเบสโทนชมพู ที่ช่วยปรับโทนสีผิวให้ดูชมพูเป็นธรรมชาติ และช่วยให้การแต่งหน้าติดทนนาน ช่วยให้ผิวที่หมองคล้ำดูกระจ่างใสยิ่งขึ้น พร้อมกันแดดเนื้อบางเบา SPF 50+ PA+++ ไม่เหนียวติดผิว ให้ผิวคุณพร้อมเผชิญแสงแดดในทุกช่วงวัน<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(21, 'JUNGSAEMMOOL Essential Tinted Lip Glow', NULL, '20191216164611ktMNm.jpg', '20200425152707.jpg', 'Y', 2, '600.00', '4g', NULL, '<p>ลิปบาล์มที่ให้ความชุ่มชื้นอย่างสูงที่มีส่วนผสมของน้ำมันที่มีความหนืดสูงที่ให้ความชุ่มชื้นและฟื้นฟูริมฝีปากของคุณโดยสร้างฟิล์มบางๆมาเคลือบ สีธรรมชาติช่วยให้ริมฝีปากมีชีวิตชีวาและฟิลม์เคลือบที่ยืดหยุ่นมอบความชุ่มชื้นอย่างล้ำลึกถึงริมฝีปากแห้ง ให้ริมฝีปากสีชมพูที่เป็นธรรมชาติและเปล่งประกายราวกับว่าสีของดอกไม้อยู่บนริมฝีปากของคุณ&nbsp;<br></p>', '<p>ลิปบาล์มที่ให้ความชุ่มชื้นอย่างสูงที่มีส่วนผสมของน้ำมันที่มีความหนืดสูงที่ให้ความชุ่มชื้นและฟื้นฟูริมฝีปากของคุณโดยสร้างฟิล์มบางๆมาเคลือบ สีธรรมชาติช่วยให้ริมฝีปากมีชีวิตชีวาและฟิลม์เคลือบที่ยืดหยุ่นมอบความชุ่มชื้นอย่างล้ำลึกถึงริมฝีปากแห้ง ให้ริมฝีปากสีชมพูที่เป็นธรรมชาติและเปล่งประกายราวกับว่าสีของดอกไม้อยู่บนริมฝีปากของคุณ&nbsp;<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(22, 'JUNGSAEMMOOL High Tinted Lip Lacquer', NULL, '20200120232405b1huB.jpg', '20200425180139.jpg', 'Y', 2, '750.00', '8.3 ml', NULL, '<p>High Tinted Lip Lacquer&nbsp;</p><p><br></p><p>ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด เพิ่มความชุ่มชื้นให้ริมฝีปาก แต่ไม่เหนียวเหนอะหนะ สีสดชัด และติดทนนาน เพียงแค่แต้มลงบนริมฝีปาก ลิปแลคเกอร์สามารถซึมเข้าสู่ริมฝีปากเป็นเนื้อเดียวกันทันที พร้อมให้ความชุ่มชื้น ฉ่ำวาว และสีคมชัด ทินท์ที่ช่วยบำรุงริมฝีปากด้วยเนื้อกึ่งน้ำกึ่งน้ำมัน ด้วยสูตรพิเศษที่ทำจากสารสกัดธรรมชาติ ช่วยสร้างปราการเก็บกักความชุ่มชื้นให้ริมฝีปากได้ยาวนาน ให้ความรู้สึกเบาสบาย และเรียบลื่น เนื้อสัมผัสที่เคลือบบนริมฝีปากอย่างบางเบา ช่วยให้ริมฝีปากไม่แห้งแตก หรือ หยาบกร้าน&nbsp;</p><p><br></p><p>ส่วนผสม:&nbsp;</p><p><br></p><p>Damask Rosewater&nbsp;</p><p><br></p><p>Calendula Extracts&nbsp;</p><p><br></p><p>Rose hip oil&nbsp;</p><p><br></p><p>Honey Extract&nbsp;</p><p><br></p><p>Propolis Extract</p><p><br></p><p>ชาร์ทสี:&nbsp;</p><p><br></p><p>#Rose Cosset สีคอรัลแห่งความอบอุ่นของฤดูใบไม้ผลิ</p><p><br></p><p>#French Pink สีชมพูแห่งหัวใจพริ้วไหวดุจดั่งดอกไม้แรกแย้ม</p><p><br></p><p>#Muted Pink สีชมพูแห่งความสงบและล้ำลึก</p><p><br></p><p>#Nude Apricot สีนู้ดพีชแห่งความสงบ</p><p><br></p><p>#Damask Rose สีคอรัลกุหลาบที่เข้ากันได้ดีกับแก้มแดงระเรื่อ&nbsp;</p><p><br></p><p>#Youth Rose สีกุหลาบ marsala แห่งรุ่งอรุณในเมืองกรุง</p><p><br></p><p>#Carmen Red สีแดงส้มสดฉ่ำแห่งความสดชื่น</p><p><br></p><p>#Brick Moment สีแดงอิฐแห่งยามเย็นของฤดูใบไม้ร่วง</p><p><br></p><p>#Extreme Red สีแดงเข้มที่สะกดสายตาคุณ&nbsp;</p><p><br></p><p>#Red Heel สีแดงสดใสเย้ายวน</p><p><br></p><p>#Rose Petal สีชมพูกุหลาบสดใส</p><p><br></p><p>#Crystal สีน้ำนมแวววาว</p><p><br></p><p>#Sheer Red สีแดงสดฉ่ำดุจดั่งสายน้ำ</p><p><br></p><p>#Sheer Rosy สีแดงอย่างเป็นธรรมชาติ</p><p><br></p><p>เคล็ดลับ ดีขึ้นเมื่อใช้คู่กัน เมื่อใช้คู่กับ High-Tinted Lip Lacquer #Crystal, จะได้เรียวปากฉ่ำวาวแบบกลิตเตอร์</p><p><br></p><p>วิธีใช้:</p><p><br></p><p>ควรทาริมฝีปากในปริมาณที่มากเพียงพอ</p><p><br></p><p>เคล็ดลับจากผู้เชียวชาญ 01</p><p><br></p><p>\'- ด้านแบน: ถ้าต้องการให้ริมฝีปากดูเอิบอิ่ม ให้ทาลิปสติกตามรูปปากและค่อยๆ แต้มตามร่องริมฝีปากให้เต็มขึ้น&nbsp;</p><p><br></p><p>\'- ด้านเบลนด์: ถ้าต้องการไล่เฉดสีบนริมฝีปากให้มีมิติ ให้ปรับปริมาณลิปสติกที่ทาให้เหมาะสม โดยเริ่มทาจากด้านในของริมฝีปาก</p><p><br></p><p>เคล็ดลับจากผู้เชียวชาญ 02</p><p><br></p><p>สำหรับสี&nbsp; Rose Cosset, French Pink และ Muted Pink ให้เขียนขอบริมฝีปากด้วยขอบแปรงก่อน จากนั้นจึงระบายทั้งริมฝีปากให้เท่ากัน&nbsp;</p><p><br></p>', '<p><span style=\"font-size: 1rem;\">High Tinted Lip Lacquer&nbsp;</span><br></p><p>ลิปแลคเกอร์ที่ให้เม็ดสีสดชัด เพิ่มความชุ่มชื้นให้ริมฝีปาก แต่ไม่เหนียวเหนอะหนะ สีสดชัด และติดทนนาน เพียงแค่แต้มลงบนริมฝีปาก ลิปแลคเกอร์สามารถซึมเข้าสู่ริมฝีปากเป็นเนื้อเดียวกันทันที พร้อมให้ความชุ่มชื้น ฉ่ำวาว และสีคมชัด ทินท์ที่ช่วยบำรุงริมฝีปากด้วยเนื้อกึ่งน้ำกึ่งน้ำมัน&nbsp;<span style=\"font-size: 1rem;\">ด้วยสูตรพิเศษที่ทำจากสารสกัดธรรมชาติ ช่วยสร้างปราการเก็บกักความชุ่มชื้นให้ริมฝีปากได้ยาวนาน ให้ความรู้สึกเบาสบาย และเรียบลื่น เนื้อสัมผัสที่เคลือบบนริมฝีปากอย่างบางเบา ช่วยให้ริมฝีปากไม่แห้งแตก หรือ หยาบกร้าน&nbsp;</span></p><p>ส่วนผสม:&nbsp;</p><p>Damask Rosewater&nbsp;</p><p>Calendula Extracts&nbsp;</p><p>Rose hip oil&nbsp;</p><p>Honey Extract&nbsp;</p><p>Propolis Extract</p><p>ชาร์ทสี:&nbsp;</p><p>#Rose Cosset สีคอรัลแห่งความอบอุ่นของฤดูใบไม้ผลิ</p><p>#French Pink สีชมพูแห่งหัวใจพริ้วไหวดุจดั่งดอกไม้แรกแย้ม</p><p>#Muted Pink สีชมพูแห่งความสงบและล้ำลึก</p><p>#Nude Apricot สีนู้ดพีชแห่งความสงบ</p><p>#Damask Rose สีคอรัลกุหลาบที่เข้ากันได้ดีกับแก้มแดงระเรื่อ&nbsp;</p><p>#Youth Rose สีกุหลาบ marsala แห่งรุ่งอรุณในเมืองกรุง</p><p>#Carmen Red สีแดงส้มสดฉ่ำแห่งความสดชื่น</p><p>#Brick Moment สีแดงอิฐแห่งยามเย็นของฤดูใบไม้ร่วง</p><p>#Extreme Red สีแดงเข้มที่สะกดสายตาคุณ&nbsp;</p><p>#Red Heel สีแดงสดใสเย้ายวน</p><p>#Rose Petal สีชมพูกุหลาบสดใส</p><p>#Crystal สีน้ำนมแวววาว</p><p>#Sheer Red สีแดงสดฉ่ำดุจดั่งสายน้ำ</p><p>#Sheer Rosy สีแดงอย่างเป็นธรรมชาติ</p><p>เคล็ดลับ ดีขึ้นเมื่อใช้คู่กัน เมื่อใช้คู่กับ High-Tinted Lip Lacquer #Crystal, จะได้เรียวปากฉ่ำวาวแบบกลิตเตอร์</p><p>วิธีใช้:</p><p>ควรทาริมฝีปากในปริมาณที่มากเพียงพอ</p><p>เคล็ดลับจากผู้เชียวชาญ 01</p><p>\'- ด้านแบน: ถ้าต้องการให้ริมฝีปากดูเอิบอิ่ม ให้ทาลิปสติกตามรูปปากและค่อยๆ แต้มตามร่องริมฝีปากให้เต็มขึ้น&nbsp;</p><p>\'- ด้านเบลนด์: ถ้าต้องการไล่เฉดสีบนริมฝีปากให้มีมิติ ให้ปรับปริมาณลิปสติกที่ทาให้เหมาะสม โดยเริ่มทาจากด้านในของริมฝีปาก</p><p>เคล็ดลับจากผู้เชียวชาญ 02</p><p>สำหรับสี&nbsp; Rose Cosset, French Pink และ Muted Pink ให้เขียนขอบริมฝีปากด้วยขอบแปรงก่อน จากนั้นจึงระบายทั้งริมฝีปากให้เท่ากัน&nbsp;</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(23, 'JUNGSAEMMOOL High Tinted Lip Lacquer Full Glaze', NULL, '20200120232445iEYIp.jpg', '20200425180149.jpg', 'Y', 2, '750.00', '8.5ml', NULL, '<p>เนื้อเพิ่มความชุ่มชื้นและไม่เหนียวเหนอะหนะเพิ่มความอิ่มตัวของสีและความมันวาวให้ริมฝีปาก โกลว์ลิปแลคเคอร์ด้วยสูตรที่ไม่เหนียวเหนอะหนะให้ความเงางามและความแวววาวอย่างล้นหลามด้วยไข่มุกแวววาวบนริมฝีปากและสีสันสดใส&nbsp;<br></p>', '<p>เนื้อเพิ่มความชุ่มชื้นและไม่เหนียวเหนอะหนะเพิ่มความอิ่มตัวของสีและความมันวาวให้ริมฝีปาก โกลว์ลิปแลคเคอร์ด้วยสูตรที่ไม่เหนียวเหนอะหนะให้ความเงางามและความแวววาวอย่างล้นหลามด้วยไข่มุกแวววาวบนริมฝีปากและสีสันสดใส&nbsp;<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(24, 'JUNGSAEMMOOL High Tinted Lip Lacquer Hyper Matt', NULL, '202001202325235Z8Bg.jpg', '20200425180159.jpg', 'Y', 2, '750.00', '10g', NULL, '<p>ลิปกลอสเนื้อแมทเนื้อบางเบามากให้สีสวยและสดใหม่โดยไม่เลอะเลือน! ช่วยลดความแห้งกร้านและริมฝีปากที่เป็นขุยเพื่อทำให้ผิวด้านของริมฝีปากด้านในขณะที่รักษาริมฝีปากภายในชุ่มชื้นเป็นเวลานาน&nbsp;<br></p>', '<p>ลิปกลอสเนื้อแมทเนื้อบางเบามากให้สีสวยและสดใหม่โดยไม่เลอะเลือน! ช่วยลดความแห้งกร้านและริมฝีปากที่เป็นขุยเพื่อทำให้ผิวด้านของริมฝีปากด้านในขณะที่รักษาริมฝีปากภายในชุ่มชื้นเป็นเวลานาน&nbsp;<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(25, 'JUNGSAEMMOOL Lip-Pression', NULL, '20200120232556Olz1q.jpg', '20200425180207.jpg', 'Y', 2, '950.00', '3.4g', NULL, '<p>ลิปสติก Lip-pression ที่ช่วยเปลี่ยนบุคลิกคุณได้&nbsp;</p><p>ลิปสติกมากกว่า 12 เฉดสี ที่เปลี่ยนอารมณ์ บุคคลิก ความโดดเด่นบนใบหน้าได้ชัดเจน กดปุ่มเพื่อเปิดลิปสติกและวางมุม 40 องศาบนริมฝีปาก กดเริ่มจากขอบปาก ทาให้ทั่วริมฝีปากลิปสติกสีแมท ความเข้มของเม็ดสีชัดเจน บางเบา เรียบเนียนไปกับริมฝีปาก ติดทนนาน เป็นลิปสติกที่ทุกเฉดสีแสดงตัวตนและอารมณ์ บนใบหน้าคุณได้อย่างชัดเจน แท่งลิปสติกที่มีปุ่มกด (One touch container)&nbsp; ปลายลิปสติกขอบตัด 40 องศา ช่วยให้การกดลงบนริมฝีปากแน่น และเรียบเนียน ภายใน 3 วินาที สีทั้งหมด 12 สี ที่ได้ถูกสรรค์สร้างจาก make up artist ชื่อดังให้คุณเลือกใช้ในทุกวันและทุกโอกาส&nbsp;</p>', '<p>ลิปสติก Lip-pression ที่ช่วยเปลี่ยนบุคลิกคุณได้&nbsp;</p><p>ลิปสติกมากกว่า 12 เฉดสี ที่เปลี่ยนอารมณ์ บุคคลิก ความโดดเด่นบนใบหน้าได้ชัดเจน กดปุ่มเพื่อเปิดลิปสติกและวางมุม 40 องศาบนริมฝีปาก กดเริ่มจากขอบปาก ทาให้ทั่วริมฝีปากลิปสติกสีแมท ความเข้มของเม็ดสีชัดเจน บางเบา เรียบเนียนไปกับริมฝีปาก ติดทนนาน เป็นลิปสติกที่ทุกเฉดสีแสดงตัวตนและอารมณ์ บนใบหน้าคุณได้อย่างชัดเจน แท่งลิปสติกที่มีปุ่มกด (One touch container)&nbsp; ปลายลิปสติกขอบตัด 40 องศา ช่วยให้การกดลงบนริมฝีปากแน่น และเรียบเนียน ภายใน 3 วินาที สีทั้งหมด 12 สี ที่ได้ถูกสรรค์สร้างจาก make up artist ชื่อดังให้คุณเลือกใช้ในทุกวันและทุกโอกาส&nbsp;</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(26, 'JUNGSAEMMOOL Artist Eyeshadow Palette', NULL, '201912162125472OZAU.jpg', '20200425174404.jpg', 'Y', 3, '1700.00', '13.6g', NULL, '<p>พาเลทอายแชโดว์ของอาร์ตทิส เพื่อให้การแต่งตาสมบูรณ์แบบที่สุด อายแชโดว์พาเลทที่มีเฉดสีที่หลากหลายเพื่อการแต่งตาเหมาะสำหรับทุกสีผิวและใช้ได้กับทุกรูปตา เนื้อผ้าไหมนุ่ม ผสมด้วยระบบ Skin Fit Binder และผงเคลือบผิว Smooth Melting Coated สูตรซิลกี้ที่ผสมผสานกันอย่างสมบูรณ์แบบติดทนนานบนผิวและสร้างสีสันสดใส แต่งหน้าแบบมิกซ์แอนด์แมช ช่วยให้สามารถสร้างสีที่หลากหลายได้&nbsp; ปกป้องผิวที่บอบบาง ผสมกับน้ำมันโรสฮิปและบัตเตอร์ซาลทำให้ผิวชุ่มชื่นและเรียบเนียน<br></p>', '<p>พาเลทอายแชโดว์ของอาร์ตทิส เพื่อให้การแต่งตาสมบูรณ์แบบที่สุด อายแชโดว์พาเลทที่มีเฉดสีที่หลากหลายเพื่อการแต่งตาเหมาะสำหรับทุกสีผิวและใช้ได้กับทุกรูปตา เนื้อผ้าไหมนุ่ม ผสมด้วยระบบ Skin Fit Binder และผงเคลือบผิว Smooth Melting Coated สูตรซิลกี้ที่ผสมผสานกันอย่างสมบูรณ์แบบติดทนนานบนผิวและสร้างสีสันสดใส แต่งหน้าแบบมิกซ์แอนด์แมช ช่วยให้สามารถสร้างสีที่หลากหลายได้&nbsp; ปกป้องผิวที่บอบบาง ผสมกับน้ำมันโรสฮิปและบัตเตอร์ซาลทำให้ผิวชุ่มชื่นและเรียบเนียน<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(27, 'JUNGSAEMMOOL Artist Kohl Pen Liner', NULL, '20191216230221DVDJl.jpg', '20200425174421.jpg', 'Y', 3, '800.00', '0.4ml', NULL, '<p>เขียนเปลือกตาได้อย่างสวยคม ถูกใจ ในสไตล์ที่เป็นตัวคุณ ด้วยอายไลน์เนอร์ JUNGSAEMMOOL Artist Kohl Pen Liner เฉดสี Kohl Black อายไลน์เนอร์แบบปากกา เนื้อนิ่มเขียนง่าย ให้เส้นที่คมชม พร้อมเนื้อสีที่เนียนละเอียด ติดทนได้ดีตลอดวัน พร้อมการดีไซน์ด้ามจับที่พอเหมาะกับขนาดมือ ช่วยให้แต่งและลากเส้นได้ง่ายตามใจนึก เหมาะอย่างยิ่งกับลุคการแต่งหน้าของสาวเอเซีย&nbsp;</p><p>เขียนเปลือกตาได้อย่างสวยคม ถูกใจ ในสไตล์ที่เป็นตัวคุณ ด้วยอายไลน์เนอร์ JUNGSAEMMOOL Artist Kohl Pen Liner เฉดสี Kohl Brown อายไลน์เนอร์แบบปากกา เนื้อนิ่มเขียนง่าย ให้เส้นที่คมชม พร้อมเนื้อสีที่เนียนละเอียด ติดทนได้ดีตลอดวัน พร้อมการดีไซน์ด้ามจับที่พอเหมาะกับขนาดมือ ช่วยให้แต่งและลากเส้นได้ง่ายตามใจนึก เหมาะอย่างยิ่งกับลุคการแต่งหน้าของสาวเอเซีย&nbsp;</p>', '<p>เขียนเปลือกตาได้อย่างสวยคม ถูกใจ ในสไตล์ที่เป็นตัวคุณ ด้วยอายไลน์เนอร์ JUNGSAEMMOOL Artist Kohl Pen Liner เฉดสี Kohl Black อายไลน์เนอร์แบบปากกา เนื้อนิ่มเขียนง่าย ให้เส้นที่คมชม พร้อมเนื้อสีที่เนียนละเอียด ติดทนได้ดีตลอดวัน พร้อมการดีไซน์ด้ามจับที่พอเหมาะกับขนาดมือ ช่วยให้แต่งและลากเส้นได้ง่ายตามใจนึก เหมาะอย่างยิ่งกับลุคการแต่งหน้าของสาวเอเซีย&nbsp;</p><p>เขียนเปลือกตาได้อย่างสวยคม ถูกใจ ในสไตล์ที่เป็นตัวคุณ ด้วยอายไลน์เนอร์ JUNGSAEMMOOL Artist Kohl Pen Liner เฉดสี Kohl Brown อายไลน์เนอร์แบบปากกา เนื้อนิ่มเขียนง่าย ให้เส้นที่คมชม พร้อมเนื้อสีที่เนียนละเอียด ติดทนได้ดีตลอดวัน พร้อมการดีไซน์ด้ามจับที่พอเหมาะกับขนาดมือ ช่วยให้แต่งและลากเส้นได้ง่ายตามใจนึก เหมาะอย่างยิ่งกับลุคการแต่งหน้าของสาวเอเซีย&nbsp;</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(28, 'JUNGSAEMMOOL Artist Powdery Brow Pencil', NULL, '20191216230809FmH0n.jpg', '20200425174438.jpg', 'Y', 3, '800.00', '2.6g', NULL, '<p>ดินสออัจฉริยะที่มีการผสมผสานที่ละเอียดอ่อนและเข้ากันได้ดีกับผงและดินสอ รังสรรค์คิ้วแบบ 3 in 1 พร้อมดินสอชนิดแข็งด้านหนึ่งและแป้งฝุ่นที่อยู่ด้านอื่น ๆ ทำให้คิ้วเป็นเส้นคิ้วได้อย่างสมบูรณ์แบบ&nbsp;&nbsp;</p><p>ส่วน pencil ดินสอ: ใช้ปลายมุมที่เป็นมุม, ลากเส้นและแต่งคิ้วตามที่คุณต้องการ สูตรของดินสอเขียนคิ้วเนื้อแน่น แบบนี้ทำให้คิ้วดูเป็นธรรมชาติโดยไม่ทำให้เลอะ เติมคิ้วที่ว่างเปล่าโดยใช้ปลายผงและกำหนดความหมายของขอบคิ้วด้วยดินสอ</p><p>ส่วน powder แป้ง: ใช้ปลายผงที่บรรจุไว้ในดินสอเติมในบริเวณที่ว่างของคิ้วเพื่อให้ได้เฉดสีที่เป็นธรรมชาติ&nbsp;</p><p>ส่วน Screw brush แปรง : ช่วยจัดเรียงขนคิ้วให้เป็นธรรมชาติ</p><div><br></div>', '<p>ดินสออัจฉริยะที่มีการผสมผสานที่ละเอียดอ่อนและเข้ากันได้ดีกับผงและดินสอ รังสรรค์คิ้วแบบ 3 in 1 พร้อมดินสอชนิดแข็งด้านหนึ่งและแป้งฝุ่นที่อยู่ด้านอื่น ๆ ทำให้คิ้วเป็นเส้นคิ้วได้อย่างสมบูรณ์แบบ&nbsp;&nbsp;</p><p>ส่วน pencil ดินสอ: ใช้ปลายมุมที่เป็นมุม, ลากเส้นและแต่งคิ้วตามที่คุณต้องการ สูตรของดินสอเขียนคิ้วเนื้อแน่น แบบนี้ทำให้คิ้วดูเป็นธรรมชาติโดยไม่ทำให้เลอะ เติมคิ้วที่ว่างเปล่าโดยใช้ปลายผงและกำหนดความหมายของขอบคิ้วด้วยดินสอ</p><p>ส่วน powder แป้ง: ใช้ปลายผงที่บรรจุไว้ในดินสอเติมในบริเวณที่ว่างของคิ้วเพื่อให้ได้เฉดสีที่เป็นธรรมชาติ&nbsp;</p><p>ส่วน Screw brush แปรง : ช่วยจัดเรียงขนคิ้วให้เป็นธรรมชาติ</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(29, 'JUNGSAEMMOOL Colorpiece Eyeshadow Prism', NULL, '20200120232637MlnpK.jpg', '20200425180241.jpg', 'Y', 3, '700.00', '2.6g', NULL, '<p>JUNGSAEMMOOL Colorpiece Eyesahdow Prism อายแชโดว์สูตรเงางามสูง ให้ประกายแวววาวที่คมชัดยิ่งขึ้นพร้อมอนุภาคละเอียดที่ประกอบด้วยไข่มุกระยิบระยับ ชั้นเคลือบมุกปริซึม แสดงความเปล่งประกาย ทำให้การแต่งหน้าตาติดทนนาน<br></p>', '<p>JUNGSAEMMOOL Colorpiece Eyesahdow Prism อายแชโดว์สูตรเงางามสูง ให้ประกายแวววาวที่คมชัดยิ่งขึ้นพร้อมอนุภาคละเอียดที่ประกอบด้วยไข่มุกระยิบระยับ ชั้นเคลือบมุกปริซึม แสดงความเปล่งประกาย ทำให้การแต่งหน้าตาติดทนนาน<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(30, 'JUNGSAEMMOOL Refining Color-Bony Brow', NULL, '20191217111420z5Sth.jpg', '20200425174501.jpg', 'Y', 3, '650.00', 'Pencil 0.18g+Powder 0.5g', NULL, '<p>ดินสออัจฉริยะที่มีการผสมผสานที่ละเอียดอ่อนและเข้ากันได้ดีกับผงและดินสอ รังสรรค์คิ้วแบบ 3 in 1 พร้อมดินสอชนิดแข็งด้านหนึ่งและแป้งฝุ่นที่อยู่ด้านอื่น ๆ ทำให้คิ้วเป็นเส้นคิ้วได้อย่างสมบูรณ์แบบ&nbsp;&nbsp;</p><p>ส่วน pencil ดินสอ : ใช้ปลายมุมที่เป็นมุม, ลากเส้นและแต่งคิ้วตามที่คุณต้องการ สูตรของดินสอเขียนคิ้วเนื้อแน่น แบบนี้ทำให้คิ้วดูเป็นธรรมชาติโดยไม่ทำให้เลอะ เติมคิ้วที่ว่างเปล่าโดยใช้ปลายผงและกำหนดความหมายของขอบคิ้วด้วยดินสอ</p><p>ส่วน powder แป้ง: ใช้ปลายผงที่บรรจุไว้ในดินสอเติมในบริเวณที่ว่างของคิ้วเพื่อให้ได้เฉดสีที่เป็นธรรมชาติ&nbsp;</p><p>ส่วน Screw brush แปรง : ช่วยจัดเรียงขนคิ้วให้เป็นธรรมชาติ</p>', '<p>ดินสออัจฉริยะที่มีการผสมผสานที่ละเอียดอ่อนและเข้ากันได้ดีกับผงและดินสอ รังสรรค์คิ้วแบบ 3 in 1 พร้อมดินสอชนิดแข็งด้านหนึ่งและแป้งฝุ่นที่อยู่ด้านอื่น ๆ ทำให้คิ้วเป็นเส้นคิ้วได้อย่างสมบูรณ์แบบ&nbsp;&nbsp;</p><p>ส่วน pencil ดินสอ : ใช้ปลายมุมที่เป็นมุม, ลากเส้นและแต่งคิ้วตามที่คุณต้องการ สูตรของดินสอเขียนคิ้วเนื้อแน่น แบบนี้ทำให้คิ้วดูเป็นธรรมชาติโดยไม่ทำให้เลอะ เติมคิ้วที่ว่างเปล่าโดยใช้ปลายผงและกำหนดความหมายของขอบคิ้วด้วยดินสอ</p><p>ส่วน powder แป้ง: ใช้ปลายผงที่บรรจุไว้ในดินสอเติมในบริเวณที่ว่างของคิ้วเพื่อให้ได้เฉดสีที่เป็นธรรมชาติ&nbsp;</p><p>ส่วน Screw brush แปรง : ช่วยจัดเรียงขนคิ้วให้เป็นธรรมชาติ</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(31, 'JUNGSAEMMOOL Refining Edge Eye Pencil', NULL, '201912171127243uLRY.jpg', '20200425174515.jpg', 'Y', 3, '600.00', '0.13g', NULL, '<p>ดินสอเขียนขอบตาที่บางเฉียบเป็นพิเศษสำหรับการใช้งานที่ละเอียดอ่อนและแม่นยำ อายไลเนอร์ขนาด 2 มม. บางเฉียบสร้างเส้นสายที่สวยงามไร้ที่ติ&nbsp;<br></p>', '<p>ดินสอเขียนขอบตาที่บางเฉียบเป็นพิเศษสำหรับการใช้งานที่ละเอียดอ่อนและแม่นยำ อายไลเนอร์ขนาด 2 มม. บางเฉียบสร้างเส้นสายที่สวยงามไร้ที่ติ&nbsp;<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(32, 'JUNGSAEMMOOL Refining Eyeshadow Double', NULL, '20200120232705lwept.jpg', '20200425180251.jpg', 'Y', 3, '1100.00', 'Sparkle 3g+ Paste 4.5g (All 7.5g)', NULL, '<p>JUNGSAEMMOOL Refining Eyeshadow Double อายแชโดว์ดับเบิ้ลซึ่งประกอบด้วยสูตรสองประเภทที่แตกต่างกันและสร้างการแต่งตาแบบมีประกายโดดเด่น มิกซ์แอนด์แมทช์ครีมเนื้อนุ่มและประกายแวววาว มีคุณสมบัติการยึดติดที่หลากหลาย เพื่อให้เหมาะกับลุคของคุณในทุกวัน นวัตกรรมใหม่จากผงมุกช่วยสร้างเอฟเฟกต์ดวงตาที่บอบบางและมีชีวิตชีวา<br></p>', '<p>JUNGSAEMMOOL Refining Eyeshadow Double อายแชโดว์ดับเบิ้ลซึ่งประกอบด้วยสูตรสองประเภทที่แตกต่างกันและสร้างการแต่งตาแบบมีประกายโดดเด่น มิกซ์แอนด์แมทช์ครีมเนื้อนุ่มและประกายแวววาว มีคุณสมบัติการยึดติดที่หลากหลาย เพื่อให้เหมาะกับลุคของคุณในทุกวัน นวัตกรรมใหม่จากผงมุกช่วยสร้างเอฟเฟกต์ดวงตาที่บอบบางและมีชีวิตชีวา<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(33, 'JUNGSAEMMOOL Refining Eyeshadow Triple', NULL, '20200120232750kkoFR.jpg', '20200425180301.jpg', 'Y', 3, '1300.00', 'Sparkle, Shimmer 3g +Paste 4.5g+ Matt1g (All 8.5g)', NULL, '<p>อายแชโดว์ JUNGSAEMMOOL Refining Eyeshadow Triple Dark Raspberry อายแชโดว์ทริปเปิ้ล เพื่อการแต่งหน้าที่ตรงตามใจ ในสไตล์ที่คุณชื่นชอบ ด้วยแรงบันดาลใจของการออกแบบเฉดสี ที่สามารถสื่อถึงตัวตนของผู้หญิงในไสตล์และลุคที่เป็นตัวของตัวเอง สู่อายแชโดว์ที่คุณสามารถ Mix &amp; Macth ได้ตามสไตล์และตัวตน จากเนื้อสีแบบ Paste/Shimmer, Sparkle และ Mat เพื่อการแต่งแต้มรังสรรค์ความงามบนใบหน้าได้ตามใจต้องการ</p><div><br></div>', '<p>อายแชโดว์ JUNGSAEMMOOL Refining Eyeshadow Triple Dark Raspberry อายแชโดว์ทริปเปิ้ล เพื่อการแต่งหน้าที่ตรงตามใจ ในสไตล์ที่คุณชื่นชอบ ด้วยแรงบันดาลใจของการออกแบบเฉดสี ที่สามารถสื่อถึงตัวตนของผู้หญิงในไสตล์และลุคที่เป็นตัวของตัวเอง สู่อายแชโดว์ที่คุณสามารถ Mix &amp; Macth ได้ตามสไตล์และตัวตน จากเนื้อสีแบบ Paste/Shimmer, Sparkle และ Mat เพื่อการแต่งแต้มรังสรรค์ความงามบนใบหน้าได้ตามใจต้องการ</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(34, 'JUNGSAEMMOOL Refining Lash Fit Mascara', NULL, '20191217120009CzZpL.jpg', '20200425174544.jpg', 'Y', 3, '800.00', '9g', NULL, '<p>JUNGSAEMMOOL Refining Lash Fit Mascara มาสคาร่าแบบ Multi-Effect สูตรติดทนนาน&nbsp; แปรงออกแบบพิเศษสำหรับเส้นขอบตาของชาวเอเชีย พอดีกับขนตาที่ติดเข้ากับขนตาได้อย่างสมบูรณ์ ช่วยยกขนตาทุกเส้นเพื่อให้ได้ลอนที่สมบูรณ์แบบโดยไม่ต้องใช้เครื่องมือดัดขนตา<br></p>', '<p>JUNGSAEMMOOL Refining Lash Fit Mascara มาสคาร่าแบบ Multi-Effect สูตรติดทนนาน&nbsp; แปรงออกแบบพิเศษสำหรับเส้นขอบตาของชาวเอเชีย พอดีกับขนตาที่ติดเข้ากับขนตาได้อย่างสมบูรณ์ ช่วยยกขนตาทุกเส้นเพื่อให้ได้ลอนที่สมบูรณ์แบบโดยไม่ต้องใช้เครื่องมือดัดขนตา<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(35, 'JUNGSAEMMOOL Refining Multi Eye Drawing', NULL, '20191217120448387fC.jpg', '20200425174555.jpg', 'Y', 3, '650.00', '1.8g', NULL, '<p>ไฮไลต์และ contour ใบหน้าของคุณในครั้งเดียว! Contour ตลับเดียว ที่อาร์ตทิสแนะนำซึ่งช่วยให้คุณสามารถผสมผสานสีของ \'ไฮไลต์ - เฉดดิ้ง – แรเงา\' เพื่อให้ได้รูปหน้าตาและคิ้วและเส้นผมที่ชัดเจน<br></p>', '<p>ไฮไลต์และ contour ใบหน้าของคุณในครั้งเดียว! Contour ตลับเดียว ที่อาร์ตทิสแนะนำซึ่งช่วยให้คุณสามารถผสมผสานสีของ \'ไฮไลต์ - เฉดดิ้ง – แรเงา\' เพื่อให้ได้รูปหน้าตาและคิ้วและเส้นผมที่ชัดเจน<br></p>', '2020-01-11', '2025-01-25', 'N', 'N', '2020-06-25 08:53:18', '2020-06-25 08:53:18'),
(36, 'JUNGSAEMMOOL Clean Start Dtoxeed Cleansing Water', NULL, '2019121714003285slD.jpg', '20200425173717.jpg', 'N', 4, '1000.00', '250ml', NULL, '<p>ผลิตภัณฑ์ทำความสะอาดเครื่องสำอางค์ สูตรผสมอิมัลชั่นที่เหมาะกับทุกสภาพผิว ใช้สูตรผสมอิมัลชันที่เป็นมิตรกับผิวหนังเพื่อการดูแลที่ยืดหยุ่นและให้ความชุ่มชื้นแก่ผิวสูงสุด&nbsp; ลบแต่ละจุดแต่งหน้าได้อย่างสะอาดหมดจด! ทำความสะอาดเครื่องสำอางสะอาดหมดจดในครั้งเดียว เป็นการใช้ทำความสะอาดชนิดไม่ต้องล้างออกด้วยcleansing อีกครั้งก็ได้ ซึ่งสามารถใช้งานได้ง่ายทุกที่ทุกเวลา<br></p>', '<p>ผลิตภัณฑ์ทำความสะอาดเครื่องสำอางค์ สูตรผสมอิมัลชั่นที่เหมาะกับทุกสภาพผิว ใช้สูตรผสมอิมัลชันที่เป็นมิตรกับผิวหนังเพื่อการดูแลที่ยืดหยุ่นและให้ความชุ่มชื้นแก่ผิวสูงสุด&nbsp; ลบแต่ละจุดแต่งหน้าได้อย่างสะอาดหมดจด! ทำความสะอาดเครื่องสำอางสะอาดหมดจดในครั้งเดียว เป็นการใช้ทำความสะอาดชนิดไม่ต้องล้างออกด้วยcleansing อีกครั้งก็ได้ ซึ่งสามารถใช้งานได้ง่ายทุกที่ทุกเวลา<br></p>', '2020-01-11', '2025-01-25', 'N', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(37, 'JUNGSAEMMOOL Clean Start Dtoxeed Creamy Foam', NULL, '20191217140448UFzrT.jpg', '20200425173736.jpg', 'N', 4, '900.00', '150ml', NULL, '<p>โฟมครีม เนื้อละเอียดหนา นุ่ม สำหรับใช้ทำความสะอาดผิวหน้าอย่างอ่อนโยน&nbsp; สารสกัดจากเมล็ดมะรุม, สารทำความสะอาดที่เป็นลิขสิทธิ์เฉพาะของแบรนด์, เหนือกว่าฟื้นฟูผิวให้กลับมาดูเป็นธรรมชาติและบริสุทธิ์ ฟองโฟม เนื้อเนียนแน่น ทำความสะอาดได้อย่างอ่อนโยน โฟมที่มีอนุภาคละเอียดจะช่วยให้การทำความสะอาดเป็นสองเท่าโดยการทำความสะอาดเครื่องสำอางและฝุ่นละอองที่เกิดขึ้นจากภายนอกเป็นหลัก Foams with fine particles provide double</p><p><br></p>', '<p>โฟมครีม เนื้อละเอียดหนา นุ่ม สำหรับใช้ทำความสะอาดผิวหน้าอย่างอ่อนโยน&nbsp; สารสกัดจากเมล็ดมะรุม, สารทำความสะอาดที่เป็นลิขสิทธิ์เฉพาะของแบรนด์, เหนือกว่าฟื้นฟูผิวให้กลับมาดูเป็นธรรมชาติและบริสุทธิ์ ฟองโฟม เนื้อเนียนแน่น ทำความสะอาดได้อย่างอ่อนโยน โฟมที่มีอนุภาคละเอียดจะช่วยให้การทำความสะอาดเป็นสองเท่าโดยการทำความสะอาดเครื่องสำอางและฝุ่นละอองที่เกิดขึ้นจากภายนอกเป็นหลัก Foams with fine particles provide double</p><div><br></div>', '2020-01-11', '2025-01-25', 'N', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(38, 'JUNGSAEMMOOL Clean Start Dtoxeed Lip & Eye Remover', NULL, '20191217140851GW9e7.jpg', '20200425173751.jpg', 'N', 4, '650.00', '120ml', NULL, '<p>Perfect Lip &amp; Eye Remover ทำความสะอาดเมคอัพที่ดวงตาและริมฝีปากของคุณได้อย่างหมดจดและอ่อนโยน ทำความสะอาดอย่างหมดจด แม้จะเป็นผลิตภัณฑ์สูตร waterproof /longlasting ลดความรู้สึกไม่สบายรอบดวงตา ปลอดภัยสำหรับดวงตา<br></p>', '<p>Perfect Lip &amp; Eye Remover ทำความสะอาดเมคอัพที่ดวงตาและริมฝีปากของคุณได้อย่างหมดจดและอ่อนโยน ทำความสะอาดอย่างหมดจด แม้จะเป็นผลิตภัณฑ์สูตร waterproof /longlasting ลดความรู้สึกไม่สบายรอบดวงตา ปลอดภัยสำหรับดวงตา<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(39, 'JUNGSAEMMOOL Essential Enriched Mask', NULL, '20191217141345RYGcE.jpg', '20200425173815.jpg', 'N', 4, '150.00', '20ml', NULL, '<p>มาส์กที่มีความเข้มข้นสูงผสมกับเอสเซ้นส์น้ำนมไข่มุก ซึ่งซึมซาบสู่ผิวทันทีที่ใช้ และ ช่วยให้ผิวชุ่มชื่นยาวนาน&nbsp; ผสานเข้ากับโปรตีนจากไข่มุก ซึ่งเป็นองค์ประกอบหลักของไข่มุก (โปรตีนไฮโดรไลซ์คอนชิโอลิน) ที่ช่วยเสริมและปกป้องผิวให้ชุ่มชื้น พร้อม 6 วิตามินคอมเพล็กซ์ ที่จะช่วยปรับสีผิวให้กระจ่างใสและยังช่วยฟื้นฟูผิวอีกด้วย<br></p>', '<p>มาส์กที่มีความเข้มข้นสูงผสมกับเอสเซ้นส์น้ำนมไข่มุก ซึ่งซึมซาบสู่ผิวทันทีที่ใช้ และ ช่วยให้ผิวชุ่มชื่นยาวนาน&nbsp; ผสานเข้ากับโปรตีนจากไข่มุก ซึ่งเป็นองค์ประกอบหลักของไข่มุก (โปรตีนไฮโดรไลซ์คอนชิโอลิน) ที่ช่วยเสริมและปกป้องผิวให้ชุ่มชื้น พร้อม 6 วิตามินคอมเพล็กซ์ ที่จะช่วยปรับสีผิวให้กระจ่างใสและยังช่วยฟื้นฟูผิวอีกด้วย<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(40, 'JUNGSAEMMOOL Essential Enriched Mask Set (3 Sheets)', NULL, '20191217141711imCqD.jpg', '20200425173832.jpg', 'N', 4, '400.00', '20ml*3', NULL, '<p>มาส์กที่มีความเข้มข้นสูงผสมกับเอสเซ้นส์น้ำนมไข่มุก ซึ่งซึมซาบสู่ผิวทันทีที่ใช้ และ ช่วยให้ผิวชุ่มชื่นยาวนาน&nbsp; ผสานเข้ากับโปรตีนจากไข่มุก ซึ่งเป็นองค์ประกอบหลักของไข่มุก (โปรตีนไฮโดรไลซ์คอนชิโอลิน) ที่ช่วยเสริมและปกป้องผิวให้ชุ่มชื้น พร้อม 6 วิตามินคอมเพล็กซ์ ที่จะช่วยปรับสีผิวให้กระจ่างใสและยังช่วยฟื้นฟูผิวอีกด้วย<br></p>', '<p>มาส์กที่มีความเข้มข้นสูงผสมกับเอสเซ้นส์น้ำนมไข่มุก ซึ่งซึมซาบสู่ผิวทันทีที่ใช้ และ ช่วยให้ผิวชุ่มชื่นยาวนาน&nbsp; ผสานเข้ากับโปรตีนจากไข่มุก ซึ่งเป็นองค์ประกอบหลักของไข่มุก (โปรตีนไฮโดรไลซ์คอนชิโอลิน) ที่ช่วยเสริมและปกป้องผิวให้ชุ่มชื้น พร้อม 6 วิตามินคอมเพล็กซ์ ที่จะช่วยปรับสีผิวให้กระจ่างใสและยังช่วยฟื้นฟูผิวอีกด้วย<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(41, 'JUNGSAEMMOOL Essential Mool Micro Fitting Mist', NULL, '20191217142131iza4W.jpg', '20200425173847.jpg', 'N', 4, '1100.00', '120ml', NULL, '<p>ขั้นตอนสำคัญสำหรับการแต่งหน้าที่สมบูรณ์แบบ ด้วยส่วนผสมของน้ำพุร้อนคาร์โลวีวารี ถึง 82% ใช้ก่อนแต่งหน้า จะทำให้ผิวมีสัมผัสที่นุ่มนวลและชุ่มชื่น สำหรับการแต่งหน้าที่ง่ายขึ้น&nbsp; บรรจุภัณฑ์ปลอดแก็ส ด้วยสเปรย์น้ำอนุภาคละเอียด น้ำหนักเบา สร้างเกราะป้องกันความชื้นได้ดี ใช้ในขั้นตอนสุดท้ายของการแต่งหน้า อนุภาคละเอียดจะซึมซาบสู่ผิว ให้ผิวหน้าเปล่งประกายเงางามอย่างเป็นธรรมชาติ<br></p>', '<p>ขั้นตอนสำคัญสำหรับการแต่งหน้าที่สมบูรณ์แบบ ด้วยส่วนผสมของน้ำพุร้อนคาร์โลวีวารี ถึง 82% ใช้ก่อนแต่งหน้า จะทำให้ผิวมีสัมผัสที่นุ่มนวลและชุ่มชื่น สำหรับการแต่งหน้าที่ง่ายขึ้น&nbsp; บรรจุภัณฑ์ปลอดแก็ส ด้วยสเปรย์น้ำอนุภาคละเอียด น้ำหนักเบา สร้างเกราะป้องกันความชื้นได้ดี ใช้ในขั้นตอนสุดท้ายของการแต่งหน้า อนุภาคละเอียดจะซึมซาบสู่ผิว ให้ผิวหน้าเปล่งประกายเงางามอย่างเป็นธรรมชาติ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(42, 'JUNGSAEMMOOL Essential Mool Micro Fitting Mist', NULL, '20191217142512nQTAo.jpg', '20200425173900.jpg', 'N', 4, '700.00', '55ml', NULL, '<p>ขั้นตอนสำคัญสำหรับการแต่งหน้าที่สมบูรณ์แบบ ด้วยส่วนผสมของน้ำพุร้อนคาร์โลวีวารี ถึง 82% ใช้ก่อนแต่งหน้า จะทำให้ผิวมีสัมผัสที่นุ่มนวลและชุ่มชื่น สำหรับการแต่งหน้าที่ง่ายขึ้น&nbsp; บรรจุภัณฑ์ปลอดแก็ส ด้วยสเปรย์น้ำอนุภาคละเอียด น้ำหนักเบา สร้างเกราะป้องกันความชื้นได้ดี ใช้ในขั้นตอนสุดท้ายของการแต่งหน้า อนุภาคละเอียดจะซึมซาบสู่ผิว ให้ผิวหน้าเปล่งประกายเงางามอย่างเป็นธรรมชาติ<br></p>', '<p>ขั้นตอนสำคัญสำหรับการแต่งหน้าที่สมบูรณ์แบบ ด้วยส่วนผสมของน้ำพุร้อนคาร์โลวีวารี ถึง 82% ใช้ก่อนแต่งหน้า จะทำให้ผิวมีสัมผัสที่นุ่มนวลและชุ่มชื่น สำหรับการแต่งหน้าที่ง่ายขึ้น&nbsp; บรรจุภัณฑ์ปลอดแก็ส ด้วยสเปรย์น้ำอนุภาคละเอียด น้ำหนักเบา สร้างเกราะป้องกันความชื้นได้ดี ใช้ในขั้นตอนสุดท้ายของการแต่งหน้า อนุภาคละเอียดจะซึมซาบสู่ผิว ให้ผิวหน้าเปล่งประกายเงางามอย่างเป็นธรรมชาติ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19');
INSERT INTO `product_details` (`id`, `name_en`, `name_th`, `cover_img`, `cover_zoom`, `display_type`, `category_id`, `price`, `size`, `spl_price`, `detail_th`, `detail_en`, `start_time`, `end_time`, `is_active`, `is_bestseller`, `created_at`, `updated_at`) VALUES
(43, 'JUNGSAEMMOOL Essential Mool Cream', NULL, '20191217143406fivDl.jpg', '20200425173914.jpg', 'N', 4, '1900.00', '50ml', NULL, '<p>ครีมบำรุงผิวสูตรพิเศษที่มอบความชุ่มชื้นสูงสู่ผิว เนื้อสัมผัสที่มีความบางเบา เมื่อเวลาผ่านไป คุณจะรู้สึกเหมือนใส่เอสเซ้นส์อ่อน ๆ แต่ผิวของคุณจะชุ่มชื่นราวกับว่าปกคลุมด้วยชั้นมอยซ์เจอไรเซอร์สูงบนผิว ด้วย Lecithin เลซิตินจะทาหน้าเสมือนโครงสร้างตาข่ายเสริมสร้างให้ผิวดูแข็งแรง เก็บกักความชุ่มชื้นยาวนาน ด้วย Squalene มอบเนื้อสัมผัสเนียนนุ่มบางเบาจนผิวคุณรู้สึกสดชื่นชุ่มชื่นทันทีที่ใช้ ด้วยส่วนผสมหลักจาก Fermented Hydrolyzed Pearl Extract และ White Flower Complex ดอกไม้สีขาวถึง 8 ชนิดทั่วโลก ได้แก่ ดอกบัวขาว , ไอริส, ดอกมะลิ, ลิลลี่, เอเดลไวส์, กุหลาบขาว, ดอกฟรีเซีย และ ดอกแดฟโฟดิล ฟื้นฟูผิวให้กระจ่างใส ทำให้การแต่งหน้าเป็นเรื่องง่าย มอบผิวเรียบเนียน บางเบาและเป็นธรรมชาติยิ่งขึ้น<br></p>', '<p>ครีมบำรุงผิวสูตรพิเศษที่มอบความชุ่มชื้นสูงสู่ผิว เนื้อสัมผัสที่มีความบางเบา เมื่อเวลาผ่านไป คุณจะรู้สึกเหมือนใส่เอสเซ้นส์อ่อน ๆ แต่ผิวของคุณจะชุ่มชื่นราวกับว่าปกคลุมด้วยชั้นมอยซ์เจอไรเซอร์สูงบนผิว ด้วย Lecithin เลซิตินจะทาหน้าเสมือนโครงสร้างตาข่ายเสริมสร้างให้ผิวดูแข็งแรง เก็บกักความชุ่มชื้นยาวนาน ด้วย Squalene มอบเนื้อสัมผัสเนียนนุ่มบางเบาจนผิวคุณรู้สึกสดชื่นชุ่มชื่นทันทีที่ใช้ ด้วยส่วนผสมหลักจาก Fermented Hydrolyzed Pearl Extract และ White Flower Complex ดอกไม้สีขาวถึง 8 ชนิดทั่วโลก ได้แก่ ดอกบัวขาว , ไอริส, ดอกมะลิ, ลิลลี่, เอเดลไวส์, กุหลาบขาว, ดอกฟรีเซีย และ ดอกแดฟโฟดิล ฟื้นฟูผิวให้กระจ่างใส ทำให้การแต่งหน้าเป็นเรื่องง่าย มอบผิวเรียบเนียน บางเบาและเป็นธรรมชาติยิ่งขึ้น<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(44, 'JUNGSAEMMOOL Essential Mool Essence', NULL, '20191217145033KwVMG.jpg', '20200425173928.jpg', 'N', 4, '1200.00', '30ml', NULL, '<p>ครีมบำรุงผิวสูตรพิเศษที่มอบความชุ่มชื้นสูงสู่ผิว เนื้อสัมผัสที่มีความบางเบา เมื่อเวลาผ่านไป คุณจะรู้สึกเหมือนใส่เอสเซ้นส์อ่อน ๆ แต่ผิวของคุณจะชุ่มชื่นราวกับว่าปกคลุมด้วยชั้นมอยซ์เจอไรเซอร์สูงบนผิว ด้วย Lecithin เลซิตินจะทาหน้าเสมือนโครงสร้างตาข่ายเสริมสร้างให้ผิวดูแข็งแรง เก็บกักความชุ่มชื้นยาวนาน ด้วย Squalene มอบเนื้อสัมผัสเนียนนุ่มบางเบาจนผิวคุณรู้สึกสดชื่นชุ่มชื่นทันทีที่ใช้ ด้วยส่วนผสมหลักจาก Fermented Hydrolyzed Pearl Extract และ White Flower Complex ดอกไม้สีขาวถึง 8 ชนิดทั่วโลก ได้แก่ ดอกบัวขาว , ไอริส, ดอกมะลิ, ลิลลี่, เอเดลไวส์, กุหลาบขาว, ดอกฟรีเซีย และ ดอกแดฟโฟดิล ฟื้นฟูผิวให้กระจ่างใส ทำให้การแต่งหน้าเป็นเรื่องง่าย มอบผิวเรียบเนียน บางเบาและเป็นธรรมชาติยิ่งขึ้น<br></p>', '<p>ครีมบำรุงผิวสูตรพิเศษที่มอบความชุ่มชื้นสูงสู่ผิว เนื้อสัมผัสที่มีความบางเบา เมื่อเวลาผ่านไป คุณจะรู้สึกเหมือนใส่เอสเซ้นส์อ่อน ๆ แต่ผิวของคุณจะชุ่มชื่นราวกับว่าปกคลุมด้วยชั้นมอยซ์เจอไรเซอร์สูงบนผิว ด้วย Lecithin เลซิตินจะทาหน้าเสมือนโครงสร้างตาข่ายเสริมสร้างให้ผิวดูแข็งแรง เก็บกักความชุ่มชื้นยาวนาน ด้วย Squalene มอบเนื้อสัมผัสเนียนนุ่มบางเบาจนผิวคุณรู้สึกสดชื่นชุ่มชื่นทันทีที่ใช้ ด้วยส่วนผสมหลักจาก Fermented Hydrolyzed Pearl Extract และ White Flower Complex ดอกไม้สีขาวถึง 8 ชนิดทั่วโลก ได้แก่ ดอกบัวขาว , ไอริส, ดอกมะลิ, ลิลลี่, เอเดลไวส์, กุหลาบขาว, ดอกฟรีเซีย และ ดอกแดฟโฟดิล ฟื้นฟูผิวให้กระจ่างใส ทำให้การแต่งหน้าเป็นเรื่องง่าย มอบผิวเรียบเนียน บางเบาและเป็นธรรมชาติยิ่งขึ้น<br></p>', '2020-01-11', '2025-01-25', 'N', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(45, 'JUNGSAEMMOOL Essential Mool Radiance Cream', NULL, '20191217145506caebO.jpg', '20200425174010.jpg', 'N', 4, '1700.00', '50ml', NULL, '<p>ครีมบำรุงล้ำลึก ที่ช่วยมอบความชุ่มชื้นให้กับผิวหน้า ด้วยคุณสมบัติการผสานความชุ่มชื้นลงลึกสู่ผิวทั้ง 3 ชั้น ให้คุณรู้สึกถึงความนุ่ม เรียบเนียนขึ้น พร้อมช่วยปรับสีผิวให้เนียนสวย และสว่างกระจ่างใสขึ้น อย่างเป็นธรรมชาติ เหมาะอย่างยิ่งกับผู้ที่ต้องการดูแลและฟื้นฟูผิวหน้าจากมลภาวะต่างๆ ให้กระจ่างใส<br></p>', '<p>ครีมบำรุงล้ำลึก ที่ช่วยมอบความชุ่มชื้นให้กับผิวหน้า ด้วยคุณสมบัติการผสานความชุ่มชื้นลงลึกสู่ผิวทั้ง 3 ชั้น ให้คุณรู้สึกถึงความนุ่ม เรียบเนียนขึ้น พร้อมช่วยปรับสีผิวให้เนียนสวย และสว่างกระจ่างใสขึ้น อย่างเป็นธรรมชาติ เหมาะอย่างยิ่งกับผู้ที่ต้องการดูแลและฟื้นฟูผิวหน้าจากมลภาวะต่างๆ ให้กระจ่างใส<br></p>', '2020-01-11', '2025-01-25', 'N', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(46, 'JUNGSAEMMOOL Essential Mool Toner', NULL, '20191217145809SqP3M.jpg', '20200425174038.jpg', 'N', 4, '800.00', '200ml', NULL, '<p>โทนเนอร์ที่มอบความชุ่มชื่นให้กับผิว พร้อมช่วยให้ผิวเรียบเนียนยิ่งขึ้น&nbsp; มีส่วนผสมของมอย์เจอร์ไรชิ่งเข้มข้น ผสานส่วนผสมจากสารกสัดของดอกไม้สีขาว ช่วยให้ผิวเรียบเนียน ดูกระจ่างใส เปล่งประกายดุจไข่มุก เนื้อเจลบางเบา ช่วยขจัดสิ่งสกปรกที่อยู่บนผิวได้อย่างอ่อนโยน โดยไม่ก่อให้เกิดการระคายเคืองผิว ช่วยให้ผิวชุ่มชื่น เปล่งปลั่ง เนียนเรียบกว่าที่เคย<br></p>', '<p>โทนเนอร์ที่มอบความชุ่มชื่นให้กับผิว พร้อมช่วยให้ผิวเรียบเนียนยิ่งขึ้น&nbsp; มีส่วนผสมของมอย์เจอร์ไรชิ่งเข้มข้น ผสานส่วนผสมจากสารกสัดของดอกไม้สีขาว ช่วยให้ผิวเรียบเนียน ดูกระจ่างใส เปล่งประกายดุจไข่มุก เนื้อเจลบางเบา ช่วยขจัดสิ่งสกปรกที่อยู่บนผิวได้อย่างอ่อนโยน โดยไม่ก่อให้เกิดการระคายเคืองผิว ช่วยให้ผิวชุ่มชื่น เปล่งปลั่ง เนียนเรียบกว่าที่เคย<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(47, 'JUNGSAEMMOOL Essential Mool Waterising Start Mask (5 Sheets)', NULL, '20191217150155OpFvi.jpg', '20200425174050.jpg', 'N', 4, '900.00', '28ml*5ea', NULL, '<p>Waterising Start Mask จะมอบความสดชื่นและความเปล่งปลั่งให้กับผิวที่แห้งกร้านก่อนบำรุงผิวและแต่งหน้า จุดเด่นความชุ่มชื้นเปล่งประกายจากด้านในผิว!&nbsp;</p><p>Waterising Start Mask ที่สร้างกำแพงความชื้นจากผิวด้านใน&nbsp; เติมความชุ่มชื้นให้ผิว! ด้วย Waterising effect ให้ความชุ่มชื้นตลอดทั้งวัน!&nbsp;</p>', '<p>Waterising Start Mask จะมอบความสดชื่นและความเปล่งปลั่งให้กับผิวที่แห้งกร้านก่อนบำรุงผิวและแต่งหน้า จุดเด่นความชุ่มชื้นเปล่งประกายจากด้านในผิว!&nbsp;</p><p>Waterising Start Mask ที่สร้างกำแพงความชื้นจากผิวด้านใน&nbsp; เติมความชุ่มชื้นให้ผิว! ด้วย Waterising effect ให้ความชุ่มชื้นตลอดทั้งวัน!&nbsp;</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(48, 'JUNGSAEMMOOL Essential Star Eye Cream', NULL, '2019121715053604fnF.jpg', '20200425174102.jpg', 'N', 4, '1400.00', '20ml', NULL, '<p>อายครีมเนื้อบางเบาสำหรับการแต่งหน้าที่ดีกว่า&nbsp; ครีมบำรุงรอบดวงตาสำหรับใช้เป็นประจำทุกวัน มีส่วนผสมของผงมุกละเอียด และ โปรตีนจากไข่มุก ที่ช่วยให้ผิวรอบดวงตากระชับและจ่างใสขึ้น ด้วยสูตรที่เหมาะกับทุกสภาพผิว เพื่อช่วยให้ผิวมีความยืดหยุ่นและให้ความชุ่มชื่นแก่ผิว ด้วยเนื้อบางเบาแต่ให้ความชุ่มชื้นแก่ผิวสูง สารสกัด Phytoact golden silk oil drop ซึ่งประกอบด้วยสารบำรุงและให้ความชุ่มชื่น ที่เป็นสารอาหารแก่ผิวผงไข่มุกที่มอบประกายระยิบระยับ ให้ผิวดูกระจ่างใส เรียบเนียน สม่ำเสมอ<br></p>', '<p>อายครีมเนื้อบางเบาสำหรับการแต่งหน้าที่ดีกว่า&nbsp; ครีมบำรุงรอบดวงตาสำหรับใช้เป็นประจำทุกวัน มีส่วนผสมของผงมุกละเอียด และ โปรตีนจากไข่มุก ที่ช่วยให้ผิวรอบดวงตากระชับและจ่างใสขึ้น ด้วยสูตรที่เหมาะกับทุกสภาพผิว เพื่อช่วยให้ผิวมีความยืดหยุ่นและให้ความชุ่มชื่นแก่ผิว ด้วยเนื้อบางเบาแต่ให้ความชุ่มชื้นแก่ผิวสูง สารสกัด Phytoact golden silk oil drop ซึ่งประกอบด้วยสารบำรุงและให้ความชุ่มชื่น ที่เป็นสารอาหารแก่ผิวผงไข่มุกที่มอบประกายระยิบระยับ ให้ผิวดูกระจ่างใส เรียบเนียน สม่ำเสมอ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(49, 'JUNGSAEMMOOL Minifying Boosting Toner', NULL, '201912171509231bijx.jpg', '20200425174117.jpg', 'N', 4, '1100.00', '200ml', NULL, '<p>โทนเนอร์สูตรอ่อนโยนแบบน้ำ ที่ให้ความชุ่มชื้นแก่ผิวของคุณได้ง่ายและรวดเร็ว&nbsp;</p><p>Refreshing hydration ช่วยรักษาระดับความชุ่มชื้นและความยืดหยุ่นของผิวคุณในระดับสูง&nbsp; สูตรกรดอ่อนช่วยปรับสมดุลความมันของน้ำมันและระดับความชื้นโดยไม่ระคายเคือง</p><p>Lactobionic acid ทำความสะอาดส่วนที่ตกค้างจากการล้างหน้าและช่วยขจัดเซลล์ผิวที่ตายแล้วออกอย่างอ่อนโยน ขัดผิวอย่างอ่อนโยนอยู่ในระดับที่เหมาะสมสำหรับการเพิ่มความชุ่มชื้นในระยะต่อไป</p>', '<p>โทนเนอร์สูตรอ่อนโยนแบบน้ำ ที่ให้ความชุ่มชื้นแก่ผิวของคุณได้ง่ายและรวดเร็ว&nbsp;</p><p>Refreshing hydration ช่วยรักษาระดับความชุ่มชื้นและความยืดหยุ่นของผิวคุณในระดับสูง&nbsp; สูตรกรดอ่อนช่วยปรับสมดุลความมันของน้ำมันและระดับความชื้นโดยไม่ระคายเคือง</p><p>Lactobionic acid ทำความสะอาดส่วนที่ตกค้างจากการล้างหน้าและช่วยขจัดเซลล์ผิวที่ตายแล้วออกอย่างอ่อนโยน ขัดผิวอย่างอ่อนโยนอยู่ในระดับที่เหมาะสมสำหรับการเพิ่มความชุ่มชื้นในระยะต่อไป</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(50, 'JUNGSAEMMOOL Minifying VC Ampoule', NULL, '20191217151832nYcii.jpg', '20200425174134.jpg', 'N', 4, '1900.00', '30ml', NULL, '<p>JUNGSAEMMOOL Minifying Boosting VC Ampoule</p><p>แคปซูลวิตามินแอมเพิล ที่มีความเข้มข้นมากกว่าเซรั่ม จะช่วยให้ผิวเก็บกักความชุ่มชื้นได้ดี เพื่อผิวที่กระจ่างใสอย่างที่สุด เก็บกักความชุ่มชื้นในชั้นผิว จากกรดไฮยาลูโรนิก 5 ชั้นจะซึมซับล้ำลึกสู่ผิวของคุณ ด้วยความชุ่มชื้นอย่างเข้มข้นพร้อมเก็บความชุ่มชื้นไว้ในชั้นผิว ผิวกระจ่างใสมีชีวิตชีวา แอมเพิลจะช่วยเพิ่มความชุ่มชื้น กระจ่างใส โดยลดความหมองคล้ำของผิวจากวิตามินซี ในแคปซูล&nbsp;&nbsp;</p><div><br></div>', '<p>JUNGSAEMMOOL Minifying Boosting VC Ampoule</p><p>แคปซูลวิตามินแอมเพิล ที่มีความเข้มข้นมากกว่าเซรั่ม จะช่วยให้ผิวเก็บกักความชุ่มชื้นได้ดี เพื่อผิวที่กระจ่างใสอย่างที่สุด เก็บกักความชุ่มชื้นในชั้นผิว จากกรดไฮยาลูโรนิก 5 ชั้นจะซึมซับล้ำลึกสู่ผิวของคุณ ด้วยความชุ่มชื้นอย่างเข้มข้นพร้อมเก็บความชุ่มชื้นไว้ในชั้นผิว ผิวกระจ่างใสมีชีวิตชีวา แอมเพิลจะช่วยเพิ่มความชุ่มชื้น กระจ่างใส โดยลดความหมองคล้ำของผิวจากวิตามินซี ในแคปซูล&nbsp;&nbsp;</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(51, 'JUNGSAEMMOOL Minifying Water-Wrap Cream', NULL, '20191217152214qBdlE.jpg', '20200425174148.jpg', 'N', 4, '1500.00', '50ml', NULL, '<p>ครีมบำรุงที่ช่วยปลอบประโลมผิว ห่อหุ้มผิวแห้งกร้านของคุณไว้ด้วยความชุ่มชื้น&nbsp; สารสกัดสดจาก organic French buds ช่วยปลอบประโลมผิวที่บอบบางและแพ้ง่าย&nbsp; ให้ความชุ่มชื้นอย่างล้ำลึก ครีมฟื้นฟูสภาพผิวด้วยความชุ่มชื้นที่อุดมสมบูรณ์และให้ผิวสบายโดยไม่เหนียวเหนอะหนะ&nbsp; เสมือนน้ำหุ้มผิวไว้ เนื้อครีม BABY NMF เสมือนความชุ่มชื้นของผิวทารก ที่ห่อหุ้มผิวด้วยความชุ่มชื้น โดยปราศจากความแห้งกร้าน ล็อคความชุ่มชื้นไว้อย่างแน่นหนา ยาวนาน สูตรเนื้อบางเบา ไม่เหนียวเหนอะหนะ ทำให้ผิวชุ่มชื้น<br></p>', '<p>ครีมบำรุงที่ช่วยปลอบประโลมผิว ห่อหุ้มผิวแห้งกร้านของคุณไว้ด้วยความชุ่มชื้น&nbsp; สารสกัดสดจาก organic French buds ช่วยปลอบประโลมผิวที่บอบบางและแพ้ง่าย&nbsp; ให้ความชุ่มชื้นอย่างล้ำลึก ครีมฟื้นฟูสภาพผิวด้วยความชุ่มชื้นที่อุดมสมบูรณ์และให้ผิวสบายโดยไม่เหนียวเหนอะหนะ&nbsp; เสมือนน้ำหุ้มผิวไว้ เนื้อครีม BABY NMF เสมือนความชุ่มชื้นของผิวทารก ที่ห่อหุ้มผิวด้วยความชุ่มชื้น โดยปราศจากความแห้งกร้าน ล็อคความชุ่มชื้นไว้อย่างแน่นหนา ยาวนาน สูตรเนื้อบางเบา ไม่เหนียวเหนอะหนะ ทำให้ผิวชุ่มชื้น<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(52, 'JUNGSAEMMOOL Pre-tect Sun Extreme', NULL, '20191217152701KLhfH.jpg', '20200425174202.jpg', 'N', 4, '1250.00', '70ml', NULL, '<p>ครีมกันแดด 2 ประสิทธิภาพ ไม่เพียงแค่การช่วยปกป้องผิวจากรังสี uv แต่ยังช่วยทำให้ผิวหน้าเรียบเนียน เตรียมพร้อมผิวก่อนการแต่งหน้าได้อย่างดีที่สุด&nbsp; ปกป้องผิวจากรังสี uv ดีที่สุด แม้ผิวที่บอบบาง เตรียมผิวสำหรับการแต่งหน้า บำรุงผิว ทำให้ผิวสดชื่น ต่อเนื่องจากขั้นตอนของสกินแคร์ แม้วันที่อากาศร้อนอบอ้าว ปกป้องแสงแดดได้อย่างสูงสุดแ แม้ต้องป้องกันผิวในกิจกรรมกลางแจ้ง<br></p>', '<p>ครีมกันแดด 2 ประสิทธิภาพ ไม่เพียงแค่การช่วยปกป้องผิวจากรังสี uv แต่ยังช่วยทำให้ผิวหน้าเรียบเนียน เตรียมพร้อมผิวก่อนการแต่งหน้าได้อย่างดีที่สุด&nbsp; ปกป้องผิวจากรังสี uv ดีที่สุด แม้ผิวที่บอบบาง เตรียมผิวสำหรับการแต่งหน้า บำรุงผิว ทำให้ผิวสดชื่น ต่อเนื่องจากขั้นตอนของสกินแคร์ แม้วันที่อากาศร้อนอบอ้าว ปกป้องแสงแดดได้อย่างสูงสุดแ แม้ต้องป้องกันผิวในกิจกรรมกลางแจ้ง<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(53, 'JUNGSAEMMOOL Pre-tect Sun Waterfull', NULL, '20191217153053MPbNW.jpg', '20200425174214.jpg', 'N', 4, '1350.00', '50ml', NULL, '<p>ครีมกันแดดสองประสิทธิภาพ Waterfull ไม่เพียงแต่ช่วยปกป้องผิวจากรังสี UV ที่ทำร้ายผิว แต่ยังช่วยทำให้ผิวหน้าดูเรียบเนียน เตรียมพร้อมผิวสำหรับการแต่งหน้าในขั้นตอนต่อไป&nbsp;</p><p>\'- ปกป้องแสงแดดอย่างสูง ด้วยนวัตกรรม PA++++ ที่ถูกพัฒนาอย่างดี Waterfull ช่วยป้องกันผิวจากการทำร้ายของรังสี UV</p><p>\'- ให้ความชุ่มชื้น และให้ผิวดูสดชื่น ด้วยกรดไฮยาลูโรนิค 5 ชนิด และน้ำที่ระเหยออกมาถึง 50% ช่วยให้ผิวชุ่มชื้นในวันที่อากาศร้อน&nbsp;</p><p>\'- รักษาระดับความชุ่มชื้นอย่างสมดุล ด้วยเนื้อโลชั่นที่บางเบา ช่วยให้ความชุ่มชื้นระเหยออกมาและซึมซาบเข้าสู่ผิวได้อย่างรวดเร็ว เมื่อนวดครีมบนผิว&nbsp;</p><p>ผ่านการทดสอบว่าไม่ทำให้เกิดอาการแพ้ การทดสอบภูมิแพ้ทางผิวหนัง Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp;</p><p>สถาบันผู้ทำการทดสอบ:&nbsp; Korea Dermatology Research Institute&nbsp;</p><p>ระยะเวลาการทดสอบ: 16 - 19 มกราคม 2561&nbsp;</p><p>ชื่อสินค้า: Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp;</p><p>ผู้เข้ารับการทดสอบ: จำนวน 33 คน ที่ผ่านมาตรฐานการคัดเลือก และไม่ตกข้อยกเว้นใดๆ&nbsp;</p><p>วิธีการทดสอบ: แปะแผ่นทดสอบบนผิวเป็นระยะเวลา 24 ชั่วโมง จากนั้นดึงแผ่นทดสอบออก สังเกตการเปลี่ยนแปลงบนผิวหลังจาก 30 นาที, 24 ชั่วโมง และ 48 ชั่วโมง</p><p>มาตรฐานการทดสอบ: ตามแนวทางของ International Contact Dermatitis Research Group (ICDRG) และ Personal Care Products Council (PCPC)&nbsp;</p><p>มาตรชี้วัดจากแพทย์ผู้เชี่ยวชาญด้านผิวหนัง&nbsp;</p><p>Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++) : 0.005/ ไม่ก่อให้เกิดภูมิแพ้&nbsp;</p><p>Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp; ถูกจัดให้อยู่ในหมวด \"\"สารที่ไม่ก่อให้เกิดภูมิแพ้\"\" ในการทดสอบภูมิแพ้เบื้องต้น&nbsp;</p><p>สูตรพิเศษ ปราศจาก:&nbsp; พาราเบนทั้ง 6 ชนิด (Methylparaben, Ethylparaben, Propylparaben, Isopropylparaben, Butylparaben,&nbsp;</p><p>Hyalpol Matrix เพิ่มและเก็บล็อคความชุ่มชื้นไว้อย่างอ่อนโยน</p><p>Hydrolyzed glycosaminoglycan: นาโนโมเลกุล (Oligo-HA) ช่วยเพิ่มพลังการดูดซึม ให้ความชุ่มชื้นซึมลงสู่ผิวหนัง</p><p>Sodium hyaluronate crosspolymer: เก็บรักษาความชุ่มชื้นอย่างยาวนาน&nbsp;</p><p>Hydrolyzed hyaluronic acid: ช่วยให้ผิวแข็งแรง&nbsp;</p><p>Hyaluronic acid: เก็บกักความชุ่มชื้น&nbsp;</p><p>Sodium hyaluronate:&nbsp; ช่วยให้ผิวชุ่มชื้น ตึงกระชับ และยืดหยุ่นด้วยโมเลกุล HA ที่มีขนาดใหญ่ขึ้น&nbsp;</p><p>Bamboo Water: ปลอบประโลมผิวที่ระคายเคือง&nbsp;</p><p>Cottonseed Extracts: ปรับสมดุลความชุ่มชื้นและควบคุมความมัน&nbsp;</p><p>Edelweis Extracts: ช่วยเก็บรักษาความชุ่มชื้น&nbsp;</p><p>วิธีใช้:&nbsp; ในขั้นตอนสุดท้ายของการบำรุงผิว ทา Waterfull ให้ทั่วใบหน้า หลีกเลี่ยงบริเวณรอบดวงตา</p><p>เทคนิคจากผู้เชี่ยวชาญ: เมื่อครีมบำรุงผิวซึมซาบเข้าสู่ผิวแล้ว ทา Waterfull และนวดเบาๆ จากนั้นแตะเบาๆ ทั่วใบหน้าเพื่อให้ครีมกันแดดกระจายตัว</p><p>เหมาะสำหรับ:</p><p>\'- ผู้ที่ต้องการครีมกันแดดซึ่งช่วยเพิ่มความชุ่มชื้น</p><p>\'- ผู้ที่ผิวแห้ง&nbsp;</p><p>\'- ผู้ที่ต้องการครีมกันแดดซึ่งช่วยปลอบประโลมผิว ให้ผิวสดชื่น</p>', '<p>ครีมกันแดดสองประสิทธิภาพ Waterfull ไม่เพียงแต่ช่วยปกป้องผิวจากรังสี UV ที่ทำร้ายผิว แต่ยังช่วยทำให้ผิวหน้าดูเรียบเนียน เตรียมพร้อมผิวสำหรับการแต่งหน้าในขั้นตอนต่อไป&nbsp;</p><p>\'- ปกป้องแสงแดดอย่างสูง ด้วยนวัตกรรม PA++++ ที่ถูกพัฒนาอย่างดี Waterfull ช่วยป้องกันผิวจากการทำร้ายของรังสี UV</p><p>\'- ให้ความชุ่มชื้น และให้ผิวดูสดชื่น ด้วยกรดไฮยาลูโรนิค 5 ชนิด และน้ำที่ระเหยออกมาถึง 50% ช่วยให้ผิวชุ่มชื้นในวันที่อากาศร้อน&nbsp;</p><p>\'- รักษาระดับความชุ่มชื้นอย่างสมดุล ด้วยเนื้อโลชั่นที่บางเบา ช่วยให้ความชุ่มชื้นระเหยออกมาและซึมซาบเข้าสู่ผิวได้อย่างรวดเร็ว เมื่อนวดครีมบนผิว&nbsp;</p><p>ผ่านการทดสอบว่าไม่ทำให้เกิดอาการแพ้ การทดสอบภูมิแพ้ทางผิวหนัง Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp;</p><p>สถาบันผู้ทำการทดสอบ:&nbsp; Korea Dermatology Research Institute&nbsp;</p><p>ระยะเวลาการทดสอบ: 16 - 19 มกราคม 2561&nbsp;</p><p>ชื่อสินค้า: Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp;</p><p>ผู้เข้ารับการทดสอบ: จำนวน 33 คน ที่ผ่านมาตรฐานการคัดเลือก และไม่ตกข้อยกเว้นใดๆ&nbsp;</p><p>วิธีการทดสอบ: แปะแผ่นทดสอบบนผิวเป็นระยะเวลา 24 ชั่วโมง จากนั้นดึงแผ่นทดสอบออก สังเกตการเปลี่ยนแปลงบนผิวหลังจาก 30 นาที, 24 ชั่วโมง และ 48 ชั่วโมง</p><p>มาตรฐานการทดสอบ: ตามแนวทางของ International Contact Dermatitis Research Group (ICDRG) และ Personal Care Products Council (PCPC)&nbsp;</p><p>มาตรชี้วัดจากแพทย์ผู้เชี่ยวชาญด้านผิวหนัง&nbsp;</p><p>Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++) : 0.005/ ไม่ก่อให้เกิดภูมิแพ้&nbsp;</p><p>Jungsaemmool Pre-tect Sun Waterfull (Waterfull SPF50+/ PA++++)&nbsp; ถูกจัดให้อยู่ในหมวด \"\"สารที่ไม่ก่อให้เกิดภูมิแพ้\"\" ในการทดสอบภูมิแพ้เบื้องต้น&nbsp;</p><p>สูตรพิเศษ ปราศจาก:&nbsp; พาราเบนทั้ง 6 ชนิด (Methylparaben, Ethylparaben, Propylparaben, Isopropylparaben, Butylparaben,&nbsp;</p><p>Hyalpol Matrix เพิ่มและเก็บล็อคความชุ่มชื้นไว้อย่างอ่อนโยน</p><p>Hydrolyzed glycosaminoglycan: นาโนโมเลกุล (Oligo-HA) ช่วยเพิ่มพลังการดูดซึม ให้ความชุ่มชื้นซึมลงสู่ผิวหนัง</p><p>Sodium hyaluronate crosspolymer: เก็บรักษาความชุ่มชื้นอย่างยาวนาน&nbsp;</p><p>Hydrolyzed hyaluronic acid: ช่วยให้ผิวแข็งแรง&nbsp;</p><p>Hyaluronic acid: เก็บกักความชุ่มชื้น&nbsp;</p><p>Sodium hyaluronate:&nbsp; ช่วยให้ผิวชุ่มชื้น ตึงกระชับ และยืดหยุ่นด้วยโมเลกุล HA ที่มีขนาดใหญ่ขึ้น&nbsp;</p><p>Bamboo Water: ปลอบประโลมผิวที่ระคายเคือง&nbsp;</p><p>Cottonseed Extracts: ปรับสมดุลความชุ่มชื้นและควบคุมความมัน&nbsp;</p><p>Edelweis Extracts: ช่วยเก็บรักษาความชุ่มชื้น&nbsp;</p><p>วิธีใช้:&nbsp; ในขั้นตอนสุดท้ายของการบำรุงผิว ทา Waterfull ให้ทั่วใบหน้า หลีกเลี่ยงบริเวณรอบดวงตา</p><p>เทคนิคจากผู้เชี่ยวชาญ: เมื่อครีมบำรุงผิวซึมซาบเข้าสู่ผิวแล้ว ทา Waterfull และนวดเบาๆ จากนั้นแตะเบาๆ ทั่วใบหน้าเพื่อให้ครีมกันแดดกระจายตัว</p><p>เหมาะสำหรับ:</p><p>\'- ผู้ที่ต้องการครีมกันแดดซึ่งช่วยเพิ่มความชุ่มชื้น</p><p>\'- ผู้ที่ผิวแห้ง&nbsp;</p><p>\'- ผู้ที่ต้องการครีมกันแดดซึ่งช่วยปลอบประโลมผิว ให้ผิวสดชื่น</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(54, 'JUNGSAEMMOOL Artist Angle Sponge', NULL, '20191217172801mz4o7.jpg', '20200425174704.jpg', 'N', 5, '450.00', '-', NULL, '<p>ฟองน้ำสำหรับเกลี่ยเครื่องสำอางค์ รองพื้น มีหลายขนาด ออกแบบพิเศษทั้งด้าน flat เรียบและปลายมุม สามารถใช้เกลี่ยได้ทั้งริมหน้าผาก ไรผม สะดวกในการใช้งาน เกลี่ยได้เป็นธรรมชาติ<br></p>', '<p>ฟองน้ำสำหรับเกลี่ยเครื่องสำอางค์ รองพื้น มีหลายขนาด ออกแบบพิเศษทั้งด้าน flat เรียบและปลายมุม สามารถใช้เกลี่ยได้ทั้งริมหน้าผาก ไรผม สะดวกในการใช้งาน เกลี่ยได้เป็นธรรมชาติ<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(55, 'JUNGSAEMMOOL Artist Brush Blush', NULL, '20191217173230jQZ2D.jpg', '20200425174848.jpg', 'N', 5, '700.00', '-', NULL, '<div>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</div><div>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</div><div>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</div><div>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</div><div>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</div>', '<div>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</div><div>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</div><div>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</div><div>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</div><div>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(56, 'JUNGSAEMMOOL Artist Brush Brow Edge', NULL, '20191217173920zKQii.jpg', '20200425174908.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(57, 'JUNGSAEMMOOL Artist Brush Brow Round', NULL, '20191217174223CuwMd.jpg', '20200425174939.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(58, 'JUNGSAEMMOOL Artist Brush Concealer L', NULL, '20191217174538W3Uuv.jpg', '20200425175004.jpg', 'N', 5, '500.00', '-', NULL, '<p><br></p><p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p><br></p><p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-15', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(59, 'JUNGSAEMMOOL Artist Brush Concealer M', NULL, '201912171748200yuDX.jpg', '20200425175026.jpg', 'N', 5, '450.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(60, 'JUNGSAEMMOOL Artist Brush Concealer S', NULL, '20191217175055hezpu.jpg', '20200425175046.jpg', 'N', 5, '400.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p><span style=\"font-size: 1rem;\">- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</span><br></p><p><span style=\"font-size: 1rem;\">- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</span><br></p><p><span style=\"font-size: 1rem;\">- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</span></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(61, 'JUNGSAEMMOOL Artist Brush Contour', NULL, '20191218102831KDu8U.jpg', '20200425175101.jpg', 'N', 5, '600.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยแปรงคอนทัวร์ที่ทำจากขนแปรงสังเคราะห์ที่นุ่มนวล ช่วยให้ใบหน้าดูมีมิติ 3D&nbsp;</p><p>- เอนกประสงค์! นอกจากจะใช้คอนทัวร์แล้ว ยังสามารถใช้ไฮไลท์, สโตรค และปัดแก้มได้ด้วย</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><p><br></p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยแปรงคอนทัวร์ที่ทำจากขนแปรงสังเคราะห์ที่นุ่มนวล ช่วยให้ใบหน้าดูมีมิติ 3D&nbsp;</p><p>- เอนกประสงค์! นอกจากจะใช้คอนทัวร์แล้ว ยังสามารถใช้ไฮไลท์, สโตรค และปัดแก้มได้ด้วย</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><p><br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(62, 'JUNGSAEMMOOL Artist Brush Eye Liner', NULL, '201912181034262AmIJ.jpg', '20200425175124.jpg', 'N', 5, '400.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(63, 'JUNGSAEMMOOL Artist Brush Eye Shadow L', NULL, '20191218110330FFIhX.jpg', '20200425175148.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(64, 'JUNGSAEMMOOL Artist Brush Eye Shadow M', NULL, '20191218110706bcRKI.jpg', '20200425175307.jpg', 'N', 5, '450.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(65, 'JUNGSAEMMOOL Artist Brush Eye Shadow Point', NULL, '20191218111807VJDQQ.jpg', '20200425175332.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(66, 'JUNGSAEMMOOL Artist Brush Eye Shadow S', NULL, '20191218113003lc65z.jpg', '20200425175358.jpg', 'N', 5, '400.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(67, 'JUNGSAEMMOOL Artist Brush Eye Shadow Smudge', NULL, '20191218113603QNX3o.jpg', '20200425175419.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(68, 'JUNGSAEMMOOL Artist Brush Foundation', NULL, '20191218115100Waofo.jpg', '20200425175549.jpg', 'N', 5, '700.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(69, 'JUNGSAEMMOOL Artist Brush Hair Line', NULL, '20191218115416dmIR0.jpg', '20200425175606.jpg', 'N', 5, '600.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(70, 'JUNGSAEMMOOL Artist Brush Highlight', NULL, '20191218115713iN2Wp.jpg', '20200425175626.jpg', 'N', 5, '600.00', '-', NULL, '<div>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</div><div>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</div><div>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</div><div>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</div><div>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</div><div><br></div>', '<div>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</div><div>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</div><div>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</div><div>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</div><div>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</div><div><br></div>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(71, 'JUNGSAEMMOOL Artist Brush Lip', NULL, '20191218115948dxhBj.jpg', '20200425175651.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(72, 'JUNGSAEMMOOL Artist Brush Nose Contour', NULL, '20191218120233u1tNl.jpg', '20200425175707.jpg', 'N', 5, '500.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(73, 'JUNGSAEMMOOL Artist Brush Pouch Set', NULL, '20191218120635C6CyK.jpg', '20200425175720.jpg', 'N', 5, '8000.00', '19pcs of Artist Brush', NULL, '<p>ห้ามพลาด!! กับไอเทมเด็ดที่เมคอัพอาร์ตทิสมืออาชีพต้องมี JUNGSAEMMOOL Artist Brush Pouch Set ชุดแปรงอาร์ตทิส 19 ชิ้น เพื่อการแต่งหน้าที่สวยงามสมบูรณ์แบบในทุกสถานการณ์</p><p>- แปรงแต่งหน้าหลากหลายรูปแบบ เพื่อการใช้งานที่แตกต่างกันไป ไม่ว่าจะเน้นสีหรือเน้นเส้น</p><p>- ขนแปรงสังเคราะห์ระดับพรีเมี่ยม ที่มีความนุ่ม ปัดเกลี่ยได้อย่างสวยงามเรียบเนียน ไม่ระคายเคืองต่อผิว</p><p>- ด้ามจับดีไซน์พิเศษ จับได้อย่างกระชับมั่นคง เพื่อรังสรรค์การแต่งหน้าที่สมบูรณ์</p><p>- พร้อมซองสำหรับป้องกันขนแปรง ช่วยในการจัดเก็บให้ขนแปรงอยู่ในสภาพสมบูรณ์ ไม่หักงอ</p>', '<p>ห้ามพลาด!! กับไอเทมเด็ดที่เมคอัพอาร์ตทิสมืออาชีพต้องมี JUNGSAEMMOOL Artist Brush Pouch Set ชุดแปรงอาร์ตทิส 19 ชิ้น เพื่อการแต่งหน้าที่สวยงามสมบูรณ์แบบในทุกสถานการณ์</p><p>- แปรงแต่งหน้าหลากหลายรูปแบบ เพื่อการใช้งานที่แตกต่างกันไป ไม่ว่าจะเน้นสีหรือเน้นเส้น</p><p>- ขนแปรงสังเคราะห์ระดับพรีเมี่ยม ที่มีความนุ่ม ปัดเกลี่ยได้อย่างสวยงามเรียบเนียน ไม่ระคายเคืองต่อผิว</p><p>- ด้ามจับดีไซน์พิเศษ จับได้อย่างกระชับมั่นคง เพื่อรังสรรค์การแต่งหน้าที่สมบูรณ์</p><p>- พร้อมซองสำหรับป้องกันขนแปรง ช่วยในการจัดเก็บให้ขนแปรงอยู่ในสภาพสมบูรณ์ ไม่หักงอ</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(74, 'JUNGSAEMMOOL Artist Brush Powder', NULL, '20191218121005V8mNg.jpg', '20200425175740.jpg', 'N', 5, '700.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(75, 'JUNGSAEMMOOL Artist Brush Powder & Blusher', NULL, '20191218121659b3PDH.jpg', '20200425175803.jpg', 'N', 5, '650.00', '-', NULL, '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '<p>เป็นมืออาชีพมากขึ้นสำหรับช่างแต่งหน้า! ง่ายขึ้นสำหรับผู้เริ่มแต่งหน้า! Jung Saem Mool Artist Brush ช่วยให้การแต่งหน้าสมบูรณ์แบบยิ่งขึ้น</p><p>- ด้วยรูปแปรงที่เป็นหน้าตัดเฉียง ช่วยให้คอนทัวร์กรอบหน้าได้อย่างสมบูรณ์แบบ</p><p>- ขนแปรงสังเคราะห์ที่นุ่มนวล เหมาะสำหรับผิวบอบบาง</p><p>- จับถนัดมือด้วยน้ำหนักและความยาวที่เหมาะสม</p><p>- ปลอกสวมที่ช่วยรักษาให้ขนแปรงแน่นอยู่ทรง</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(76, 'JUNGSAEMMOOL Artist Make-Up Blending Tool', NULL, '20191218122104D6aNi.jpg', '20200425175815.jpg', 'N', 5, '1200.00', '-', NULL, '<p>สะดวกสบายและหลากหลายประโยชน์ ถาดผสมเครื่องสำอางชิ้นนี้ช่วยให้คุณสามารถผสมและเบลนด์เมคอัพต่างๆ เพื่อให้ได้เนื้อเมคอัพที่ต้องการ</p><p>- มีถาดผสมเครื่องสำอางถึง 5 ถาด เพื่อความสะดวกและเป็นระเบียบในการผสมผลิตภัณฑ์ที่หลากหลาย&nbsp;</p><p>- พาเล็ทแต่งหน้าสำหรับช่างแต่งหน้ามืออาชีพ ที่ใช้งานง่าย ขนาดกระชับมือ ช่วยให้ผสมและปรับเนื้อเมคอัพได้ตามต้องการ</p>', '<p>สะดวกสบายและหลากหลายประโยชน์ ถาดผสมเครื่องสำอางชิ้นนี้ช่วยให้คุณสามารถผสมและเบลนด์เมคอัพต่างๆ เพื่อให้ได้เนื้อเมคอัพที่ต้องการ</p><p>- มีถาดผสมเครื่องสำอางถึง 5 ถาด เพื่อความสะดวกและเป็นระเบียบในการผสมผลิตภัณฑ์ที่หลากหลาย&nbsp;</p><p>- พาเล็ทแต่งหน้าสำหรับช่างแต่งหน้ามืออาชีพ ที่ใช้งานง่าย ขนาดกระชับมือ ช่วยให้ผสมและปรับเนื้อเมคอัพได้ตามต้องการ</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(77, 'JUNGSAEMMOOL Artist Pencil Sharpener', NULL, '20191218122408SjPzv.jpg', '20200425175831.jpg', 'N', 5, '200.00', '-', NULL, '<p>ไม่พลาดทุกจังหวะสำคัญ ด้วยกบเหลาดินสอสำหรับเครื่องสำอาง JUNGSAEMMOOL Artist Pencil Sharpener กบเหลาดินสอเกรดพรีเมี่ยมที่ช่วยให้ดินสอมีความแหลมคมพร้อมใช้ในทุกสถานการณ์ ด้วยใบมีพิเศษจากเยอรมนี ที่มีความคมทนทาน ใช้งานง่ายโดยไม่ต้องออกแรงมาก พร้อมระบบ Blade Cleaner ที่ช่วยทำความสะอาดใบมีดให้สะอาดหมดจด คมกริบเหมือนใหม่!<br></p>', '<p>ไม่พลาดทุกจังหวะสำคัญ ด้วยกบเหลาดินสอสำหรับเครื่องสำอาง JUNGSAEMMOOL Artist Pencil Sharpener กบเหลาดินสอเกรดพรีเมี่ยมที่ช่วยให้ดินสอมีความแหลมคมพร้อมใช้ในทุกสถานการณ์ ด้วยใบมีพิเศษจากเยอรมนี ที่มีความคมทนทาน ใช้งานง่ายโดยไม่ต้องออกแรงมาก พร้อมระบบ Blade Cleaner ที่ช่วยทำความสะอาดใบมีดให้สะอาดหมดจด คมกริบเหมือนใหม่!<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(78, 'JUNGSAEMMOOL Essential Cleansing Cotton Pad', NULL, '20191218122652JRYv8.jpg', '20200425175848.jpg', 'N', 5, '200.00', '80pcs', NULL, '<p>เช็ดทำความสะอาดผิวได้อย่างหมดจดล้ำลึก ด้วยแผ่นเช็ดทำความสะอาดเครื่องสำอาง JUNGSAEMMOOL Essential Cleansing Cotton Pad แผ่นสำลีเนื้อนุ่ม ปลอดภัยไม่ระคายเคืองต่อผิวหน้า พร้อมการออกแบบพิเศษที่พื้นผิวสำลี ที่ช่วยให้คุณเช็ดทำความสะอาดคราบเครื่องสำอาง หรือสิ่งสกปรกบนใบหน้าได้อย่างรวดเร็วและทั่วถึงในแผ่นเดียว<br></p>', '<p>เช็ดทำความสะอาดผิวได้อย่างหมดจดล้ำลึก ด้วยแผ่นเช็ดทำความสะอาดเครื่องสำอาง JUNGSAEMMOOL Essential Cleansing Cotton Pad แผ่นสำลีเนื้อนุ่ม ปลอดภัยไม่ระคายเคืองต่อผิวหน้า พร้อมการออกแบบพิเศษที่พื้นผิวสำลี ที่ช่วยให้คุณเช็ดทำความสะอาดคราบเครื่องสำอาง หรือสิ่งสกปรกบนใบหน้าได้อย่างรวดเร็วและทั่วถึงในแผ่นเดียว<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(79, 'JUNGSAEMMOOL Cushion Puff', NULL, '20191218122928QJV4t.jpg', '20200425175903.jpg', 'N', 5, '200.00', '-', NULL, '<p>แต่งง่าย เกลี่ยไว เนียนสวย ด้วย JUNGSAEMMOOL Essential Skin Nuder Cushion Puff พัฟคุชชั่นเนื้อดีที่ตอบโจทย์การใช้งานได้ดีเยี่ยม เนื้อนุ่ม เกลี่ยเนียนได้อย่างรวดเร็ว ช่วยให้การลงรองพื้นของคุณง่ายดายและรวดกว่าที่เคยเป็น พร้อมความปลอดภัย ไม่แพ้ง่าย ในทุกสภาพผิว<br></p>', '<p>แต่งง่าย เกลี่ยไว เนียนสวย ด้วย JUNGSAEMMOOL Essential Skin Nuder Cushion Puff พัฟคุชชั่นเนื้อดีที่ตอบโจทย์การใช้งานได้ดีเยี่ยม เนื้อนุ่ม เกลี่ยเนียนได้อย่างรวดเร็ว ช่วยให้การลงรองพื้นของคุณง่ายดายและรวดกว่าที่เคยเป็น พร้อมความปลอดภัย ไม่แพ้ง่าย ในทุกสภาพผิว<br></p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(80, 'JUNGSAEMMOOL Essential Star-Cealer Foundation Puff', NULL, '20191218123257h6c1I.jpg', '20200425175914.jpg', 'N', 5, '250.00', '-', NULL, '<p>พัฟสำหรับใช้กับ JUNGSAEMMOOL Essential Star-cealer Foundation Puff&nbsp;</p><p>- พัฟพิเศษ เนื้อนุ่ม ดีไซน์ พิเศษ สำหรับใช้กับรองพื้น นุ่มและช่วยให้ความชุ่มชื้น กับผิวหน้า</p><p>- Micro cellular ในตัว puff ช่วยให้ผิวสัมผัสนุ่มและท่รองพื้นได้เนียนสู่ผิว</p><p>- วิธีใช้ แตะรองพื้นในปริมาณที่เหมาะสมและแตะเพียงเบาๆ บนผิวหน้า</p>', '<p>พัฟสำหรับใช้กับ JUNGSAEMMOOL Essential Star-cealer Foundation Puff&nbsp;</p><p>- พัฟพิเศษ เนื้อนุ่ม ดีไซน์ พิเศษ สำหรับใช้กับรองพื้น นุ่มและช่วยให้ความชุ่มชื้น กับผิวหน้า</p><p>- Micro cellular ในตัว puff ช่วยให้ผิวสัมผัสนุ่มและท่รองพื้นได้เนียนสู่ผิว</p><p>- วิธีใช้ แตะรองพื้นในปริมาณที่เหมาะสมและแตะเพียงเบาๆ บนผิวหน้า</p>', '2020-01-11', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(81, 'Skin Nuder Concealer', NULL, '20200429165111miRD0.jpg', '20200429165155.jpg', 'Y', 1, '950.00', NULL, NULL, '<p style=\"text-align: center;\"><br></p>', '<p style=\"text-align: center; \"><br></p>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(82, 'JUNGSAEMMOOL Essential Mool Cream Brightening', NULL, '20200429171636FTWDw.jpg', '20200429171641.jpg', 'Y', 4, '1900.00', NULL, NULL, '<p><br></p><p>เกลี้ยงเกลา ส่วนผสมไฮยาลูโรนิค 8 ชนิด ให้ผิวชุ่มชื้นอย่างล้ำลึก ผลลัพธ์ผิวฉ่ำ กระจ่างใส เปล่งประกายตลอดวัน เหมาะกับผู้ที่ต้องการเพิ่มความกระจ่างใส ช่วยแก้ปัญหาความหมองคล้ำ</p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p style=\"text-align: center; \"><br></p>', '<p><br></p><p>เกลี้ยงเกลา ส่วนผสมไฮยาลูโรนิค 8 ชนิด ให้ผิวชุ่มชื้นอย่างล้ำลึก ผลลัพธ์ผิวฉ่ำ กระจ่างใส เปล่งประกายตลอดวัน เหมาะกับผู้ที่ต้องการเพิ่มความกระจ่างใส ช่วยแก้ปัญหาความหมองคล้ำ</p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p style=\"text-align: center; \"><br></p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(83, 'JUNGSAEMMOOL Essential Mool Cream Light', NULL, '202004291736174b70P.jpg', '20200429173622.jpg', 'Y', 4, '1900.00', NULL, NULL, '<p>ครีมบำรุงผิวสูตรบางเบา เพื่อผิวใส ฉ่ำสุขภาพดีราวกับพื้นน้ำใสสะอาดยามแสงผ่านทะลุได้ในเวลากลางวัน เทคโนโลยี OxygenWater มอบเนื้อสัมผัสบางเบาราวกับผิวสามารถหายใจได้ โอบอุ้มความชุ่มชื้นยาวนาน ด้วยเซรามาย วอเตอร์ และส่วนผสมไฮยาลูโรนิค7 ชนิด สารสกัดซิการ์แคร์ ช่วยปลอบประโลมผิวได้ดี เหมาะผู้ที่ต้องการการบำรุงอย่างเต็มเปี่ยม แต่ต้องการความเบาสบายผิว<br></p>', '<p><font face=\"Roboto, -apple-system, BlinkMacSystemFont, Helvetica Neue, Helvetica, sans-serif\"><span style=\"font-size: 14px; white-space: pre-wrap;\">ครีมบำรุงผิวสูตรบางเบา เพื่อผิวใส ฉ่ำสุขภาพดีราวกับพื้นน้ำใสสะอาดยามแสงผ่านทะลุได้ในเวลากลางวัน เทคโนโลยี OxygenWater มอบเนื้อสัมผัสบางเบาราวกับผิวสามารถ\nหายใจได้ โอบอุ้มความชุ่มชื้นยาวนาน ด้วยเซรามาย วอเตอร์ และส่วนผสมไฮยาลูโรนิค7 ชนิด สารสกัดซิการ์แคร์ ช่วยปลอบประโลมผิวได้ดี เหมาะผู้ที่ต้องการการบำรุงอย่างเต็มเปี่ยม แต่ต้องการความเบาสบายผิว</span></font><br></p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(84, 'JUNGSAEMMOOL Colorpeice Eyeshadow Nude', NULL, '20200429174604iLDCz.jpg', '20200429174609.jpg', 'Y', 3, '700.00', NULL, NULL, '<p>อายแชโดว์โทนสีนู้ดที่กลมกลืนกับโทนสีผิวได้เป็นอย่างดีคุณสมบัติพิเศษสามารถมองเห็นสีได้อย่างชัดเจน</p><p>ดึงความเป็นธรรมชาติของผิวออกมาได้อย่างดีเยี่ยมเนื้อเกลี่ยง่ายติดทนนานโดยไม่เป็นคราบ มีให้เลือกทั้งหมด 4 เฉดสี ทั้ง</p><p>Bare Nude – สีนู้ดที่ทำให้เปลือกตาดูสะอาด</p><p>Petal Nude - สีนู้ดที่ทำให้เปลือกตาสีชมพู</p><p>Cosy Nude – สีโทนส้มอมชมพูน้ำตาล และ</p><p>Classy Nude - สีน้ำตาล ดูทันสมัย โดย จอง แซมมุล</p><p>ผ่านกระบวนการต่าง ๆมากมายที่ตั้งใจรังสรรค์เพื่อบิ้วตี้เลิฟเวอร์โดยเฉพาะ</p><div><br></div>', '<p>อายแชโดว์โทนสีนู้ดที่กลมกลืนกับโทนสีผิวได้เป็นอย่างดีคุณสมบัติพิเศษสามารถมองเห็นสีได้อย่างชัดเจน</p><p>ดึงความเป็นธรรมชาติของผิวออกมาได้อย่างดีเยี่ยมเนื้อเกลี่ยง่ายติดทนนานโดยไม่เป็นคราบ มีให้เลือกทั้งหมด 4 เฉดสี ทั้ง</p><p>Bare Nude – สีนู้ดที่ทำให้เปลือกตาดูสะอาด</p><p>Petal Nude - สีนู้ดที่ทำให้เปลือกตาสีชมพู</p><p>Cosy Nude – สีโทนส้มอมชมพูน้ำตาล และ</p><p>Classy Nude - สีน้ำตาล ดูทันสมัย โดย จอง แซมมุล</p><p>ผ่านกระบวนการต่าง ๆมากมายที่ตั้งใจรังสรรค์เพื่อบิ้วตี้เลิฟเวอร์โดยเฉพาะ</p><div><br></div>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(85, 'Jungsaemmool Lip-pression Shine', NULL, '20200429180010VV4dj.jpg', '20200429175809.jpg', 'Y', 2, '950.00', NULL, NULL, '<ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size: 12px;\"><li class=\"\" data-spm-anchor-id=\"a2o4m.pdp.product_detail.i1.2f312472xcY8jP\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">3-SECOND VIVID อัพโทนผิวสว่างใน 3 วินาที&nbsp; &nbsp;เมื่อแสงตกกระทบริมฝีปาก ริมฝีปากคุณจะโดดเด่นและสดใสยิ่งขึ้น&nbsp; ลิปดีไซน์พิเศษ เพื่อรังสรรค์การแต่งหน้าได้ง่ายดายและแม่นยำยิ่งขึ้น</li></ul>', '<ul class=\"\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; list-style: none; overflow: hidden; columns: auto 2; column-gap: 32px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-size: 12px;\"><li class=\"\" data-spm-anchor-id=\"a2o4m.pdp.product_detail.i1.2f312472xcY8jP\" style=\"margin: 0px; padding: 0px 0px 0px 15px; position: relative; font-size: 14px; line-height: 18px; text-align: left; list-style: none; word-break: break-word; break-inside: avoid;\">3-SECOND VIVID อัพโทนผิวสว่างใน 3 วินาที&nbsp; &nbsp;เมื่อแสงตกกระทบริมฝีปาก ริมฝีปากคุณจะโดดเด่นและสดใสยิ่งขึ้น&nbsp; ลิปดีไซน์พิเศษ เพื่อรังสรรค์การแต่งหน้าได้ง่ายดายและแม่นยำยิ่งขึ้น</li></ul>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19');
INSERT INTO `product_details` (`id`, `name_en`, `name_th`, `cover_img`, `cover_zoom`, `display_type`, `category_id`, `price`, `size`, `spl_price`, `detail_th`, `detail_en`, `start_time`, `end_time`, `is_active`, `is_bestseller`, `created_at`, `updated_at`) VALUES
(86, '\'20 PINK EDITION - Essential Skin Nuder Cushion (refill 2 included)', NULL, '20200429183458eC9Bq.jpg', '20200429183542.jpg', 'Y', 1, '1900.00', NULL, NULL, '<p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing Powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้ว รอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบางๆ บนผิว ทำให้เม็ดสีและความชุ่มชื้นบนผิวคงอยู่ยาวนาน ผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุมชื่น ตลอดวัน<br></p>', '<p>คุชชั่นมอบงานผิวธรรมชาติถึง 3 ประการ Skin Nuder Cushion มอบผิวกระจ่างใส ปกปิดริ้วรอย พร้อมป้องกัน ผิวจากรังสี UV ด้วยค่า SPF50 + PA +++ เสมือนผิวได้สวมเสื้อเกราะตลอดวัน พร้อมเผยผิวโกลวอย่างธรรมชาติ ด้วยส่วนผสมสำคัญ Huebalancing Powder ช่วยปรับสีผิวให้กระจ่างใสด้วยเอฟเฟกต์สีที่สมบูรณ์และแสดงออกถึงโทนสีผิวให้ดูอ่อนเยาว์ ไร้ริ้ว รอย ผสานกับ สกินฟิตติ้ง โพลิเมอร์ Skin Fitting Polymer ป้อมปราการแห่งผิว ด้วยการสร้างฟิล์มบางๆ บนผิว ทำให้เม็ดสีและความชุ่มชื้นบนผิวคงอยู่ยาวนาน ผลลัพธ์ที่ได้ เมคอัพติดทนนานและผิวที่ชุ่มชื่นและเรียบเนียน สวยเป็นประกายยาวนานตลอดวัน เหมาะกับสภาพผิวแห้ง และต้องการให้ผิวดูฉ่ำ ชุมชื่น ตลอดวัน<br></p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(87, 'Skin Setting Tone-up Sun Cushion+Tone-up Sun Cushion refill', NULL, '2020042918410448GfI.jpg', '20200429184108.jpg', 'Y', 1, '1500.00', NULL, NULL, '<p>คุชชั่นที่ช่วยเพิ่มความสว่างให้กับผิว พร้อมเพิ่มเติมการปกป้องผิวจากแสงแดดในระหว่างวัน</p><p>\n• ใช้เติมระหว่างวันได้ตามต้องการ เพื่อการปกป้องผิวจากรังสียูวี\n</p><p>• ให้ผิวกระจ่างใสยิ่งขึ้น ด้วยส่วนประกอบของอลูมิน่า (Alumina) และสารสกัดจากดอกบัวและดอกซากุระ\n</p><p>• เทคโนโลยีการปกปิดผิว 2 ชั้น สามารถเติมได้อย่างแนบสนิทกับผิวโดยไม่เป็นคราบ และช่วยให้การแต่งหน้าติดทนนานตลอดวัน</p>', '<p data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.303a5b27dfcDOv\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px;\"><font face=\"Roboto, -apple-system, BlinkMacSystemFont, Helvetica Neue, Helvetica, sans-serif\"><span style=\"font-size: 14px; white-space: pre-wrap;\">คุชชั่นที่ช่วยเพิ่มความสว่างให้กับผิว พร้อมเพิ่มเติมการปกป้องผิวจากแสงแดดในระหว่างวัน\n\n• ใช้เติมระหว่างวันได้ตามต้องการ เพื่อการปกป้องผิวจากรังสียูวี\n\n• ให้ผิวกระจ่างใสยิ่งขึ้น ด้วยส่วนประกอบของอลูมิน่า (Alumina) และสารสกัดจากดอกบัวและดอกซากุระ\n\n• เทคโนโลยีการปกปิดผิว 2 ชั้น สามารถเติมได้อย่างแนบสนิทกับผิวโดยไม่เป็นคราบ และช่วยให้การแต่งหน้าติดทนนานตลอดวัน</span></font><br></p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(88, 'Skin Nuder Foundation(puff included)', NULL, '20200429185139uAZLG.jpg', '20200429185149.jpg', 'Y', 1, '1700.00', NULL, NULL, '<p>JUNGSAEMMOOL Skin Nuder Foundation</p><p>ภายใต้นิยาม “More Nude, Nuder” -</p><p>ให้มากกว่าผิวสวย เพื่อผิวส่องประกาย เป็นธรรมชาติ</p><p>ถือเป็นรองพื้นสมบูรณ์แบบไร้ที่ติ โดยในผลิตภัณฑ์ที่มีคุณสมบัติเฉพาะ</p><p>ด้วยเม็ดสีที่โปร่งแสงทำให้รองพื้นกลืนเข้ากับโทนสีผิวได้เป็นอย่างดี<span style=\"font-size: 1rem;\">ให้ความรู้สึกที่ปกปิดได้อย่างเรียบเนียนติดทนนานตลอดทั้งวัน</span></p><p>พร้อมมอบลุคฉ่ำว้าวให้กับผิวดุจดั่งธรรมชาติ สำหรับตัวผลิตภัณฑ์JUNG&nbsp;<span style=\"font-size: 1rem;\">SAEM MOOL Skin Nuder Foundation มาพร้อมกับ EASY TAP</span></p><p>PUFF มีให้เลือกทั้ง 5 เฉดสี คือ<span style=\"font-size: 1rem;\">สีขาว เหลืองไอวอรี่, สีขาวเหลือง ไอวอรี่, สีเนื้อ ครีม, สีครีมเบจ และ</span><span style=\"font-size: 1rem;\">สีเหลืองเบจ</span></p>', '<p>JUNGSAEMMOOL Skin Nuder Foundation</p><p>ภายใต้นิยาม “More Nude, Nuder” -</p><p>ให้มากกว่าผิวสวย เพื่อผิวส่องประกาย เป็นธรรมชาติ</p><p>ถือเป็นรองพื้นสมบูรณ์แบบไร้ที่ติ โดยในผลิตภัณฑ์ที่มีคุณสมบัติเฉพาะ</p><p>ด้วยเม็ดสีที่โปร่งแสงทำให้รองพื้นกลืนเข้ากับโทนสีผิวได้เป็นอย่างดี<span style=\"font-size: 1rem;\">ให้ความรู้สึกที่ปกปิดได้อย่างเรียบเนียนติดทนนานตลอดทั้งวัน</span></p><p>พร้อมมอบลุคฉ่ำว้าวให้กับผิวดุจดั่งธรรมชาติ สำหรับตัวผลิตภัณฑ์JUNG&nbsp;<span style=\"font-size: 1rem;\">SAEM MOOL Skin Nuder Foundation มาพร้อมกับ EASY TAP</span></p><p>PUFF มีให้เลือกทั้ง 5 เฉดสี คือ<span style=\"font-size: 1rem;\">สีขาว เหลืองไอวอรี่, สีขาวเหลือง ไอวอรี่, สีเนื้อ ครีม, สีครีมเบจ และ</span><span style=\"font-size: 1rem;\">สีเหลืองเบจ</span></p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(89, 'Essential Mool Cleansing Water', NULL, '20200429190545KTddK.jpeg', '20200429190550.jpg', 'Y', 4, '1000.00', NULL, NULL, '<p>คลีนซิ่งวอเตอร์ที่ทำความสะอาดผิวได้อย่างหมดจดล้ำลึก ช่วยเผยผิวให้กระจ่างใส และคงความชุ่มชื้นให้กับผิวด้วยสูตรที่แตกต่างจากผลิตภัณฑ์ทั่วไป ด้วยส่วนผสมหลักของน้ำแร่ Mineral Hydrogen Water, สารสกัดจากไข่มุก และพืชพรรณธรรมชาติ</p><p>\n• ทำความสะอาดอย่างหมดจดล้ำลึก ด้วยส่วนผสมของ Mineral Hydrogen Water ด้วยอนุภาคขนาดเล็ก ที่สามารถเข้าทำความสะอาดและเคลียร์ผิวได้อย่างหมดจด เผยผิวให้ดูกระจ่างใสอย่างเป็นธรรมชาติ</p><p>• มอบความชุ่มชื้นสู่ผิว ด้วยส่วนผสมจากน้ำแร่ที่อุดมไปด้วยแร่ธาตุที่ช่วยให้ผิวชุ่มชื้น และสารสกัดจากไข่มุกที่ช่วยเติมเต็มผิวให้คงความชุ่มชื้นไว้ได้นานขึ้น\n</p><p>• ทำความสะอาดผิวได้อย่างอ่อนโยน ด้วยสูตรที่เป็นกรดอ่อนๆ ใกล้เคียงกับค่า pH ตามธรรมชาติ ช่วยคืนความสมดุลของผิวหน้า\n</p><p>• สูตรอ่อนโยน ผ่านการทดสอบการระคายเคืองผิวหน้า </p><p>\n• ปราศจากสารที่ก่อให้เกิดการระคายเคือง 10 ชนิด ได้แก่ พาราเบน 6 ชนิด, สารกันเสีย,เอทานอล, สีสังเคราะห์, น้ำหอมสังเคราะห์</p>', '<p>คลีนซิ่งวอเตอร์ที่ทำความสะอาดผิวได้อย่างหมดจดล้ำลึก ช่วยเผยผิวให้กระจ่างใส และคงความชุ่มชื้นให้กับผิวด้วยสูตรที่แตกต่างจากผลิตภัณฑ์ทั่วไป ด้วยส่วนผสมหลักของน้ำแร่ Mineral Hydrogen Water, สารสกัดจากไข่มุก และพืชพรรณธรรมชาติ</p><p>• ทำความสะอาดอย่างหมดจดล้ำลึก ด้วยส่วนผสมของ Mineral Hydrogen Water ด้วยอนุภาคขนาดเล็ก ที่สามารถเข้าทำความสะอาดและเคลียร์ผิวได้อย่างหมดจด เผยผิวให้ดูกระจ่างใสอย่างเป็นธรรมชาติ</p><p>• มอบความชุ่มชื้นสู่ผิว ด้วยส่วนผสมจากน้ำแร่ที่อุดมไปด้วยแร่ธาตุที่ช่วยให้ผิวชุ่มชื้น และสารสกัดจากไข่มุกที่ช่วยเติมเต็มผิวให้คงความชุ่มชื้นไว้ได้นานขึ้น</p><p>• ทำความสะอาดผิวได้อย่างอ่อนโยน ด้วยสูตรที่เป็นกรดอ่อนๆ ใกล้เคียงกับค่า pH ตามธรรมชาติ ช่วยคืนความสมดุลของผิวหน้า</p><p>• สูตรอ่อนโยน ผ่านการทดสอบการระคายเคืองผิวหน้า</p><p>• ปราศจากสารที่ก่อให้เกิดการระคายเคือง 10 ชนิด ได้แก่ พาราเบน 6 ชนิด, สารกันเสีย,เอทานอล, สีสังเคราะห์, น้ำหอมสังเคราะห์</p>', '2020-04-29', '2025-01-25', 'Y', 'Y', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(90, 'Essential Mool Cleansing Oil', NULL, '20200429191208nQjKk.jpeg', '20200429191301.jpg', 'Y', 4, '1400.00', NULL, NULL, '<p>\nด้วยสัมผัสที่บางเบาดุจน้ำธรรมชาติ แต่ให้ประสิทธิภาพการทำความสะอาดที่เหนือกว่า ด้วยส่วนผสมสำคัญ อาทิ น้ำแร่ Hydrogen water, Hyaluronic Acids 8 ชนิด และสารสกัดจากไข่มุก</p><p>\n• เนื้อออยล์ที่จะแตกตัวเป็นอนุภาคเล็ก เข้าแทรกซึมทำความสะอาดในรูขุมขนและสิวหัวดำได้อย่างหมดจดล้ำลึก\n</p><p>• สูตรออยล์เนื้อบางเบาที่ทำความสะอาดผิวโดยปราศจากการระคายเคือง\n</p><p>• ทำความสะอาดได้อย่างหมดจด ไร้สารตกค้าง ด้วยเนื้อออยล์ที่บางเบา ละลายน้ำได้ดี ขจัดเมคอัพตกค้างได้หมดจดทั่วถึง คืนความสะอาดใสให้กับผิวอย่างเป็นธรรมชาติ\n</p>', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.cd203e38ErTrqg\" style=\"margin: 0px; padding: 0px;\">ด้วยสัมผัสที่บางเบาดุจน้ำธรรมชาติ แต่ให้ประสิทธิภาพการทำความสะอาดที่เหนือกว่า ด้วยส่วนผสมสำคัญ อาทิ น้ำแร่ Hydrogen water, Hyaluronic Acids 8 ชนิด และสารสกัดจากไข่มุก</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• เนื้อออยล์ที่จะแตกตัวเป็นอนุภาคเล็ก เข้าแทรกซึมทำความสะอาดในรูขุมขนและสิวหัวดำได้อย่างหมดจดล้ำลึก</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• สูตรออยล์เนื้อบางเบาที่ทำความสะอาดผิวโดยปราศจากการระคายเคือง</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• ทำความสะอาดได้อย่างหมดจด ไร้สารตกค้าง ด้วยเนื้อออยล์ที่บางเบา ละลายน้ำได้ดี ขจัดเมคอัพตกค้างได้หมดจดทั่วถึง คืนความสะอาดใสให้กับผิวอย่างเป็นธรรมชาติ</span></p>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(91, 'Essential Mool Cleansing Foam', NULL, '20200429192455XE2tp.jpg', '20200429192508.jpg', 'Y', 4, '950.00', NULL, NULL, '<p>\nโฟมเนื้อวิปหนานุ่ม ที่ล้างทำความสะอาดผิวได้อย่างสะอาดหมดจด พร้อมคงความชุ่มชื้น ด้วยส่วนผสมของน้ำแร่ กรดไฮยาลูโรนิค และสารสกัดจากพืชพรรณธรรมชาติ\n</p><p>• เนื้อโฟมที่แน่นและเข้มข้น ช่วยขจัดเครื่องสำอาง ของเสีย และสิ่งสกปรกที่ตกค้างในรูขุมขน\n</p><p>• ด้วยส่วนผสมของ Mineral rich hydrogen water และ hyaluronic acids ถึง 8 ชนิด จึงช่วยเติมเต็มผิวให้เนียนนุ่มชุ่มชื้น\n</p><p>• ทำความสะอาดผิวอย่างอ่อนโยน ด้วยส่วนผสมที่ช่วยลดการระคายเคืองผิว\n• ผ่านการทดสอบการระคายเคืองผิวหน้า </p><p>\n• ปราศจากสารที่ก่อให้เกิดการระคายเคือง 10 ชนิด ได้แก่ พาราเบน 6 ชนิด, สารกันเสีย,เอทานอล, สีสังเคราะห์, น้ำหอมสังเคราะห์\n</p>', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.5de25f0ckQHwYn\" style=\"margin: 0px; padding: 0px;\">โฟมเนื้อวิปหนานุ่ม ที่ล้างทำความสะอาดผิวได้อย่างสะอาดหมดจด พร้อมคงความชุ่มชื้น ด้วยส่วนผสมของน้ำแร่ กรดไฮยาลูโรนิค และสารสกัดจากพืชพรรณธรรมชาติ</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• เนื้อโฟมที่แน่นและเข้มข้น ช่วยขจัดเครื่องสำอาง ของเสีย และสิ่งสกปรกที่ตกค้างในรูขุมขน</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• ด้วยส่วนผสมของ Mineral rich hydrogen water และ hyaluronic acids ถึง 8 ชนิด จึงช่วยเติมเต็มผิวให้เนียนนุ่มชุ่มชื้น</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• ทำความสะอาดผิวอย่างอ่อนโยน ด้วยส่วนผสมที่ช่วยลดการระคายเคืองผิว</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• ผ่านการทดสอบการระคายเคืองผิวหน้า </span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">• ปราศจากสารที่ก่อให้เกิดการระคายเคือง 10 ชนิด ได้แก่ พาราเบน 6 ชนิด, สารกันเสีย,เอทานอล, สีสังเคราะห์, น้ำหอมสังเคราะห์</span></p>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(92, 'Essential Mool Cream Mask (5P)', NULL, '20200429192710ooBSH.jpg', '20200429192757.jpg', 'Y', 4, '900.00', NULL, NULL, '<p>แผ่นมาส์กหน้าสูตรเข้มข้นจาก JUNG SAEM MOOL ที่มอบคุณค่าและความชุ่มชื้นสำหรับผิวที่แห้งเสีย พร้อมฟื้นบำรุงผิวหน้าอย่างล้ำลึก ด้วยคุณค่าจาก Ceramide 5 ชนิด และ Hyaluronic acids 8 ชนิด ที่ช่วยคืนความชุ่มชื้นให้แก่ผิว พร้อมสร้างเกราะป้องกันที่ช่วยกักเก็บความชุ่มชื้นให้คงอยู่ยาวนานตลอดวัน เนื้อมาส์กมีความชุ่มฉ่ำ ซึมซาบสู่ชั้นผิวได้อย่างรวดเร็ว พร้อมแผ่นมาส์กแบบ Microfiber ที่แปะได้แนบสนิทกับผิว ช่วยให้เนื้อครีมซึมซาบสู่เนื้อผิวได้ดียิ่งขึ้น  </p>', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.72a0796aEzlR7n\" style=\"margin: 0px; padding: 0px;\">แผ่นมาส์กหน้าสูตรเข้มข้นจาก JUNG SAEM MOOL ที่มอบคุณค่าและความชุ่มชื้นสำหรับผิวที่แห้งเสีย พร้อมฟื้นบำรุงผิวหน้าอย่างล้ำลึก ด้วยคุณค่าจาก Ceramide 5 ชนิด และ Hyaluronic acids 8 ชนิด ที่ช่วยคืนความชุ่มชื้นให้แก่ผิว พร้อมสร้างเกราะป้องกันที่ช่วยกักเก็บความชุ่มชื้นให้คงอยู่ยาวนานตลอดวัน เนื้อมาส์กมีความชุ่มฉ่ำ ซึมซาบสู่ชั้นผิวได้อย่างรวดเร็ว พร้อมแผ่นมาส์กแบบ Microfiber ที่แปะได้แนบสนิทกับผิว ช่วยให้เนื้อครีมซึมซาบสู่เนื้อผิวได้ดียิ่งขึ้น  </span></p><div><span data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.72a0796aEzlR7n\" style=\"margin: 0px; padding: 0px;\"><br></span></div>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19'),
(93, 'Minifying Skin Care Set', NULL, '20200429193029fJsc7.jpg', '20200429193139.jpg', 'Y', 4, '2300.00', NULL, NULL, '<p><b>JUNGSAEMMOOL Minifying Skin Care Set</b>\n</p><p>เซตสุดคุ้มกับ 3 ไอเทม กับ 3 ขั้นตอน เพื่อสุขภาพผิวที่เนียนนุ่ม ชุ่มชื่น กระจ่างใสยิ่งขึ้น Minifying Boosting Toner 100ml\n</p><p>โทนเนอร์สูตรอ่อนโยนแบบน้ำ ที่ให้ความชุ่มชื้นแก่ผิวของคุณได้ง่ายและรวดเร็ว Refreshing hydration ช่วยรักษาระดับความชุ่มชื้นและความยืดหยุ่นของผิวคุณในระดับสูง  สูตรกรดอ่อนช่วยปรับสมดุลความมันของน้ำมันและระดับความชื้นโดยไม่ระคายเคืองMinifying VC Ampoule 30ml</p><p>\nแคปซูลวิตามินแอมเพิล ที่มีความเข้มข้นมากกว่าเซรั่ม จะช่วยให้ผิวเก็บกักความชุ่มชื้นได้ดี เพื่อผิวที่กระจ่างใสอย่างที่สุด\n</p><p>Minifying Water-Wrap Cream 25ml\n</p><p>ครีมบำรุงที่ช่วยปลอบประโลมผิว ห่อหุ้มผิวแห้งกร้านของคุณไว้ด้วยความชุ่มชื้น  สารสกัดสดจาก Organic French Buds ช่วยปลอบประโลมผิวที่บอบบางและแพ้ง่าย ให้ความชุ่มชื้นอย่างล้ำลึก<br></p>', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\"><span data-spm-anchor-id=\"a2o4m.pdp.product_detail.i0.1fb73fbfnoIzWz\" style=\"margin: 0px; padding: 0px; font-weight: bolder;\">JUNGSAEMMOOL Minifying Skin Care Set</span></span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">เซตสุดคุ้มกับ 3 ไอเทม กับ 3 ขั้นตอน เพื่อสุขภาพผิวที่เนียนนุ่ม ชุ่มชื่น กระจ่างใสยิ่งขึ้น <span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Minifying Boosting Toner 100ml</span></span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">โทนเนอร์สูตรอ่อนโยนแบบน้ำ ที่ให้ความชุ่มชื้นแก่ผิวของคุณได้ง่ายและรวดเร็ว Refreshing hydration ช่วยรักษาระดับความชุ่มชื้นและความยืดหยุ่นของผิวคุณในระดับสูง  สูตรกรดอ่อนช่วยปรับสมดุลความมันของน้ำมันและระดับความชื้นโดยไม่ระคายเคือง<span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Minifying VC Ampoule 30ml</span></span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">แคปซูลวิตามินแอมเพิล ที่มีความเข้มข้นมากกว่าเซรั่ม จะช่วยให้ผิวเก็บกักความชุ่มชื้นได้ดี เพื่อผิวที่กระจ่างใสอย่างที่สุด</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\"><span style=\"margin: 0px; padding: 0px; font-weight: bolder;\">Minifying Water-Wrap Cream 25ml</span></span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 8px 0px; font-size: 14px; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; white-space: pre-wrap;\"><span style=\"margin: 0px; padding: 0px;\">ครีมบำรุงที่ช่วยปลอบประโลมผิว ห่อหุ้มผิวแห้งกร้านของคุณไว้ด้วยความชุ่มชื้น  สารสกัดสดจาก Organic French Buds ช่วยปลอบประโลมผิวที่บอบบางและแพ้ง่าย ให้ความชุ่มชื้นอย่างล้ำลึก</span></p>', '2020-04-29', '2025-01-25', 'Y', 'N', '2020-06-25 08:53:19', '2020-06-25 08:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `qarelat`
--

CREATE TABLE `qarelat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_admin` int(11) DEFAULT NULL,
  `topic_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `qarelat`
--

INSERT INTO `qarelat` (`id`, `user_admin`, `topic_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, NULL, 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, NULL, 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `score` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `title`, `product_id`, `user_id`, `score`, `content`, `status`, `hit`, `created_at`, `updated_at`) VALUES
(1, 'Review Lip', 21, 4, '4', '<p><span style=\"color: rgb(51, 51, 51); font-family: Tahoma; font-size: 14px;\">ราคาน่ารัก​ แถมกลิ่นหอมมากๆด้วย​ หาซื้อง่ายๆ​ ข้างบ้านเลยจ้าา​ เซเว่นน​ ราคาก็ไม่แพงนัก​ เห็นคนรีวิวเยอะมากเลยเลยลองใช้</span><br></p>', 'Y', 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Review Eye', 26, 4, '2', '<p><span style=\"font-family: Arial, Helvetica, sans-serif, Geneva, Tahoma; font-size: 16.002px;\"><font color=\"#ff9c00\">ถูกและดี</font></span><br></p>', 'Y', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Review Base', 11, 4, '3', '<p><span style=\"color: rgb(192, 192, 192); font-family: Arial, Helvetica, sans-serif, Geneva, Tahoma; font-size: 16.002px; background-color: rgb(34, 34, 68);\">ค่อย ๆ มาไล่อ่านและพบว่า มันคือ คลังแสงชัด ๆ</span><br></p>', 'Y', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'Review Tool', 78, 3, '4', 'กระดาษนุ่น และอ่อนโยนต่อผิว', 'Y', 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'Review Skincare', 47, 3, '3', '<p>ถูกและดี</p>', 'Y', 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(6, 'ทดสอบรีวิวแอดมิน', 79, 3, '5', '<p>Puff นุ่มมาก</p>', 'Y', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(7, 'ทดสอบรีวิวแอดมิน2', 69, 1, '4', '<p>แปรงนุ่มมาก</p>', 'Y', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(8, 'แก้ไขรีวิวแอดมิน', 21, 3, '3', 'สี Lip สดมาก', 'Y', 7, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(9, 'Test <script>alert(111)</script>', 1, 1, '1', 'test', 'Y', 126, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `sendemail`
--

CREATE TABLE `sendemail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `store_category`
--

CREATE TABLE `store_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `name_th` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_category`
--

INSERT INTO `store_category` (`id`, `name_en`, `name_th`, `is_active`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'INDEX', 'หน้าหลัก', 'Y', 1, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'DEPARTMENT STORE', 'ห้างสรรพสินค้า', 'Y', 2, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'MULTI BRAND BOUTIQUE', 'เทส', 'Y', 3, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'DUTY FREE', 'เทส', 'Y', 4, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'SHOP IN SHOP', 'เทส', 'Y', 5, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `topic_qa`
--

CREATE TABLE `topic_qa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci COMMENT 'หัวเรื่อง',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ประเภท',
  `content` longtext COLLATE utf8mb4_unicode_ci COMMENT 'เนื้อเรื่อง',
  `code` text COLLATE utf8mb4_unicode_ci COMMENT 'รหัส capchat',
  `hit` int(11) DEFAULT NULL COMMENT 'จำนวนเข้าดู',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'สถานนะ',
  `user_id` int(11) DEFAULT NULL COMMENT 'คนตั้งtopic',
  `product_id` int(11) DEFAULT NULL COMMENT 'Product',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topic_qa`
--

INSERT INTO `topic_qa` (`id`, `title`, `type`, `content`, `code`, `hit`, `status`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'Gu Pune Test', '1', '<p>Gu Pune Test</p>', NULL, 0, 'Y', 1, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(2, 'Check out error', '2', '<p>Check out error<br></p>', NULL, 0, 'Y', 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'Test Content', '2', '<p>Test Content<br></p>', NULL, 0, 'Y', 4, NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(4) NOT NULL DEFAULT '0',
  `reset_password_token` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `fname`, `lname`, `verified`, `reset_password_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'rkknoob1', 'rkknoob1@gmail.com', NULL, '$2y$10$hsOyV7bm1U8wTFQ97GdeQ.UOpx5PTH8cAJpU/oTNZP91fbPv5XCqy', 'rkknoob', 'rkknoob', 1, '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(3, 'ploy', 'ploy@gmail.com', NULL, '$2y$10$ee2NM0UvZMKBjxcVuNEicOYZE9K5nCVrfFhIu5/SLXsFvmPu/Qeh.', 'sujittra', 'wauwai', 1, '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(4, 'sujittra.adev', 'sujittra.adev@gmail.com', NULL, '$2y$10$ee2NM0UvZMKBjxcVuNEicOYZE9K5nCVrfFhIu5/SLXsFvmPu/Qeh.', 'sujittra', 'wauwai', 1, '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(5, 'kraisiea', 'kraisiea@siampiwat.com', NULL, '$2y$10$vyaAmgImg6pm0LANLqiIwOdcPf3CeImjrorvV5jouZL7IZy5WwzLe', 'Ftest', 'LTest', 1, '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20'),
(20, 'rkknoob', 'rkknoob@gmail.com', NULL, '$2y$10$xZGxfT/9ezOJSlv6pjUljujN9CJ8nkNp0SbpV55foA77e7pxNQl3K', 'rkknoob', 'rkknoob', 0, '', NULL, '2020-06-25 08:53:20', '2020-06-25 08:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` text COLLATE utf8mb4_unicode_ci,
  `link_video` text COLLATE utf8mb4_unicode_ci,
  `imagemobile` text COLLATE utf8mb4_unicode_ci,
  `is_active` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `type`, `name_en`, `link_video`, `imagemobile`, `is_active`, `created_at`, `updated_at`) VALUES
(1, NULL, 'VDO Banner', 'https://www.youtube.com/embed/n7zaxcp6prc?playlist=n7zaxcp6prc', NULL, 'Y', '2020-06-25 08:53:20', '2020-06-25 08:53:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu_head`
--
ALTER TABLE `admin_menu_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu_process`
--
ALTER TABLE `admin_menu_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_submenu`
--
ALTER TABLE `admin_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers_qa`
--
ALTER TABLE `answers_qa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_magazine`
--
ALTER TABLE `artist_magazine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_tip`
--
ALTER TABLE `artist_tip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brandjsm`
--
ALTER TABLE `brandjsm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand_magazine`
--
ALTER TABLE `brand_magazine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cate_qa`
--
ALTER TABLE `cate_qa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dim_line_user`
--
ALTER TABLE `dim_line_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fact_menu`
--
ALTER TABLE `fact_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `finestore`
--
ALTER TABLE `finestore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontlang`
--
ALTER TABLE `frontlang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linestory`
--
ALTER TABLE `linestory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linestory_description`
--
ALTER TABLE `linestory_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_lang`
--
ALTER TABLE `menu_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_images`
--
ALTER TABLE `notice_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popup`
--
ALTER TABLE `popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_description_th`
--
ALTER TABLE `product_description_th`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qarelat`
--
ALTER TABLE `qarelat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sendemail`
--
ALTER TABLE `sendemail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_category`
--
ALTER TABLE `store_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic_qa`
--
ALTER TABLE `topic_qa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `admin_menu_head`
--
ALTER TABLE `admin_menu_head`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admin_menu_process`
--
ALTER TABLE `admin_menu_process`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `admin_submenu`
--
ALTER TABLE `admin_submenu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `answers_qa`
--
ALTER TABLE `answers_qa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `artist_magazine`
--
ALTER TABLE `artist_magazine`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `artist_tip`
--
ALTER TABLE `artist_tip`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brandjsm`
--
ALTER TABLE `brandjsm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `brand_magazine`
--
ALTER TABLE `brand_magazine`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cate_qa`
--
ALTER TABLE `cate_qa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dim_line_user`
--
ALTER TABLE `dim_line_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fact_menu`
--
ALTER TABLE `fact_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `finestore`
--
ALTER TABLE `finestore`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `frontlang`
--
ALTER TABLE `frontlang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `linestory`
--
ALTER TABLE `linestory`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `linestory_description`
--
ALTER TABLE `linestory_description`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `menu_lang`
--
ALTER TABLE `menu_lang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=339;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notice_images`
--
ALTER TABLE `notice_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `popup`
--
ALTER TABLE `popup`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `product_description`
--
ALTER TABLE `product_description`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `product_description_th`
--
ALTER TABLE `product_description_th`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `qarelat`
--
ALTER TABLE `qarelat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sendemail`
--
ALTER TABLE `sendemail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store_category`
--
ALTER TABLE `store_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topic_qa`
--
ALTER TABLE `topic_qa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
