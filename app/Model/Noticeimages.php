<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Noticeimages extends Model
{
    protected $table = 'notice_images';
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'notice_id',
        'detail_type',
        'img_en',
    ];

}
