<?php

namespace App\Http\Controllers;


use JWTAuth;
use JWTexception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Log;



class AuthController extends Controller
{

    public function __construct(){

        $this->middleware('jwt.auth',['except' => 'login']);
    }
    public function login(Request $request){
        $credentials = $request->only(['email','password']);
        \Log::info($request->all());
        try{
            if(!$token = JWTAuth::attempt($credentials)){
                return response()->json([
                    'status' => false,
                    'message' => "invalid credentials"
                ]);
            }
        } catch (JWTexception $e) {
            return response()->json([
                'status' => false,
                'message' => "count not create token"
            ]);
        }

        return response()->json([
            'status' => true,
            'response' => Auth::guard('api')->user(),
            'token' => $token,
            'expires_in' => auth('api')->factory()->getTTL() * 1,
            'message' => "Login Success"
        ]);

    }

    public function check(Request $request){

        return response()->json([
            'status' => true,
            'response' => Auth::guard('api')->user(),
            'message' => "Logout Success"
        ],200);
    }

    public function logout(Request $request){
        Auth::guard('api')->logout();
        return response()->json([
            'status' => true,
            'response' => Auth::guard('api')->user(),
            'message' => "Logout Success"
        ],200);
    }
}
