<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('admin/auth')->group(function (){
    Route::post('/login', 'AuthController@login');
    Route::get('/check', 'AuthController@check');
    Route::get('/logout', 'AuthController@logout');
    Route::post('/user', 'API\UserController@datauser');
    Route::post('/adduser', 'API\UserController@adduser');
    Route::post('/event', 'API\EventController@datevent');
    Route::resource('/events', 'API\EventController');
});

